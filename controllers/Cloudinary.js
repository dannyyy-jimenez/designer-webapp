const Colors = require('./colors');
const Metrics = require('./Metrics');
const cloudinary = require('cloudinary').v2;
cloudinary.config({
  cloud_name: 'lgxy',
  api_key: process.env.CLOUDINARY_KEY,
  api_secret: process.env.CLOUDINARY_SECRET
});
const crypto = require('crypto');

module.exports = {
  TemplateCloudinaryID: TemplateCloudinaryID,
  GenerateTransformation: GenerateTransformation,
  GenerateTShirtShots: GenerateTShirtShots,
  GenerateTShirtSleeveShots: GenerateTShirtSleeveShots,
  GenerateCapShots: GenerateCapShots,
  GenerateHoodieShots: GenerateHoodieShots,
  GenerateCrewneckShots: GenerateCrewneckShots,
  GeneratePopSocketShots: GeneratePopSocketShots,
  GeneratePillowShots: GeneratePillowShots,
  GenerateMugShots: GenerateMugShots,
  GenerateBeanieShots: GenerateBeanieShots,
  GenerateColorChangeShots: GenerateColorChangeShots
};

function TemplateCloudinaryID(type, side = '', useAlt = false) {
  if (type === 'hoodie' && useAlt) return `tailori/templates/static/sweatshirt-hoodie-alt${side}.png`;
  if (type === 'hoodie') return `tailori/templates/static/sweatshirt-hoodie${side}.png`;
  if (type === 'cap') return `tailori/templates/static/cap${side}.png`;
  if (type === 'popsocket') return `tailori/templates/static/popsocket${side}.png`;
  if (type === 'pillow') return `tailori/templates/static/pillow.png`;
  if (type === 'mug') return `tailori/templates/static/mug${side}.png`;
  if (type === 'shirtTsleeve') return `tailori/templates/static/t-shirt-sleeve${side}.png`;
  if (type === 'crewneck') return `tailori/templates/static/sweatshirt-crewneck${side}.png`;

  return `tailori/templates/static/t-shirt${side}.png`;
}

function GenerateTransformation(metric, FormatedMetric) {

  // 210px original padding to drawable area
  // 378px width

  if (metric.artwork === 'reactive') metric.artwork = 'tailori:templates:static:reactive.png'

  return {
    overlay: metric.artwork.replace(/\//g, ':'),
    crop: 'pad',
    gravity: 'north_west',
    ...FormatedMetric
  }
}

function GenRan() {
  return (Math.random() * (9 - 1) + 1).toFixed(0);
}

function CreateIdentifier(name) {
  let beginning = GenRan();
  let ending = GenRan();
  const consonants = name.replace(/[^a-zA-Z0-9 ]/g, "").replace(/[aeiou ]/gi, '').split('').filter(function(item, pos, self) {
    return self.indexOf(item) == pos;
  }).join('').toUpperCase();

  return beginning + consonants;
}

async function GenerateTShirtShots(folder, name, metrics, type, base, tags, temp = false) {
  let front = [];
  let back = [];
  let left = [];
  let right = [];
  const shots = [];

  metrics.forEach(metric => {
    let FormatedMetric = Metrics[type][metric.side];

    if (Metrics[type].front.includes(metric.side)) {
      front.push(GenerateTransformation(metric, FormatedMetric));
    } else if (Metrics[type].back.includes(metric.side)) {
      back.push(GenerateTransformation(metric, FormatedMetric));
    } else if (Metrics[type].left.includes(metric.side)) {
      left.push(GenerateTransformation(metric, FormatedMetric));
    } else if (Metrics[type].right.includes(metric.side)) {
      right.push(GenerateTransformation(metric, FormatedMetric));
    }
  });

  if (front.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type), {transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...front]});

    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDM${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'front'],
      format: 'png'
    });
    shots.push(shot.public_id);
  }

  if (back.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type, '_back'), {transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...back]});
    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDM${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'back'],
      format: 'png'
    });
    shots.push(shot.public_id);
  }

  if (left.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type, '_left'), {transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...left]});
    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDM${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'left'],
      format: 'png'
    });
    shots.push(shot.public_id);
  }

  if (right.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type, '_right'), {transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...right]});
    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDM${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'right'],
      format: 'png'
    });
    shots.push(shot.public_id);
  }
  return shots;
}

async function GenerateTShirtSleeveShots(folder, name, metrics, type, base, tags) {
  let front = [];
  let back = [];
  let left = [];
  let right = [];
  const shots = [];

  metrics.forEach(metric => {
    let FormatedMetric = Metrics[type][metric.side];

    if (Metrics[type].front.includes(metric.side)) {
      front.push(GenerateTransformation(metric, FormatedMetric));
    } else if (Metrics[type].back.includes(metric.side)) {
      back.push(GenerateTransformation(metric, FormatedMetric));
    } else if (Metrics[type].left.includes(metric.side)) {
      left.push(GenerateTransformation(metric, FormatedMetric));
    } else if (Metrics[type].right.includes(metric.side)) {
      right.push(GenerateTransformation(metric, FormatedMetric));
    }
  });

  if (front.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type, '_front'), {transformation: [{effect: `replace_color:${Colors(type, base)}:500`}, ...front]});
    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDS${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'front'],
      format: 'png'
    });
    shots.push(shot.public_id);
  }

  if (back.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type, '_back'), {transformation: [{effect: `replace_color:${Colors(type, base)}:500`}, ...back]});
    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDS${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'back'],
      format: 'png'
    });
    shots.push(shot.public_id);
  }

  if (left.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type, '_left'), {transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...left]});
    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDS${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'left'],
      format: 'png'
    });
    shots.push(shot.public_id);
  }

  if (right.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type, '_right'), {transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...right]});
    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDS${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'right'],
      format: 'png'
    });
    shots.push(shot.public_id);
  }
  return shots;
}

async function GenerateHoodieShots(folder, name, metrics, type, base, tags) {
  let front = [];
  let back = [];
  const shots = [];

  let sides = metrics.map(metric => metric.side);
  let altSidesReq = ['mediumfront', 'centerfront', 'leftchest', 'rightchest'];
  let useAlt = true;

  for (let side of sides) {
    if (!altSidesReq.includes(side)) {
      useAlt = false;
    }
  }

  metrics.forEach(metric => {
    let FormatedMetric = Metrics[type][metric.side];

    if (Metrics[type].front.includes(metric.side)) {
      front.push(GenerateTransformation(metric, FormatedMetric));
    } else if (Metrics[type].back.includes(metric.side)) {
      back.push(GenerateTransformation(metric, FormatedMetric));
    }
  });

  if (front.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type, '', useAlt), {transformation: [{effect: `replace_color:${Colors(type, base)}:10000`}, ...front]});
    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDH${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'front'],
      format: 'png'
    });
    shots.push(shot.public_id);
  }

  if (back.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type, '_back'), {transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...back]});
    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDH${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'back'],
      format: 'png'
    });
    shots.push(shot.public_id);
  }

  return shots;
}

async function GenerateCrewneckShots(folder, name, metrics, type, base, tags) {
  let front = [];
  let back = [];
  const shots = [];

  let sides = metrics.map(metric => metric.side);

  metrics.forEach(metric => {
    let FormatedMetric = Metrics[type][metric.side];

    if (Metrics[type].front.includes(metric.side)) {
      front.push(GenerateTransformation(metric, FormatedMetric));
    } else if (Metrics[type].back.includes(metric.side)) {
      back.push(GenerateTransformation(metric, FormatedMetric));
    }
  });

  if (front.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type, ''), {transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...front]});
    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDH${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'front'],
      format: 'png'
    });
    shots.push(shot.public_id);
  }

  if (back.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type, '_back'), {transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...back]});
    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDH${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'back'],
      format: 'png'
    });
    shots.push(shot.public_id);
  }

  return shots;
}

async function GenerateCapShots(folder, name, metrics, type, base, tags) {
  let front = [];
  const shots = [];

  metrics.forEach(metric => {
    let FormatedMetric = Metrics[type][metric.side];

    if (Metrics[type].front.includes(metric.side)) {
      front.push(GenerateTransformation(metric, FormatedMetric));
    }
  });

  if (front.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type), {transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...front]});
    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDC${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'front'],
      format: 'png'
    });
    shots.push(shot.public_id);
  }

  return shots;
}

async function GeneratePopSocketShots(folder, name, metrics, type, base, tags) {
  let front = [];
  const shots = [];

  metrics.forEach(metric => {
    let FormatedMetric = Metrics[type][metric.side];

    if (Metrics[type].front.includes(metric.side)) {
      front.push(GenerateTransformation(metric, FormatedMetric));
    }
  });

  if (front.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type), {transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...front]});
    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDP${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'front'],
      format: 'png'
    });
    shots.push(shot.public_id);
  }

  return shots;
}

async function GeneratePillowShots(folder, name, metrics, type, base, tags) {
  let front = [];
  let mainShotID = '';

  const shots = [];

  metrics.forEach(metric => {
    let FormatedMetric = Metrics[type][metric.side];

    if (Metrics[type].front.includes(metric.side)) {
      mainShotID = metric.artwork + '.png';

      front.push({
        gravity: 'north_west',
        ...FormatedMetric
      });

      shots.push(mainShotID);
    }
  });

  if (front.length > 0) {
    let mockup = await cloudinary.url(mainShotID, {transformation: [...front, {overlay: 'tailori:templates:static:pillow', flags: ['cutter','layer_apply']}, {overlay: 'tailori:templates:static:pillow', opacity: 50, effect: `replace_color:${Colors(type, base)}:400`}]});
    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDW${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'front'],
      format: 'png'
    });
    shots.unshift(shot.public_id);
  }

  return shots;
}

async function GenerateBeanieShots(folder, name, metrics, type, base, tags) {
  let front = [];
  const shots = [];

  let sides = metrics.map(metric => metric.side);

  metrics.forEach(metric => {
    let FormatedMetric = Metrics[type][metric.side];

    if (Metrics[type].front.includes(metric.side)) {
      front.push(GenerateTransformation(metric, FormatedMetric));
    } else if (Metrics[type].back.includes(metric.side)) {
      back.push(GenerateTransformation(metric, FormatedMetric));
    }
  });

  if (front.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type, ''), {transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...front]});
    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDH${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'front'],
      format: 'png'
    });
    shots.push(shot.public_id);
  }

  return shots;
}

async function GenerateMugShots(folder, name, metrics, type, base, tags) {
  let right = [];
  let left = [];
  const shots = [];

  metrics.forEach(metric => {
    let FormatedMetric = Metrics[type][metric.side];

    if (Metrics[type].right.includes(metric.side)) {
      right.push(GenerateTransformation(metric, FormatedMetric));
    }

    if (Metrics[type].left.includes(metric.side)) {
      left.push(GenerateTransformation(metric, FormatedMetric));
    }
  });

  if (right.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type), {transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...right]});
    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDG${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'right'],
      format: 'png'
    });
    shots.push(shot.public_id);
  }

  if (left.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type, '_left'), {transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...left]});
    let shot = await cloudinary.uploader.upload(mockup, {
      folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${folder}/`,
      public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDG${CreateIdentifier(name)}`,
      tags: [...tags, 'mockup', 'left'],
      format: 'png'
    });
    shots.push(shot.public_id);
  }

  return shots;
}

async function GenerateColorChangeShots(metrics, type, base) {
  let front = [];
  let back = [];
  let left = [];
  let right = [];
  const shots = [];

  let useAlt = true;

  if (type === 'hoodie') {
    let sides = metrics.map(metric => metric.side);
    let altSidesReq = ['mediumfront', 'centerfront', 'leftchest', 'rightchest'];

    for (let side of sides) {
      if (!altSidesReq.includes(side)) {
        useAlt = false;
      }
    }
  }

  metrics.forEach(metric => {
    let FormatedMetric = Metrics[type][metric.side];

    if (Metrics[type].front && Metrics[type].front.includes(metric.side)) {
      front.push(GenerateTransformation(metric, FormatedMetric));
    } else if (Metrics[type].back && Metrics[type].back.includes(metric.side)) {
      back.push(GenerateTransformation(metric, FormatedMetric));
    } else if (Metrics[type].left && Metrics[type].left.includes(metric.side)) {
      left.push(GenerateTransformation(metric, FormatedMetric));
    } else if (Metrics[type].right && Metrics[type].right.includes(metric.side)) {
      right.push(GenerateTransformation(metric, FormatedMetric));
    }
  });

  if (front.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type, '', useAlt), {secure:true, transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...front]});
    shots.push(mockup);
  }

  if (back.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type, '_back'), {secure:true, transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...back]});
    shots.push(mockup);
  }

  if (left.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type, '_left'), {secure:true, transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...left]});
    shots.push(mockup);
  }

  if (right.length > 0) {
    let mockup = await cloudinary.url(TemplateCloudinaryID(type, '_right'), {secure:true, transformation: [{effect: `replace_color:${Colors(type, base)}:400`}, ...right]});
    shots.push(mockup);
  }

  return shots;
}
