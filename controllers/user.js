const crypto = require('crypto');
const twilio = require('twilio')(process.env.TWILIO_SID, process.env.TWILIO_AUTH);
const jwt = require('jsonwebtoken');
const cloudinary = require('cloudinary').v2;
const cloudinaryFunctions =  require('./Cloudinary');
const fs = require('fs');
const moment = require('moment');
const stripe = require('stripe')(process.env.STRIPE_SECRET);
const Notifications = require('./Notifications');
const { Expo } = require('expo-server-sdk');
const Colors = require('./colors');
const pathLib = require('path');
const taxJar = require('./TaxJar');
const trinumDesign = require('./TrinumDesign');

// MODELS
const User = require('../models/user');
const Brand = require('../models/brand');
const Design = require('../models/design');
const Release = require('../models/release');
const Notification = require('../models/notification');
const Order = require('../models/order');
const Model = require('../models/model');
const Admin = require('../models/admin');

cloudinary.config({
  cloud_name: 'lgxy',
  api_key: process.env.CLOUDINARY_KEY,
  api_secret: process.env.CLOUDINARY_SECRET
});

module.exports.register = Register;
module.exports.login = Login;
module.exports.forgot = Forgot;

// Authorized
module.exports.updatePassword = UpdatePassword;
module.exports.updatePermissions = UpdatePermissions;
module.exports.updatePayment = UpdatePayment;
module.exports.updateSockets = UpdateSockets;
module.exports.removeSocket = RemoveSocket;
module.exports.brand = {
  create: CreateBrand,
  verify: VerifyBrand,
  connect: ConnectBrand,
  logo: EditBrandLogo,
  edit: EditBrand,
  dashboard: DashboardBrand
};
module.exports.portfolio = {
  design: CreateDesign,
  release: ReleaseMerch,
  withhold: WithholdMerch,
  pricing: PricingMerch,
  remove: RemoveDesign
};
module.exports.store = {
  favorite: FavoriteBrand,
  bag: BagRelease,
  unbag: UnbagRelease,
  shipping: Shipping,
  shippingRemove: ShippingRemove,
  model: ModelValidate,
  checkout: Checkout
};
  // get
module.exports.load = {
  account: LoadAccount,
  payment: LoadPayment,
  shipping: LoadShipping,
  notifications: LoadNotifications,
  brand: LoadBrand,
  brandProfile: LoadBrandProfile,
  release: LoadRelease,
  portfolio: LoadPortfolio,
  portfolioZoom: LoadPortfolioZoom,
  saleZoom: LoadSaleZoom,
  store: LoadStore,
  zoom: LoadZoom,
  colorChange: LoadColorChange,
  cart: LoadCart,
  history: LoadHistory,
  track: LoadTrack
};

async function Register(body) {
  const firstName = body.firstName.replace(/ /g, '');
  const lastName = body.lastName.replace(/ /g, '');
  const phoneNumber = body.phoneNumber.replace(/\D/g,'');
  let dob;
  try {
    dob = moment(body.dob, "MM-DD-YYYY");
  } catch(e) {
    return respond({
      isError: true,
      error: 'AGE_ERR'
    });
  }
  const password = body.password;
  const isOldEnough = moment().diff(dob, 'years') >= 13 && moment().diff(dob, 'years') < 110;

  const key = crypto.randomBytes(3).toString("hex") + "." + crypto.randomBytes(9).toString("hex") + "." + crypto.randomBytes(7).toString("hex");
  const socket = crypto.randomBytes(2).toString("hex") + "." + crypto.randomBytes(40).toString("hex") + "." + crypto.randomBytes(1).toString("hex");

  if (password.length < 6) {
    return respond({
      isError: true,
      error: 'PASS_LEN'
    });
  }

  if (phoneNumber.length !== 10) {
    return respond({
      isError: true,
      error: 'INVALID_NUM'
    });
  }

  if (!isOldEnough) {
    return respond({
      isError: true,
      error: 'AGE_ERR'
    });
  }

  try {
    const user = new User({
      crypto : key,
      socket : socket,
      firstName : firstName,
      lastName : lastName,
      phoneNumber : phoneNumber,
      dob: dob
    });
    await user.setPassword(password);
    const customer = await stripe.customers.create({
      name: firstName + ' ' + lastName,
      phone: phoneNumber
    });
    user.payment.customer = customer.id;
    const savedUser = await user.save();
    twilio.messages.create({
      mediaUrl: ['https://res.cloudinary.com/lgxy/raw/upload/tailori/documents/Tailori.vcf'],
      body: `Welcome to Tailori, ${firstName}! Checkout our website for more information on how our platform works (https://www.tailorii.app) and add us to your contacts list, enjoy!`,
      from: process.env.TWILIO_NUMBER,
      to: phoneNumber
    });
    return respond({
      body: savedUser.toAuth()
    });
  } catch (e) {
    let err = 'SERVER_ERR';

    if (e.errors && 'phoneNumber' in e.errors) {
      err = "DUP_NUMBER";
    }

    return onCatch(err);
  }
}

async function Login(body) {
  const phoneNumber = body.phoneNumber.replace(/\D/g,'');
  const password = body.password;

  try {
    const user = await User.findOne({phoneNumber: phoneNumber}).exec();

    if (!user) throw 'INVALID_NUM';
    if (!user.validPassword(password)) throw 'INVALID_PASS';

    if (user.deleted) {
      user.deleted = false;
      user.save();
    }

    return respond({
      body: user.toAuth()
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function Forgot(body) {
  const phoneNumber = body.phoneNumber.replace(/\D/g, '');

  try {
    const user = await User.findOne({phoneNumber: phoneNumber, deleted: false}).exec();
    if (!user) throw 'INVALID_NUM';

    const tempPass = (Math.floor(Math.random() * (999999999 - 100000 + 1)) + 100000).toString();
    await user.setPassword(tempPass);
    await user.save();
    twilio.messages.create({
      body: `${user.firstName}, Tailori is here! Remember to change this asap, enjoy!\n\nTemporary password: ${tempPass}`,
      from: process.env.TWILIO_NUMBER,
      to: phoneNumber
    }).then(message => console.log(message));
    return respond({
      isError: false
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function UpdatePassword(body) {
  const auth = jwt.decode(body.auth);
  const password = body.password;

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    if (password.length < 6) throw 'PASS_LEN';

    await user.setPassword(password);
    await user.save();
    return respond({
      isError: false
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function UpdatePermissions(body) {
  const auth = jwt.decode(body.auth);
  const smsUpdates = body.smsUpdates;

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';

    user.permissions.sms = smsUpdates;
    await user.save();
    return respond({
      isError: false,
      body: {
        smsUpdates: user.permissions.sms
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function UpdatePayment(body) {
  const auth = jwt.decode(body.auth);
  const token = body.token;

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';

    const card = await stripe.customers.createSource(user.payment.customer, {source: token});
    await stripe.customers.update(user.payment.customer, {default_source: card.id});
    user.payment.source = card.id;
    user.payment.updated = moment();
    await user.save();
    return respond({
      isError: false,
      body: {
        smsUpdates: user.permissions.sms
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function UpdateSockets(body) {
  const auth = jwt.decode(body.auth);
  const token = body.token;

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';

    if (user.sockets.indexOf(token) === -1 && Expo.isExpoPushToken(token)) {
      user.sockets.push(token);
      await user.save();
    }
    return respond({});
  } catch (e) {
    return onCatch(e);
  }
}

async function RemoveSocket(body) {
  const auth = jwt.decode(body.auth);
  const token = body.token;

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';

    if (user.sockets.indexOf(token) > -1) {
      user.sockets.splice(user.sockets.indexOf(token), 1);
      await user.save();
    }
    return respond({});
  } catch (e) {
    return onCatch(e);
  }
}

function GenRan() {
  return (Math.random() * (9 - 1) + 1).toFixed(0);
}

function CreateIdentifier(name, useDigits = true) {
  let beginning = GenRan();
  let ending = GenRan();
  const consonants = name.replace(/[^a-zA-Z0-9 ]/g, "").replace(/[aeiou ]/gi, '').split('').filter(function(item, pos, self) {
    return self.indexOf(item) == pos;
  }).join('').toUpperCase();

  if (useDigits && consonants.length < 5) {
    beginning += GenRan();
    ending += GenRan();
  }

  return beginning + consonants + (useDigits ? ending : '');
}

async function CreateBrand(body) {
  const auth = jwt.decode(body.auth);
  const token = body.token;
  const name = body.name;
  const description = body.description;
  const encodedLogo = body.logo.replace(/^data:image\/png;base64,/, "");

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).populate('brand').exec();
    if (!user) throw 'MISMATCH_AUTH';
    const path = pathLib.join(__dirname, '../tmp', crypto.randomBytes(5).toString("hex") + '.png');
    await fs.writeFileSync(path, encodedLogo,  {encoding: 'base64'});

    let brand;

    if (user.brand) {
      brand = await Brand.findOne({_id: user.brand._id, creator: user._id}).exec();
      brand.name = name;
      brand.description = description;
    } else {
      brand = new Brand({
        creator: user._id,
        identifier: CreateIdentifier(name),
        logo: '',
        name: name,
        description: description,
      });
      user.brand = brand._id;
    }
    const result = await cloudinary.uploader.upload(path, {
      folder: `${process.env.CLOUDINARY_FOLDER}/brands/`,
      public_id: `${CreateIdentifier(name, false)}.TRBI.${crypto.randomBytes(8).toString("hex")}`,
      tags: [`${brand.identifier}`, `${brand.name}`, `${brand._id}`]
    });
    fs.unlinkSync(path);
    brand.logo = result.public_id;
    await brand.save();
    await user.save();

    const account = await stripe.accounts.create({
      type: 'express',
      capabilities: {
        card_payments: {requested: true},
        transfers: {requested: true},
        tax_reporting_us_1099_misc: {requested: true}
      },
      metadata: {
        user_id: user._id.toString()
      },
      business_type: 'individual',
      individual: {
        first_name: user.firstName,
        last_name: user.lastName,
        phone: user.phoneNumber,
        dob: {
          month: moment(user.dob).format('M'),
          day: moment(user.dob).format('D'),
          year: moment(user.dob).format('YYYY')
        }
      }
    });

    brand.connect = account.id;
    brand.save();

    const accountLinks = await stripe.accountLinks.create({
      account: brand.connect,
      refresh_url: 'https://www.tailorii.app/brand/refresh',
      return_url: 'https://www.tailorii.app/brand/success',
      type: 'account_onboarding',
    });

    return respond({
      body: {
        _u: accountLinks.url
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function ConnectBrand(body) {
  const phoneNumber = body.phoneNumber.replace(/\D/g,'');
  const connectToken = body.connectToken;

  try {
    const user = await User.findOne({phoneNumber: phoneNumber}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const brand = await Brand.findOne({creator: user._id, _id: user.brand});
    if (!brand || brand.connect) throw 'INEX_BRAND';

    const response = await stripe.oauth.token({
      grant_type: 'authorization_code',
      code: connectToken,
      assert_capabilities: ['transfers'],
    });

    if (response.error) throw 'STRIPE_ERROR';

    brand.connect = response.stripe_user_id;
    await brand.save();
    return respond({});
  } catch (e) {
    return onCatch(e);
  }
}

async function EditBrandLogo(body) {
  const auth = jwt.decode(body.auth);
  const encodedImage = body.logo.replace(/^data:image\/png;base64,/, "");

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const brand = await Brand.findOne({creator: user._id}).exec();
    if (!brand) throw 'INEX_BRAND';

    const path = pathLib.join(__dirname, '../tmp', crypto.randomBytes(5).toString("hex") + '.png');
    await fs.writeFileSync(path, encodedImage,  {encoding: 'base64'});

    if (brand.logo) {
      await cloudinary.uploader.destroy(brand.logo);
    }

    const result = await cloudinary.uploader.upload(path, {
      folder: `${process.env.CLOUDINARY_FOLDER}/brands/`,
      public_id: `${brand.identifier}.TRBI.${crypto.randomBytes(8).toString("hex")}`,
      tags: [`${brand.identifier}`, `${brand.name}`, `${brand._id}`]
    });
    fs.unlinkSync(path);
    brand.logo = result.public_id;
    await brand.save();
    return respond({
      body: {
        _l: brand.logo
      }
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function EditBrand(body) {
  const auth = jwt.decode(body.auth);
  const description = body.description.trim();
  const yt = body.yt ? body.yt.trim().replace(/^@/g, "") : null;
  const fb = body.fb ? body.fb.trim().replace(/^@/g, "") : null;
  const sc = body.sc ? body.sc.trim().replace(/^@/g, "") : null;
  const ig = body.ig ? body.ig.trim().replace(/^@/g, "") : null;

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const brand = await Brand.findOne({creator: user._id}).exec();
    if (!brand) throw 'INEX_BRAND';

    brand.description = description;
    brand.socials = {
      youtube: yt,
      facebook: fb,
      snapchat: sc,
      instagram: ig
    };
    await brand.save();
    return respond({});
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function DashboardBrand(body) {
  const auth = jwt.decode(body.auth);

  try {
    const user = await User.findOne({crypto: auth.crypto}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const brand = await Brand.findOne({creator: user._id, _id: user.brand});
    if (!brand) throw 'INEX_BRAND';

    const linkRequest = await stripe.accounts.createLoginLink(brand.connect);

    return respond({
      body: {
        _l: linkRequest.url
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function VerifyBrand(body) {
  const auth = jwt.decode(body.auth);

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const brand = await Brand.findOne({_id: user.brand, creator: user._id}).exec();
    if (!brand) throw 'INEX_BRAND';
    return respond({});
  } catch (e) {
    return onCatch(e);
  }
}

async function CreateDesign(body) {
  const auth = jwt.decode(body.auth);
  const name = body.name;
  const description = body.description;
  const type = body.type;
  const mode = body.mode;
  let metrics = body.metrics.map((metric) => {
    return {
      uri: metric.uri ? metric.uri.replace(/^data:image\/(png|jpg|jpeg);base64,/, "") : metric.file.replace(/^data:image\/(png|jpg|jpeg);base64,/, ""),
      path: pathLib.join(__dirname, '../tmp', crypto.randomBytes(6).toString("hex")),
      side: metric.side,
      secure: ''
    }
  });
  const base = body.base;

  const TYPES = ['hoodie', 'crewneck', 'beanie', 'cap', 'shirtT', 'shirtTsleeve', 'popsocket', 'pillow', 'mug'];

  try {
    if (mode !== 'workshop' && mode !== 'template') throw 'BAD_MODE';
    if (!TYPES.includes(type)) throw 'BAD_TYPE';

    const user = await User.findOne({crypto: auth.crypto, deleted: false}).populate('brand').exec();
    if (!user) throw 'MISMATCH_AUTH';

    let tags = [user._id, CreateIdentifier(name), mode, type, 'design', 'portfolio'];

    if (user.brand) {
      tags.push(user.brand.identifier);
    }

    await Promise.all(metrics.map(async (metric) => {
      await fs.writeFileSync(metric.path, metric.uri,  {encoding: 'base64'});
    }))

    const metricsFormat = [];

    for (let metric of metrics) {
      let result = await cloudinary.uploader.upload(metric.path, {
        folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${user._id}/`,
        public_id: `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDN${CreateIdentifier(name, false)}`,
        tags: [...tags, 'piece', metric.side]
      });

      metric.secure = result.public_id;
      fs.unlinkSync(metric.path);
      metricsFormat.push({
        artwork: metric.secure,
        side: metric.side
      })
    }

    let shots = [];

    if (type === 'shirtT') {
      shots = await cloudinaryFunctions.GenerateTShirtShots(user._id, name, metricsFormat, type, base, tags);
    } else if (type === 'shirtTsleeve') {
      shots = await cloudinaryFunctions.GenerateTShirtSleeveShots(user._id, name, metricsFormat, type, base, tags);
    } else if (type === 'cap') {
      shots = await cloudinaryFunctions.GenerateCapShots(user._id, name, metricsFormat, type, base, tags)
    } else if (type === 'hoodie') {
      shots = await cloudinaryFunctions.GenerateHoodieShots(user._id, name, metricsFormat, type, base, tags)
    } else if (type === 'crewneck') {
      shots = await cloudinaryFunctions.GenerateCrewneckShots(user._id, name, metricsFormat, type, base, tags)
    } else if (type === 'popsocket') {
      shots = await cloudinaryFunctions.GeneratePopSocketShots(user.id, name, metricsFormat, type, base, tags)
    } else if (type === 'pillow') {
      shots = await cloudinaryFunctions.GeneratePillowShots(user.id, name, metricsFormat, type, base, tags);
    } else if (type === 'mug') {
      shots = await cloudinaryFunctions.GenerateMugShots(user.id, name, metricsFormat, type, base, tags);
    } else if (type === 'beanie') {
      shots = await cloudinaryFunctions.GenerateBeanieShots(user.id, name, metricsFormat, type, base, tags);
    }

    const design = new Design({
      creator: user._id,
      designer: user.brand ? user.brand._id : null,
      type: type,
      shots: shots,
      mode: mode,
      name: name,
      metrics: {
        base: base,
        sides: metricsFormat
      },
      description: description
    });

    user.portfolio.push(design._id);
    await design.save();
    await user.save();

    return respond({});
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function RemoveDesign(body) {
  const auth = jwt.decode(body.auth);
  const designID = body.design;

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const brand = await Brand.findOne({_id: user.brand, creator: user._id}).populate('releases').exec();
    const design = await Design.findOne({_id: designID, creator: user._id}).exec();
    if (!design) throw 'INEX_DESIGN';

    if (brand) {
      let released = false;
      let index = 0;

      for (let i = 0; i < brand.releases.length; i++) {
        if (brand.releases[i].design.toString() === design._id.toString()) {
          released = true;
          index = i;
          break;
        }
      }

      if (released) {
        brand.releases.splice(index, 1);
        await brand.save()
      }
    }

    design.deleted = true;
    await design.save();
    return respond({});
  } catch (e) {
    return onCatch(e);
  }
}

async function ReleaseMerch(body) {
  const auth = jwt.decode(body.auth);
  const designID = body.design;
  const limited = body.limited == "true";
  let limit = body.limit;
  const colorable = body.colorable == "true";
  let royalty = body.royalty;

  try {
    if (limited && !Number.isInteger(parseInt(limit))) throw 'NO_LIMIT';
    if (isNaN(parseFloat(royalty))) throw 'BAD_ROYALTY';

    limit = parseInt(limit);
    royalty = parseFloat(royalty);

    const user = await User.findOne({crypto: auth.crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const brand = await Brand.findOne({_id: user.brand, creator: user._id}).exec();
    if (!brand) throw 'INEX_BRAND';
    const design = await Design.findOne({_id: designID, creator: user._id}).exec();
    if (!design) throw 'INEX_DESIGN';

    let hasReleased = brand.releases.find((query) => query.toString() == design._id.toString());

    if (hasReleased) throw 'MERCH_RELEASED';

    if (!design.designer) {
      design.designer = brand._id;
      await design.save();
    }

    let actions = [];

    if (colorable) {
      actions.push('colorable');
    }

    if (design.type.includes('shirt') || design.type == 'hoodie' || design.type.includes('shoe')) {
      actions.push('sizeable');
    }

    if (design.isReactive()) {
      actions.push('reactive');
    }

    let release = new Release({
      designer: brand._id,
      design: design._id,
      identifier: brand.identifier + 'TR' +  CreateIdentifier(design.name, false),
      production: 0,
      price: await trinumDesign.CalculatePrice(royalty, design.type),
      royalty: royalty,
      customizable: {
        color: colorable
      },
      actions: actions,
      remaining: limited ? limit : -1
    });
    await release.save();
    brand.releases.push(release._id);
    await brand.save();

    try {
      const admins = await Admin.find({permissions: "releases-approve"}).exec();
      Notifications.send(admins.map(admin => admin.sockets).reduce((aggregate, next) => [...aggregate, ...next], []), {body: 'New release available for validation!', data: {}});
    } catch(e) {

    }

    return respond({
      body: {
        _d: 0,
        _rP: 0,
        _r: release.remaining,
        _p: release.price.toFixed(2)
      }
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function WithholdMerch(body) {
  const auth = jwt.decode(body.auth);
  const designID = body.design;

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const brand = await Brand.findOne({_id: user.brand, creator: user._id}).populate('releases').exec();
    if (!brand) throw 'INEX_BRAND';
    const design = await Design.findOne({_id: designID, creator: user._id}).exec();
    if (!design) throw 'INEX_DESIGN';

    let released = false;
    let index = -1;

    for (let i = 0; i < brand.releases.length; i++) {
      if (brand.releases[i].design.toString() === design._id.toString()) {
        released = true;
        index = i;
        break;
      }
    }
    if (!released) throw 'NOT_RELEASED';

    await Release.deleteOne({designer: brand._id, design: designID}).exec();
    brand.releases.splice(index, 1);
    await brand.save()

    return respond({});
  } catch (e) {
    return onCatch(e);
  }
}

async function PricingMerch(body) {
  const auth = jwt.decode(body.auth);
  const designID = body.design;
  let targetPrice = body.targetPrice;

  try {
    targetPrice = parseFloat(targetPrice);

    const user = await User.findOne({crypto: auth.crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const release = await Release.findOne({design: designID});
    if (!release) throw 'MISMATCH_RELEASE';

    if (targetPrice < release.production + 1) {
      return respond({
        body: {
          _p: release.price.toFixed(2),
          _r: release.royalty.toFixed(2)
        }
      });
    }

    release.price = targetPrice;

    let royalty = targetPrice - release.production - 4.5;
    royalty *= 0.90;
    royalty = (Math.round(royalty * 100) / 100);

    release.royalty = royalty;

    release.save();

    return respond({
      body: {
        _p: release.price.toFixed(2),
        _r: release.royalty.toFixed(2)
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function FavoriteBrand(body) {
  const auth = jwt.decode(body.auth);
  const brandIdentifier = body.designer;

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const brand = await Brand.findOne({identifier: brandIdentifier}).populate("creator").exec();
    if (!brand) throw 'INEX_BRAND';

    if (user.favorites.indexOf(brand._id) > -1) {
      brand.hearts -= 1;
      user.favorites.splice(user.favorites.indexOf(brand._id), 1);
    } else {
      brand.hearts += 1;
      user.favorites.push(brand._id);
      await new Notification({
        recipient: brand.creator._id,
        header: "Someone Loves Your Brand!",
        subheader: `${brand.hearts} 🧡`
      }).save();

      try {
        Notifications.send(brand.creator.sockets, {body: `${brand.name} recieved a new heart!`});
      } catch(e) {
        console.log(e);
      }
    }
    await brand.save();
    await user.save();
    return respond({
      body: {
        _f: user.favorites.indexOf(brand._id) > -1
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

function isValidSize(type, size) {
  if (type === 'pillow' || type === 'mug' || type === 'keychain') return ['default'].includes(size);
  if (type === 'hoodie') return ['S', 'M', 'L', 'XL'].includes(size);
  if (type === 'shirtT_Sleeve') return ['S', 'M', 'L', 'XL'].includes(size);
  if (type === 'cap') return [].includes(size);
  if (type === 'shoe_AF') return ['M3.5', 'M4', 'M4.5', 'M5', 'M5.5', 'M6', 'M6.5', 'M7', 'M7.5', 'M8', 'M8.5', 'M9', 'M9.5', 'M10', 'M10.5', 'M11', 'M11.5', 'M12', 'M12.5', 'M13', 'M13.5'].includes(size);

  if (type === 'shirtT' || type === 'shirtTxB') return ['XS', 'S', 'M', 'L', 'XL', 'XXL'].includes(size);

  return false;
}

function isValidColor(type, color) {
  return Colors(type, color, false, true);
}

async function BagRelease(body) {
  const auth = jwt.decode(body.auth);
  const releaseIdentifier = body.release;
  const size = body.size;
  let color = body.color;
  let quantity = body.quantity;
  let reactives = body.reactives;

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).populate({path: 'cart.items.release', populate: {path: 'design'}}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const release = await Release.findOne({identifier: releaseIdentifier, approved: true, printable: true, remaining: {$ne : 0}}).populate('design').exec();
    if (!release) throw 'INEX_RELEASE';

    quantity = parseInt(quantity);
    if (!isValidSize(release.design.type, size)) throw 'INVALID_SIZE';
    if (color === 'default') color = release.design.metrics.base;
    if (!isValidColor(release.design.type, color)) throw 'INVALID_COLOR';

    let copiedItems = [...user.cart.items];

    if (reactives && reactives.length > 0) {
      reactives = reactives.map(side => {
        if (side.reactivity === 'image') {
          return {
            artwork: side.uri ? side.uri.replace(/^data:image\/(png|jpg|jpeg);base64,/, "") : side.file.replace(/^data:image\/(png|jpg|jpeg);base64,/, ""),
            path: pathLib.join(__dirname, '../tmp', crypto.randomBytes(6).toString("hex")),
            side: side.side
          }
        }

        return {
          artwork: side.artwork,
          side: side.side
        }
      });

      await Promise.all(reactives.map(async (side) => {
        if (side.path) {
          await fs.writeFileSync(side.path, side.artwork,  {encoding: 'base64'});
        }
      }));

      const metricsFormat = [];

      let tags = [user._id, 'reactives', 'portfolio'];

      for (let i = 0; i < reactives.length; i++) {
        if (reactives[i].path) {
          let result = await cloudinary.uploader.upload(reactives[i].path, {
            folder: `${process.env.CLOUDINARY_FOLDER}/portfolios/${user._id}/`,
            public_id: `${crypto.randomBytes(8).toString("hex")}TRRD${crypto.randomBytes(8).toString("hex")}`,
            tags: [...tags, 'reactive', reactives[i].side]
          });

          fs.unlinkSync(reactives[i].path);

          reactives[i] = {
            side: reactives[i].side,
            artwork: result.public_id
          }
        } else {
          reactives[i] = {
            side: reactives[i].side,
            artwork: reactives[i].artwork
          }
        }
      }
    }

    user.cart.items.push({
      release: release._id,
      quantity,
      size,
      color,
      reactives
    });
    copiedItems.push({
      release: release,
      quantity,
      size,
      color,
      reactives
    })

    try {
      user.cart.subtotal = copiedItems.reduce((total, item) => total + (item.quantity * item.release.price), 0);
    } catch(e) {
      user.cart.items = [];
      copiedItems = [];
      user.cart.items.push({
        release: release._id,
        quantity,
        size,
        color,
        reactives
      });
      copiedItems.push({
        release: release,
        quantity,
        size,
        color,
        reactives
      })
      user.cart.subtotal = copiedItems.reduce((total, item) => total + (item.quantity * item.release.price), 0);
    }
    user.cart.subtotal = Math.round(user.cart.subtotal * 100) / 100;

    if (user.cart.subtotal >= 25) {
      user.cart.shipping.cost = 0;
    }

    if (user.cart.shipping.valid) {
      const taxReq = await taxJar.calculate(user.cart.subtotal, user.cart.shipping)
      user.cart.tax = taxReq.tax;
      user.cart.taxRate = taxReq.rate;
    }
    await user.save();
    return respond({});
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function UnbagRelease(body) {
  const auth = jwt.decode(body.auth);
  const itemID = body.item;

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).populate({path: 'cart.items.release', populate: {path: 'design'}}).exec();
    if (!user) throw 'MISMATCH_AUTH';

    for (let i = 0; i < user.cart.items.length; i++) {
      if (user.cart.items[i]._id.toString() === itemID) {
        user.cart.items.splice(i, 1);
        break;
      }
    }

    user.cart.subtotal = user.cart.items.reduce((total, item) => total + (item.quantity * item.release.price), 0);
    user.cart.subtotal = Math.round(user.cart.subtotal * 100) / 100;

    if (user.cart.items.length === 0) {
      user.cart.shipping.cost = 0;
      user.cart.tax = 0;
    }

    if (user.cart.subtotal >= 25) {
      user.cart.shipping.cost = 0;
    } else if (user.cart.subtotal && user.cart.shipping.valid && user.cart.items.length > 0) {
      user.cart.shipping.cost = (await CalculateShipping(user)).cost;
    }

    if (user.cart.shipping.valid && user.cart.items.length > 0) {
      const taxReq = await taxJar.calculate(user.cart.subtotal, user.cart.shipping);
      user.cart.tax = taxReq.tax;
      user.cart.taxRate = taxReq.rate;
    }
    await user.save();
    return respond({});
  } catch (e) {
    return onCatch(e);
  }
}

async function CalculateShipping(user) {
  let shipping = {
    name: user.firstName + ' ' + user.lastName,
    street1: user.cart.shipping.streetAddress,
    street2: user.cart.shipping.streetAddressCont,
    city: user.cart.shipping.city,
    state: user.cart.shipping.state,
    zip: user.cart.shipping.postal,
    country: user.cart.shipping.country
  };
  let items = user.cart.items.map((item) => {
    return {
      type: item.release.design.type,
      quantity: item.quantity
    }
  });
  const shippingRequest = await trinumDesign.shipping(shipping, items);
  if (shippingRequest.isError) throw 'TRINUM_ERROR';

  return {cost: shippingRequest.cost, id: shippingRequest.id};
}

async function Shipping(body) {
  const auth = jwt.decode(body.auth);
  const coordinates = body.coordinates;
  const streetAddress = body.streetAddress;
  const streetAddressCont = body.streetAddressCont;
  const city = body.city;
  const state = body.state;
  const postal = body.postalCode;
  const country = body.country;

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).populate({path: 'cart.items.release', populate: {path: 'design'}}).exec();
    if (!user) throw 'MISMATCH_AUTH';

    user.cart.shipping.streetAddress = streetAddress;
    user.cart.shipping.streetAddressCont = streetAddressCont;
    user.cart.shipping.city = city;
    user.cart.shipping.state = state;
    user.cart.shipping.postal = postal;
    user.cart.shipping.country = country;
    user.cart.shipping.valid = true;

    await user.save();
    return respond({});
  } catch (e) {
    return onCatch(e);
  }
}

async function ShippingRemove(body) {
  const auth = jwt.decode(body.auth);

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';

    user.cart.shipping.streetAddress = null;
    user.cart.shipping.streetAddressCont = null;
    user.cart.shipping.city = null;
    user.cart.shipping.state = null;
    user.cart.shipping.postal = null;
    user.cart.shipping.country = null;
    user.cart.shipping.valid = false;
    user.cart.shipping.cost = 0;

    await user.save();
    return respond({});
  } catch (e) {
    return onCatch(e);
  }
}

async function GetDiscount(scheme, items) {
  let cost = items.reduce((total, next) => total + next.release.price * next.quantity, 0);

  if (scheme === 'minthirty' && cost >= 30) {
     return 3;
  } else if (scheme === 'minfifty' && cost >= 50) {
    return 6;
  } else if (scheme === 'multiplier') {
    return Math.round(cost * 0.05 * 100) / 100;
  }

  return 0;
}

function GetDiscountType(scheme, cost) {
  if (scheme === 'minthirty') {
     return '$3';
  } else if (scheme === 'minfifty' && cost >= 50) {
    return '$6';
  } else if (scheme === 'multiplier') {
    return '5%'
  }

  return '$1 Shipping';
}

async function ModelValidate(body) {
  const auth = jwt.decode(body.auth);
  const modelCode = body.modelCode;

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).populate({path: 'cart.items.release', populate: {path: 'design'}}).exec();
    if (!user) throw 'MISMATCH_AUTH';

    if (modelCode === "TDTMNK") {
      user.cart.shipping.cost = 0;
      user.save();
      return respond({
        body: {
          _dt: 'FREE Shipping',
          _discount: '0.00',
          _s: '0.00',
          _tx: (user.cart.subtotal * user.cart.taxRate).toFixed(2),
          _t: (user.cart.subtotal + (user.cart.subtotal * user.cart.taxRate)).toFixed(2)
        }
      });
    }

    const model = await Model.findOne({identifier: modelCode});
    if (!model) throw 'NO_MODEL';

    let discount = 0;

    if (model.scheme !== 'default') {
      discount = await GetDiscount(model.scheme, user.cart.items);
    }

    let oldSub = user.cart.subtotal;

    user.cart.subtotal = user.cart.subtotal - discount;

    if (model.scheme === 'default') {
     discount = user.cart.shipping.cost - 1;
     user.cart.shipping.cost = 1;
    }

    const taxReq = await taxJar.calculate(user.cart.subtotal, user.cart.shipping);
    user.cart.tax = taxReq.tax;

    user.cart.shipping.cost;
    let total = user.cart.subtotal + user.cart.shipping.cost + user.cart.tax;

    user.save();
    return respond({
      body: {
        _dt: GetDiscountType(model.scheme, oldSub),
        _discount: discount.toFixed(2),
        _s: user.cart.shipping.cost.toFixed(2),
        _tx: user.cart.tax.toFixed(2),
        _t: total.toFixed(2)
      }
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function GetModelPayout(scheme, items) {
  let itemsAmount = items.reduce((total, next) => total + next.quantity, 0);
  let cost = items.reduce((total, next) => total + (next.quantity * next.release.price), 0);

  if (scheme === 'default') {
    return 2;
  } else if (scheme === 'minthirty') {
    return Math.floor(cost / 30) * 4;
  } else if (scheme === 'minfifty') {
    return Math.floor(cost / 50) * 8;
  }

  return 2 * itemsAmount;
}

function CreateSerial() {
  const day = moment().date();
  const month = moment().month() + 1;
  let number = Math.floor(Math.random() * (99999999 - 1)).toString();

  for (let i = 0; i < 8 - number.length; i++) {
    number = "0" + number;
  }

  return (month < 10 ? "0"+month.toString() : month.toString()) + day.toString() + number;
}

async function Checkout(body) {
  const auth = jwt.decode(body.auth);
  const modelCode = body.modelCode;

  try {
    const user = await User.findOne({crypto: auth.crypto, deleted: false}).populate({path: 'cart.items.release', populate: [{path: 'designer'}, {path: 'design'}]}).exec();
    if (!user) throw 'MISMATCH_AUTH';

    let limitedItemsID = [];
    let limitedItemsReleases = [];
    let limitedItemsAmount = [];

    for (let i = user.cart.items.length - 1; i >= 0; i--) {
      if (!user.cart.items[i].release || user.cart.items[i].release.remaining === 0) {
        throw 'INEX_RELEASE';
      }
    }

    user.cart.items.filter((item) => item.release.remaining !== -1).forEach(item => {
      if (limitedItemsID.indexOf(item.release._id.toString()) > -1) {
        limitedItemsAmount[limitedItemsID.indexOf(item.release._id.toString())] += item.quantity;
      } else {
        limitedItemsID.push(item.release._id.toString());
        limitedItemsReleases.push(item.release);
        limitedItemsAmount.push(item.quantity);
      }
    });

    for (let i = 0; i < limitedItemsReleases.length; i++) {
      limitedItemsReleases[i].remaining -= limitedItemsAmount[i];
      if (limitedItemsReleases[i].remaining < 0) throw 'INEX_LIM_RELEASE';
    }

    if (!user.cart.shipping.valid) throw 'BAD_SHIP';

    let brands = [];
    let details = [];
    let cartItems = user.cart.items;

    user.cart.items.forEach((item) => {
      if (brands.indexOf(item.release.designer) == -1) {
        brands.push(item.release.designer);
      }
      details.push({
        seller: item.release.designer._id,
        design: item.release.design,
        color: item.color,
        size: item.size,
        reactives: item.reactives,
        production: item.release.production * item.quantity,
        payout: item.release.royalty * item.quantity,
        quantity: item.quantity,
        price: item.release.price * item.quantity
      });
    });

    let model;
    let discount = 0;
    let modelPayout = 0;
    if (modelCode && modelCode !== "TDTMNK") {
      model = await Model.findOne({identifier: modelCode});
      if (model) {
        if (model.scheme !== 'default') {
          discount = await GetDiscount(model.scheme, user.cart.items);
        }

        user.cart.subtotal -= discount;
        user.cart.subtotal = Math.round(user.cart.subtotal * 100) / 100;

        if (model.scheme === 'default') {
         discount = user.cart.shipping.cost - 1;
         user.cart.shipping.cost = 1;
        }

        const taxReq = await taxJar.calculate(user.cart.subtotal, user.cart.shipping);
        user.cart.tax = taxReq.tax;

        modelPayout = await GetModelPayout(model.scheme, user.cart.items);
        modelPayout = Math.round(modelPayout * 100) / 100;
      }
    }

    if (modelCode && modelCode === "TDTMNK") {
      user.cart.shipping.cost = 0;
      const taxReq = await taxJar.calculate(user.cart.subtotal, user.cart.shipping);
      user.cart.tax = taxReq.tax;
    }

    const shippingRes = await CalculateShipping(user);
    const order = new Order({
      buyer: user._id,
      details: details,
      updates: [{
        message: "Order Received and Processing"
      }],
      serial: CreateSerial(),
      cost: {
        production: details.reduce((total, next) => total + (next.production), 0),
        subtotal: user.cart.subtotal,
        tax: user.cart.tax,
        taxRate: user.cart.taxRate,
        shipping: user.cart.shipping.cost,
        discount: model ? discount : null,
        total: Math.round((user.cart.subtotal + user.cart.tax + user.cart.shipping.cost) * 100) / 100,
      },
      supporting: model ? model._id : null,
      shipping: {
        details: {
          streetAddress: user.cart.shipping.streetAddress,
          streetAddressCont: user.cart.shipping.streetAddressCont,
          city: user.cart.shipping.city,
          state: user.cart.shipping.state,
          postal: user.cart.shipping.postal,
          country: user.cart.shipping.country
        },
        label: {
          identifier: shippingRes.id
        }
      }
    });

    const charge = await stripe.charges.create({
      amount: parseInt(order.cost.total * 100),
      currency: 'usd',
      customer: user.payment.customer,
      description: `Tailori #${order.serial}`,
      metadata: {
        orderID: order._id.toString(),
        orderSerial: order.serial,
        "Production(L)": order.cost.production,
        "Tax Collected": order.cost.tax,
        "Total Pre-Tax(R)": Math.round((order.cost.total - order.cost.tax) * 100) / 100,
        "Model Payout(L)": modelPayout,
        "Brands Payout(L)": order.details.reduce((total, next) => total + (next.payout), 0),
        "Tailori (P)": Math.round((order.cost.subtotal + order.cost.shipping - shippingRes.cost - modelPayout - order.cost.production - order.details.reduce((total, next) => total + (next.payout), 0)) * 100) / 100
      },
      source: user.payment.source,
      statement_descriptor: 'Tailori',
      statement_descriptor_suffix: `T${order.serial}`
    });

    const orderToken = await trinumDesign.order(order.cost.production);

    if (model && model.connect !== 'NA') {
      await stripe.transfers.create({
        amount: parseInt(modelPayout * 100),
        currency: 'usd',
        destination: model.connect,
        transfer_group: order.serial
      });
      let modelLink = await stripe.accounts.createLoginLink(model.connect);
      twilio.messages.create({
        body: `Cha-Ching! ${model.firstName}, you're an amazing model! Check out your most recent payout using this single use link\n\n${modelLink}`,
        from: process.env.TWILIO_NUMBER,
        to: model.phoneNumber
      });
    }

    order.stripe = charge.id;
    order.trinumDesign = orderToken;

    user.cart.items = [];
    user.cart.subtotal = 0;
    user.cart.tax = 0;
    user.cart.taxRate = 0;
    user.cart.shipping.cost = 0;
    user.orders.push(order._id);

    await order.save();
    await user.save();
    twilio.messages.create({
      body: `Thank you for using Tailori! Here is the receipt for your order #${order.serial}\n\n${charge.receipt_url}`,
      from: process.env.TWILIO_NUMBER,
      to: user.phoneNumber
    });

    limitedItemsReleases.forEach(async (release) => {
      await release.save();
    });

    let brandsFound = await Brand.find({_id: {$in : brands}}).populate("creator").exec();

    // Send notifications
    try {
      Notifications.send(brandsFound.map((brand) => brand.creator.sockets).reduce((aggregate, next) => [...aggregate, ...next], []), {body: 'Someone bought your merch!', data: {}});
    } catch(e) {

    }

    try {
      const admins = await Admin.find({permissions: "developer"}).exec();
      Notifications.send(admins.map(admin => admin.sockets).reduce((aggregate, next) => [...aggregate, ...next], []), {body: 'A new order has arrived!', data: {}});
    } catch(e) {

    }

    return respond({
      body: {
        _o: order.serial
      }
    });
  } catch (e) {
    console.log(e);
    if (e.type && (e.type == "StripeInvalidRequestError" || e.type === 'StripeCardError')) {
      e = "PAYMENT_ERROR";
    }
    return onCatch(e);
  }
}

async function LoadAccount(data) {
  const crypto = data.crypto;

  try {
    const user = await User.findOne({crypto: crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';

    return respond({
      body: {
        account: {
          _phone: user.phoneNumber,
          _fn: user.firstName + ' ' + user.lastName
        },
        permissions: {
          _sms: user.permissions.sms
        }
      }
    });

  } catch (e) {
    return onCatch(e);
  }
}

async function LoadPayment(data) {
  const crypto = data.crypto;

  try {
    const user = await User.findOne({crypto: crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';

    let secure = false;
    let name = '';
    let last4 = '';
    let exp = '';
    let source;

    if (user.payment.source) {
      source = await stripe.customers.retrieveSource(user.payment.customer, user.payment.source);
      last4 = source.last4;
      name = source.name;
      secure = true;
      exp = (source.exp_month > 9 ? source.exp_month : `0${source.exp_month}`) + '/' + source.exp_year.toString();
    }

    return respond({
      body: {
        secure: secure,
        name: name,
        last4: last4,
        exp: exp
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function LoadShipping(data) {
  const crypto = data.crypto;

  try {
    const user = await User.findOne({crypto: crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';

    return respond({
      body: {
        streetAddress: user.cart.shipping.streetAddress || "",
        streetAddressCont: user.cart.shipping.streetAddressCont || "",
        city: user.cart.shipping.city || "",
        state: user.cart.shipping.state || "",
        postal: user.cart.shipping.postal || "",
        country: user.cart.shipping.country || "",
        valid: user.cart.shipping.valid
      }
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function LoadNotifications(data) {
  const crypto = data.crypto;

  try {
    const user = await User.findOne({crypto: crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';

    const notifications = await Notification.find({recipient: user._id}, null, {limit: 25, sort: "-createdAt"}).exec();

    const data = [];

    for (let notification of notifications) {
      data.push(notification.format());
    }

    return respond({
      body: {
        _n: data
      }
    });

  } catch (e) {
    return onCatch(e);
  }
}

async function LoadBrand(data) {
  const crypto = data.crypto;

  try {
    const user = await User.findOne({crypto: crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const brand = user.brand ? await Brand.findOne({_id: user.brand, creator: user._id, "connect": {$ne: null}}).populate({path: 'releases', populate: {path: 'design'}}).exec() : null;

    let hasBrand = false;
    let needsAttention = false;
    let loginLink = '';
    let releases = [];
    let brandData = {
      hearts: 0,
      ideas: 0,
      name: '',
      desc: '',
      logo: ''
    };

    let salesData = {
      profit: 0,
      distribution: 0,
      history: []
    };

    if (brand) {
      hasBrand = true;

      if (brand.connect !== "ADMIN") {
        const stripeAccount = await stripe.accounts.retrieve(brand.connect);

        if (!stripeAccount.payouts_enabled) {
          needsAttention = true;
          try {
            loginLink = (await stripe.accounts.createLoginLink(brand.connect)).url;
          } catch {
            loginLink = (await stripe.accountLinks.create({
              account: brand.connect,
              refresh_url: 'https://www.tailorii.app/brand/refresh',
              return_url: 'https://www.tailorii.app/brand/success',
              type: 'account_onboarding',
            })).url;
          }
        }
      }

      releases = await brand.prettyReleases();
      let salesHistory = await Order.find({"details.seller": brand._id}).populate("details.design").sort('-createdAt').exec();
      salesHistory = salesHistory.map((order) => {
        return {
          identifier: order.serial,
          royalty: order.details.reduce((total, detail) => total += detail.payout, 0),
          date: moment(order.createdAt).format('MM/DD'),
          fulfilled: order.fulfilled
        }
      });

      brandData = {
        identifier: brand.identifier,
        hearts: brand.hearts,
        ideas: brand.releases.length,
        name: brand.name,
        desc: brand.description,
        logo: brand.logo,
        socials: {
          _yt: brand.socials.youtube || "",
          _fb: brand.socials.facebook || "",
          _ig: brand.socials.instagram || "",
          _sc: brand.socials.snapchat || ""
        }
      };
      salesData = {
        profit: brand.royalties,
        dist: brand.distribution,
        history: salesHistory
      };
    }

    return respond({
      body: {
        needsAttention,
        loginLink,
        hasBrand,
        _b: brandData,
        _r: releases,
        _s: salesData
      }
    });

  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function LoadPortfolio(data) {
  const crypto = data.crypto;

  try {
    const user = await User.findOne({crypto: crypto, deleted: false}).populate({path: 'brand', populate: {path: 'releases'}}).populate('portfolio').exec();
    if (!user) throw 'MISMATCH_AUTH';

    const data = [];
    const releases = user.brand ? user.brand.releases : [];

    const designs = user.portfolio.filter(design => !design.deleted).sort((a, b) => b.createdAt - a.createdAt);

    for (let design of designs) {
      data.push(design.format(releases));
    }

    return respond({
      body: {
        _d: data
      }
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function LoadPortfolioZoom(data, id) {
  const crypto = data.crypto;
  const designID = id;

  try {
    const user = await User.findOne({crypto: crypto, deleted: false}).populate({path: 'brand', populate: {path: 'releases'}}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const design = await Design.findOne({_id: designID, creator: user._id, deleted: false}).exec();
    if (!design) throw 'NO_DESIGN';

    if (!user.brand) {
      return respond({
        body: {
          _iB: false,
          _hB: false
        }
      });
    }

    let release = user.brand.releases.find((query) => query.design.toString() == design._id.toString());

    if (!release) {
      return respond({
        body: {
          _iB: false,
          _hB: true
        }
      });
    }

    let orders = await Order.find({"details.design": design._id}).exec();
    orders = orders.map((order) => order.details.filter(detail => detail.design.toString() === design._id.toString())).reduce((aggregate, next) => [...aggregate, ...next], []);

    let distribution = orders.reduce((total, detail) => total + detail.quantity, 0);
    let royalties = orders.reduce((total, detail) => total + detail.payout, 0);

    return respond({
      body: {
        _iB: true,
        _hB: true,
        _d: distribution,
        _rP: royalties,
        _r: release.remaining,
        _rRA: release.royalty,
        _rCOP: release.production.toFixed(2),
        _p: release.price.toFixed(2),
        _rI: release.identifier,
        _bI: user.brand.identifier,
        _rA: release.approved && release.printable
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function LoadSaleZoom(data, id) {
  const crypto = data.crypto;
  const saleID = id;

  try {
    const user = await User.findOne({crypto: crypto, deleted: false}).populate({path: 'brand', populate: {path: 'releases'}}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const order = await Order.findOne({serial: saleID}).populate("details.design").exec();

    // let orders = await Order.find({"details.design": design._id}).exec();
    // orders = orders.map((order) => order.details.filter(detail => detail.design.toString() === design._id.toString())).reduce((aggregate, next) => [...aggregate, ...next], []);

    let details = order.details.filter(detail => detail.design ? user.portfolio.includes(detail.design._id) : false).map(detail => {
      return {
        payout: detail.payout,
        design: detail.design.format([]),
        color: detail.color,
        size: detail.size,
        quantity: detail.quantity,
        reactives: detail.reactives
      }
    });

    return respond({
      body: {
        _d: details
      }
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function LoadStore(data, params) {
  const crypto = data.crypto;

  try {
    let sort = params.sort;

    let query = {
      approved: true,
      printable: true
    };
    let sortQuery = {};

    if (sort === 'newest') {
      sortQuery = { updatedAt : -1 };
    } else if (sort === 'oldest') {
      sortQuery = { updatedAt : 1 };
    } else if (sort === 'highestPrice') {
      sortQuery = { price : -1 };
    } else if (sort === 'lowestPrice') {
      sortQuery = { price : 1 };
    }

    const user = await User.findOne({crypto: crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    let releases = await Release.find(query).sort(sortQuery).populate('design').populate('designer').exec();
    const featured = releases.filter(release => release.featured);
    releases = releases.filter(release => !release.featured);

    return respond({
      body: {
        _r: releases.map((release) => release.prettyFormat(user.favorites)),
        _f: featured.map((release) => release.prettyFormat(user.favorites))
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function LoadRelease(data, identifier) {
  const crypto = data.crypto;

  try {
    const user = await User.findOne({crypto: crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const release = await Release.findOne({identifier: identifier, approved: true, printable: true}).populate('design').populate('designer').exec();

    return respond({
      body: {
        _r: release.prettyFormat(user.favorites)
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function LoadBrandProfile(data, identifier) {
  const crypto = data.crypto;

  try {
    const user = await User.findOne({crypto: crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const brand = await Brand.findOne({identifier: identifier}).populate({path: 'releases', populate: ['design', 'designer']}).exec();
    if (!brand) throw 'INEX_BRAND';

    return respond({
      body: brand.profilize(user.favorites)
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function LoadZoom(data, releaseID) {
  const crypto = data.crypto;

  try {
    const user = await User.findOne({crypto: crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const release = await Release.findOne({identifier: releaseID, approved: true, printable: true}).populate('design').populate('designer').exec();
    if (!release) throw 'INEX_RELEASE';

    return respond({
      body: {
        _r: release.prettyFormat(user.favorites),
        _bc: release.design.metrics.base
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function LoadColorChange(data, releaseID, color) {
  const crypto = data.crypto;

  try {
    const user = await User.findOne({crypto: crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const release = await Release.findOne({identifier: releaseID, approved: true, printable: true}).populate('design').populate('designer').exec();
    if (!release) throw 'INEX_RELEASE';

    let shots = [];

    shots = await cloudinaryFunctions.GenerateColorChangeShots(release.design.metrics.sides, release.design.type, color);

    return respond({
      body: {
        _s: shots
      }
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function LoadCart(data) {
  const crypto = data.crypto;

  try {
    const user = await User.findOne({crypto: crypto, deleted: false}).populate({path: 'cart.items.release', populate: [{path: 'design'}, {path: 'designer'}]}).exec();
    if (!user) throw 'MISMATCH_AUTH';

    let unavailableReleases = false;

    for (let i = user.cart.items.length - 1; i >= 0; i--) {
      if (!user.cart.items[i].release) {
        user.cart.items.splice(i, 1);
        unavailableReleases = true;
      }
    }

    const taxReq = await taxJar.calculate(user.cart.subtotal, user.cart.shipping);
    user.cart.tax = taxReq.tax;
    user.cart.taxRate = taxReq.rate;

    if (user.cart.shipping.cost <= 1 && user.cart.items.length > 0 && user.cart.shipping.valid && user.cart.subtotal < 25) {
      user.cart.shipping.cost = (await CalculateShipping(user)).cost;
      const taxReq = await taxJar.calculate(user.cart.subtotal, user.cart.shipping);
      user.cart.tax = taxReq.tax;
      user.cart.taxRate = taxReq.rate;
    }

    if (unavailableReleases) {
      user.cart.subtotal = user.cart.items.reduce((total, item) => total + (item.quantity * item.release.price), 0);
      user.cart.subtotal = Math.ceil(user.cart.subtotal * 100) / 100;
      const taxReq = await taxJar.calculate(user.cart.subtotal, user.cart.shipping);
      user.cart.tax = taxReq.tax;
      user.cart.taxRate = taxReq.rate;
    }

    let last4 = '';

    if (user.payment.source) {
      source = await stripe.customers.retrieveSource(user.payment.customer, user.payment.source);
      last4 = source.last4 + " " + source.brand;
    }

    await user.save();

    return respond({
      body: {
        _c: user.cart.items.map((item) => {
            return {
              id: item._id,
              release: item.release.prettyFormat(user.favorites),
              quantity: item.quantity,
              size: item.size,
              color: Colors(item.release.design.type, item.color, true),
              total: (item.quantity * item.release.price).toFixed(2)
            }
        }),
        _p: user.payment.source && user.payment.source !== "",
        _sd: {
          v: user.cart.shipping.valid,
          _sa: user.cart.shipping.valid ? user.cart.shipping.streetAddress : null,
          _sac: user.cart.shipping.valid ? user.cart.shipping.streetAddressCont : null,
          _c: user.cart.shipping.valid ? user.cart.shipping.city : null,
          _s: user.cart.shipping.valid ? user.cart.shipping.state : null,
          _p: user.cart.shipping.valid ? user.cart.shipping.postal : null,
          _cn: user.cart.shipping.valid ? user.cart.shipping.country : null
        },
        _last4: last4,
        _st: user.cart.subtotal.toFixed(2),
        _tx: (user.cart.tax || 0).toFixed(2),
        _txA: (user.cart.taxRate * 100).toString(),
        _s: user.cart.shipping.cost.toFixed(2),
        _t: (user.cart.subtotal + user.cart.tax + user.cart.shipping.cost).toFixed(2)
      }
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function LoadHistory(data) {
  const crypto = data.crypto;

  try {
    const user = await User.findOne({crypto: crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const orders = await Order.find({buyer: user._id}).sort('-createdAt').exec();

    return respond({
      body: {
        _h: orders.map(order => order.quickFormat())
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function LoadTrack(data, orderNumber) {
  const crypto = data.crypto;

  try {
    let orderIdentifier = orderNumber.replace(/\D+ /gi, "");
    if (orderIdentifier === '') throw 'INEX_ORDER';

    const user = await User.findOne({crypto: crypto, deleted: false}).exec();
    if (!user) throw 'MISMATCH_AUTH';
    const order = await Order.findOne({buyer: user._id, serial: orderIdentifier}).populate("details.design").populate("details.seller").exec();
    if (!order) throw 'INEX_ORDER';

    return respond({
      body: {
        _d: {
          s: order.cost.subtotal.toFixed(2),
          t: order.cost.tax.toFixed(2),
          tP: (order.cost.taxRate * 100).toFixed(2),
          sP: (order.cost.shipping === 0 ? "FREE" : order.cost.shipping.toFixed(2)),
          tC: order.cost.total.toFixed(2)
        },
        _t: order.shipping.tracking || '',
        _c: order.shipping.carrier,
        _delivered: order.wasDelivered(),
        _i: order.prettyItems(),
        _u: order.prettyUpdates()
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

function onCatch(e) {
  let code = 500;
  const catchCases = ['DUP_NUMBER', 'AGE_ERR', 'PASS_LEN', 'INVALID_NUM', 'INVALID_PASS', 'TRINUM_ERROR', 'INEX_BRAND', 'UNVERIFIED_BRAND', 'STRIPE_ERROR', 'INEX_DESIGN', 'NO_CONNECT_SETUP', 'BAD_MODE', 'BAD_METRICS', 'BAD_TYPE', 'MERCH_RELEASED', 'NOT_RELEASED', 'INEX_RELEASE', 'BAD_SHIP', 'PAYMENT_ERROR', 'INEX_LIM_RELEASE', 'INEX_ORDER', 'NO_PRICING', 'BAD_SHIPPING', 'NO_MODEL'];

  if (e === 'MISMATCH_AUTH') {
    code = 401;
  } else if (catchCases.includes(e)) {
    code = 200;
  }

  return respond({
    isError: true,
    code: code,
    error: e
  });
}

function respond({isError = false, code = 200, error = null, message = isError ? 'API encountered error' : 'API finished task successfully', body = {}} = {}) {
  return {
    _hE: isError,
    _c: code,
    _e: error,
    _m: message,
    _body: body
  }
}
