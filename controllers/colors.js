const Colors = {
  shirtT: [
    { label: 'Ebb', prime: 'Natural', color: '#F2EEED', value: 'BB' },
    { label: 'Black', prime: 'Negro', color: '#212121', value: 'BLCK' },
    { label: 'White', prime: 'Blanco', color: '#FFF', value: 'WHTT' },
    { label: 'Victoria', prime: 'Acometida Purpura', color: '#503C7B', value: 'VCTR' },
    { label: 'Vanilla', prime: 'Arena', color: '#CCBCA2', value: 'VNLL' },
    { label: 'Heather', prime: 'Azul Claro', color: '#C0CDD6', value: 'HTHR' },
    { label: 'Deep Sapphire', prime: 'Azul Fresco', color: '#09436D', value: 'DPSPPHR' },
    { label: 'Bondi', prime: 'Azul Tahiti', color: '#01A3C5', value: 'BNDBL' },
    { label: 'Celtic', prime: 'Bosque Verde', color: '#123022', value: 'CLTC' },
    { label: 'Claret', prime: 'Cardenal', color: '#74192B', value: 'CLRT' },
    { label: 'Cocoa Brown', prime: 'Cholocate Negro', color: '#382926', value: 'CCBRWN' },
    { label: 'Wafer', prime: 'Crema', color: '#DED1C1', value: 'WFR' },
    { label: 'Wheat', prime: 'Cream de Platano', color: '#F3E3B2', value: 'WHT' },
    { label: 'French Gray', prime: 'Cuero Gris', color: '#B7B7C1', value: 'FRNCHGRY' },
    { label: 'Buccaneer', prime: 'Granate', color: '#562828', value: 'BCCNR' },
    { label: 'Coffee', prime: 'Gris Calido', color: '#6A6258', value: 'CFF' },
    { label: 'Dawn', prime: 'Gris Claro', color: '#ABA8A1', value: 'DWN' },
    { label: 'Bluewood', prime: 'Indigo', color: '#2F3C54', value: 'PCKLDBLWD' },
    { label: 'Salem', prime: 'Kelly Verde', color: '#0E864C', value: 'SLM' },
    { label: 'Ebony Clay', prime: 'Marina Medianoche', color: '#212733', value: 'BNYCLY' },
    { label: 'Tundora', prime: 'Metal Pesado', color: '#424242', value: 'TNDR' },
    { label: 'Blaze Orange', prime: 'Naranja Clasica', color: '#FF5D02', value: 'BLZRNG' },
    { label: 'Pearl Bush', prime: 'Harina Avena', color: '#E4DACE', value: 'PRLBSH' },
    { label: 'Mine Shaft', prime: 'Negro Grafito', color: '#292929', value: 'MNSHFT' },
    { label: 'Makara', prime: 'Olivia Claro', color: '#8B816B', value: 'MKR' },
    { label: 'Yellow Sea', prime: 'Oro', color: '#FBA70A', value: 'YLLWS' },
    { label: 'Biscay', prime: 'Real', color: '#1D4175', value: 'BSCY' },
    { label: 'Monza', prime: 'Rojo', color: '#AF012F', value: 'MNZ' },
    { label: 'Pink', prime: 'Rosa Claro', color: '#FFC5C4', value: 'PNK' },
    { label: 'Cerulean', prime: 'Turquesa', color: '#02A7CB', value: 'CRLN' },
    { label: 'Finch', prime: 'Verde Militar', color: '#585644', value: 'FNCH' }
  ],
  shirtTsleeve: [
    { label: 'Black', color: '#212121', value: 'BLCK', },
    { label: 'White', color: '#FFF', value: 'WHT', },
  ],
  shirtTxB: [
    { label: 'Black', color: '#212121', value: 'BLCK'},
    { label: 'White', color: '#FFFFFF', value: 'WHT'},
    { label: 'Denim', color: '#2262CB', value: 'DNM'},
    { label: 'Cadmium', color: '#E79F2f', value: 'CDMM'},
    { label: 'Lust', color: '#E72224', value: 'LST'}
  ],
  hoodie: [
    { label: 'Black', prime: 'Black', color: '#212121', value: 'BLCK' },
    { label: 'White', prime: 'White', color: '#FFFFFF', value: 'WHT' },
    { label: 'Jacarta', prime: 'Purple', color: '#3C2B61', value: 'JCRT' },
    { label: 'Hippie Green', prime: 'Irish Green', color: '#5D8F54', value: 'HPPGRN' },
    { label: 'Burnt Sienna', prime: 'Tennessee Orange', color: '#E4863E', value: 'BRNTSNN' },
    { label: 'Jaffa', prime: 'Security Orange', color: '#F27B37', value: 'JFF' },
    { label: 'Mexican Red', prime: 'Red', color: '#A91F2E', value: 'MXCNRD' },
    { label: 'Livid Brown',  prime: 'Granate', color: '#512836', value: 'LVDBRWN' },
    { label: 'Cararra', prime: 'Ash', color: '#F2F3EE', value: 'CRRR' },
    { label: 'Sepia Skin', prime: 'Texas Orange', color: '#9C5C38', value: 'SPSKN' },
    { label: 'Silver Chalice', prime: 'Sport Silver', color: '#9D9D9D', value: 'SLVRCHLC' },
    { label: 'Canary', prime: 'Safety Green', color: '#E5F767', value: 'CNRY' },
    { label: 'Chambray', prime: 'Real Blue', color: '#3B589E', value: 'CHMBRY' },
    { label: 'Punch', prime: 'Orange', color: '#D84F2D', value: 'PNCH' },
    { label: 'Charade', prime: 'Armada', color: '#2F313E', value: 'CHRD' },
    { label: 'Mineral Green', prime: 'Forest', color: '#485A5A', value: 'MNRLGRN' },
    { label: 'Polo Blue', prime: 'Carolina Blue', color: '#839CD2', value: 'PLBL' },
    { label: 'Tawny Port', prime: 'Cardinal', color: '#80273B', value: 'TWNYPRT' },
    { label: 'Dust Storm', prime: 'Melocoton', color: '#F2CECE', value: 'DSTSTRM'},
    { label: 'Peach Orange', prime: 'Peach Banana', color: '#FFD08A', value: 'PCHRNG'},
    { label: 'Lavender', prime: 'Lavender', color: '#DCC9E0', value: 'LNGDLVNDR'},
    { label: 'Powder Blue', prime: 'Mint', color: '#B6EBE3', value: 'PWDRBL'},
    { label: 'Baby Pink', prime: 'Light Pink', color: '#F1CDD7', value: 'BBPNK'}
  ],
  crewneck: [
    { label: 'Black', prime: 'Black', color: '#212121', value: 'BLCK' },
    { label: 'Neon Fuchsia', prime: 'Paprika', color: '#FD4863', value: 'NNFCHS', },
    { label: 'Byzantium', prime: 'Plum', color: '#763670', value: 'BZNTM', },
    { label: 'Crimson Glory', prime: 'Antique Cherry Red', color: '#B7122F', value: 'CRMSNGLR', },
    { label: 'Mint', prime: 'Mint', color: '#CDE5CB', value: 'MNT', },
    { label: 'Orange', prime: 'Orange', color: '#FD5931', value: 'SFTRNG', },
    { label: 'White', prime: 'White', color: '#F9F9F9', value: 'WHT' },
    { label: 'Ash', prime: 'Ash', color: '#CFCDC9', value: 'SH' },
    { label: 'Carmine', prime: 'Cardinal Red', color: '#93002C', value: 'CMN' },
    { label: 'Carolina Blue', prime: 'Carolina Blue', color: '#A2BCD7', value: 'SKBL' },
    { label: 'Pastel Orange', prime: 'Gold', color: '#FEB43D', value: 'PSTLRNG' },
    { label: 'French Rose', prime: 'Heliconia', color: '#EE4B8A', value: 'FRNCHRS' },
    { label: 'Shamrock Green', prime: 'Irish Green', color: '#0AA264', value: 'SHMRCKGRN' },
    { label: 'Asparagus', prime: 'Kiwi', color: '#8FAC6C', value: 'SPRGS' },
    { label: 'Iceberg', prime: 'Light Blue', color: '#81A7DF', value: 'CBRG' },
    { label: 'Lavender', prime: 'Orchid', color: '#DDCBE7', value: 'LVNDR' },
    { label: 'Regalia', prime: 'Purple', prime: 'Plume', color: '#4E3B75', value: 'RGL' },
    { label: 'Lapis Lazuli', prime: 'Royal', color: '#295FA9', value: 'LPSZL' },
    { label: 'Midori ', prime: 'Safety Green', color: '#E9F67E', value: 'MDR'},
    { label: 'Carrot', prime: 'Safety Orange', color: '#F06821', value: 'CRRT'},
    { label: 'Flamingo', prime: 'Safety Pink', color: '#FD8CAD', value: 'FLMNG'},
    { label: 'Pale Silver', prime: 'Sand', color: '#CEC1AF', value: 'PLSLVR'},
    { label: 'Blue Yonder', prime: 'Violet', color: '#A9AED6', value: 'BLYNDR'}
  ],
  beanie: [
    { label: 'Black', prime: 'Black', color: '#212121', value: 'BLCK' }
  ],
  cap: [
    {label: 'Black', color: '#212121', value: 'BLCK'},
    {label: 'White', color: '#ffffff', value: 'WHT'}
  ],
  popsocket: [
    {label: 'White', color: '#fff', value: 'WHT'},
    {label: 'Black', color: '#212121', value: 'BLCK'},
    {label: 'Monza', color: '#d50332', value: 'MNZ'},
    {label: 'Astronaut Blue', color: '#013B5B', value: 'STRNTBL'},
    {label: 'Prelude', color: '#d6c0dd', value: 'PRLD'},
    {label: 'Illusion', color: '#f4a6d6', value: 'ILLSN'}
  ],
  shoe_AF: [
    {label: 'White', color: '#fff', value: 'WHT'}
  ],
  pillow: [
    {label: 'White', color: '#fff', value: 'WHT'},
    {label: 'Black', color: '#212121', value: 'BLCK'}
  ],
  mug: [
    {label: 'White', color: '#fff', value: 'WHT'}
  ],
  keychain: [
    {label: 'Silver', color: '#555', value: 'SLVR'}
  ]
};

module.exports = function(type, color, labelize = false, availability = false, prime = false) {
  if (availability) {
    return Colors[type].find((elem) => elem.value == color) ? [type] : [];
  }

  if (labelize) {
    let c = (Colors[type].find((elem) => elem.value == color) || {label: 'TBD', prime: null});
    if (prime) {
      return c.prime;
    }
    return c.label;
  }

  return (Colors[type].find((elem) => elem.value == color) || {color: '#TBD'}).color.substr(1);
}
