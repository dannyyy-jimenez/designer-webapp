/*

  x: ,
  y: ,
  width: ,
  height:

*/

module.exports = {
  shirtT: {
    front: ['fullfrontal', 'mediumfront', 'centerfront', 'acrosschest', 'leftchest', 'rightchest', 'leftvertical', 'rightvertical', 'bottomleft', 'bottomright'],
    fullfrontal: {
      width: 320,
      x: 240,
      y: 192,
      height: 393
    },
    mediumfront: {
      width: 213,
      x: 293,
      y: 212,
      height: 213
    },
    centerfront: {
      width: 133,
      x: 333,
      y: 212,
      height: 133
    },
    acrosschest: {
      x: 240,
      y: 192,
      width: 320,
      height: 112
    },
    leftchest: {
      x: 427,
      y: 192,
      width: 133,
      height: 133
    },
    rightchest: {
      x: 240,
      y: 192,
      width: 133,
      height: 133
    },
    leftvertical: {
      x: 427,
      y: 192,
      width: 133,
      height: 393
    },
    rightvertical: {
      x: 240,
      y: 192,
      width: 133,
      height: 393
    },
    bottomleft: {
      x: 400,
      y: 625,
      width: 160,
      height: 53
    },
    bottomright: {
      x: 240,
      y: 625,
      width: 160,
      height: 53
    },

    back: ['fullback', 'mediumback', 'lockerpatch', 'acrossback', 'fullbottomback'],
    fullback: {
      x: 240,
      y: 131,
      width: 320,
      height: 450
    },
    mediumback: {
      x: 293,
      y: 163,
      width: 213,
      height: 213
    },
    lockerpatch: {
      x: 347,
      y: 131,
      width: 106,
      height: 106
    },
    acrossback: {
      x: 240,
      y: 131,
      width: 320,
      height: 106
    },
    fullbottomback: {
      x: 240,
      y: 579,
      width: 320,
      height: 106
    },

    left: ['leftsleeve'],
    leftsleeve: {
      x: 355,
      y: 198,
      width: 93,
      height: 93
    },

    right: ['rightsleeve'],
    rightsleeve: {
      x: 353,
      y: 198,
      width: 93,
      height: 93
    }
  },
  shirtTsleeve: {
    front: ['fullfrontal', 'mediumfront', 'centerfront', 'acrosschest', 'leftchest', 'rightchest', 'leftvertical', 'rightvertical', 'leftuppershoulder', 'rightuppershoulder', 'leftbottomarm', 'rightbottomarm', 'leftbottomwrist', 'rightbottomwrist', 'bottomleft', 'bottomright'],
    fullfrontal: {
      x: 261,
      y: 192,
      width: 260,
      height: 380
    },
    mediumfront: {
      x: 291,
      y: 225,
      width: 200,
      height: 200
    },
    centerfront: {
      x: 341,
      y: 225,
      width: 100,
      height: 100
    },
    acrosschest: {
      x: 261,
      y: 192,
      width: 260,
      height: 100
    },
    leftchest: {
      x: 291,
      y: 225,
      width: 200,
      height: 200
    },
    rightchest: {
      x: 446,
      y: 202,
      width: 90,
      height: 90
    },
    leftvertical: {
      x: 421,
      y: 192,
      width: 100,
      height: 380
    },
    rightvertical: {
      x: 261,
      y: 192,
      width: 100,
      height: 380
    },
    leftuppershoulder: {
      x: 590,
      y: 305,
      width: 40,
      height: 120
    },
    rightuppershoulder: {
      x: 141,
      y: 305,
      width: 40,
      height: 120
    },
    leftbottomarm: {
      x: 631,
      y: 498,
      width: 40,
      height: 120
    },
    rightbottomarm: {
      x: 119,
      y: 498,
      width: 40,
      height: 120
    },
    leftbottomwrist: {
      x: 651,
      y: 640,
      width: 30,
      height: 30
    },
    rightbottomwrist: {
      x: 121,
      y: 640,
      width: 30,
      height: 30
    },
    bottomleft: {
      x: 391,
      y: 600,
      width: 130,
      height: 50
    },
    bottomright: {
      x: 261,
      y: 600,
      width: 130,
      height: 50
    },

    left: ['lefthand', 'leftsleeve', 'leftshoulder'],
    lefthand: {
      x: 370,
      y: 643,
      width: 60,
      height: 60
    },
    leftsleeve: {
      x: 384,
      y: 201,
      width: 85,
      height: 170
    },
    leftshoulder: {
      x: 384,
      y: 201,
      width: 85,
      height: 85
    },

    right: ['righthand', 'rightsleeve', 'rightshoulder'],
    righthand: {
      x: 370,
      y: 638,
      width: 60,
      height: 60
    },
    rightsleeve: {
      x: 328,
      y: 206,
      width: 85,
      height: 170
    },
    rightshoulder: {
      x: 328,
      y: 206,
      width: 85,
      height: 85
    },

    back: ['fullback', 'mediumback', 'lockerpatch', 'acrossback', 'fullbottomback', 'leftwrist', 'leftelbow', 'leftupperbackshoulder', 'rightwrist', 'rightelbow', 'rightupperbackshoulder'],
    fullback: {
      x: 278,
      y: 155,
      width: 260,
      height: 410
    },
    mediumback: {
      x: 308,
      y: 198,
      width: 200,
      height: 200
    },
    lockerpatch: {
      x: 358,
      y: 155,
      width: 100,
      height: 100
    },
    acrossback: {
      x: 278,
      y: 155,
      width: 260,
      height: 100
    },
    fullbottomback: {
      x: 278,
      y: 586,
      width: 260,
      height: 100
    },
    leftwrist: {
      x: 111,
      y: 636,
      width: 40,
      height: 40
    },
    rightwrist: {
      x: 650,
      y: 636,
      width: 40,
      height: 40
    },
    leftelbow: {
      x: 140,
      y: 429,
      width: 40,
      height: 40
    },
    rightelbow: {
      x: 636,
      y: 429,
      width: 40,
      height: 40
    },
    leftupperbackshoulder: {
      x: 150,
      y: 265,
      width: 60,
      height: 120
    },
    rightupperbackshoulder: {
      x: 606,
      y: 265,
      width: 60,
      height: 120
    },
  },
  beanie: {
    front: ['patch'],
    front: {
      x: 275,
      y: 515,
      width: 248,
      height: 163
    }
  },
  hoodie: {
    front: ['fullfrontal', 'mediumfront', 'centerfront', 'acrosspocket', 'leftchest', 'rightchest', 'leftwrist', 'leftsleeve', 'leftshoulder', 'rightwrist', 'rightsleeve', 'rightshoulder'],
    fullfrontal: {
      x: 284,
      y: 228,
      width: 230,
      height: 247
    },
    mediumfront: {
      x: 323,
      y: 272,
      width: 158,
      height: 158
    },
    centerfront: {
      x: 366,
      y: 319,
      width: 70,
      height: 70
    },
    acrosspocket: {
      x: 308,
      y: 545,
      width: 167,
      height: 112
    },
    leftchest: {
      x: 455,
      y: 272,
      width: 73,
      height: 73
    },
    rightchest: {
      x: 272,
      y: 272,
      width: 73,
      height: 73
    },
    leftwrist: {
      x: 604,
      y: 581,
      width: 55,
      height: 55
    },
    leftsleeve: {
      x: 586,
      y: 414,
      width: 60,
      height: 120
    },
    leftshoulder: {
      x: 594,
      y: 326,
      width: 55,
      height: 55
    },
    rightwrist: {
      x: 129,
      y: 581,
      width: 55,
      height: 55
    },
    rightsleeve: {
      x: 152,
      y: 414,
      width: 60,
      height: 120
    },
    rightshoulder: {
      x: 166,
      y: 318,
      width: 55,
      height: 55
    },

    back: ['fullback', 'mediumback', 'leftbackwrist', 'leftbacksleeve', 'leftbackshoulder', 'rightbackwrist', 'rightbacksleeve', 'rightbackshoulder'],
    fullback: {
      x: 250,
      y: 172,
      width: 300,
      height: 400
    },
    mediumback: {
      x: 293,
      y: 237,
      width: 213,
      height: 213
    },
    leftbackwrist: {
      x: 627,
      y: 618,
      width: 65,
      height: 65
    },
    leftbacksleeve: {
      x: 620,
      y: 412,
      width: 65,
      height: 130
    },
    leftbackshoulder: {
      x: 601,
      y: 279,
      width: 65,
      height: 65
    },
    rightbackwrist: {
      x: 109,
      y: 618,
      width: 65,
      height: 65
    },
    rightbacksleeve: {
      x: 116,
      y: 412,
      width: 65,
      height: 130
    },
    rightbackshoulder: {
      x: 133,
      y: 279,
      width: 65,
      height: 65
    }
  },
  crewneck: {
    front: ['fullfrontal', 'mediumfront', 'centerfront', 'leftchest, rightchest'],
    fullfrontal: {
      x: 253,
      y: 238,
      width: 270,
      height: 330
    },
    mediumfront: {
      x: 308,
      y: 291,
      width: 160,
      height: 160
    },
    centerfront: {
      x: 348,
      y: 331,
      width: 80,
      height: 80
    },
    leftchest: {
      x: 468,
      y: 238,
      width: 80,
      height: 80
    },
    rightchest: {
      x: 228,
      y: 238,
      width: 80,
      height: 80
    },

    back: ['fullback', 'mediumback', 'lockerpatch', 'acrossback', 'fullbottomback', 'rightwrist', 'leftwrist'],
    fullback: {
      x: 263,
      y: 200,
      width: 275,
      height: 350
    },
    mediumback: {
      x: 300,
      y: 275,
      width: 200,
      height: 200
    },
    lockerpatch: {
      x: 360,
      y: 200,
      width: 80,
      height: 80
    },
    acrossback: {
      x: 263,
      y: 200,
      width: 275,
      height: 80
    },
    fullbottomback: {
      x: 263,
      y: 470,
      width: 275,
      height: 80
    },
    rightwrist: {
      x: 661,
      y: 450,
      width: 60,
      height: 60
    },
    leftwrist: {
      x: 80,
      y: 450,
      width: 60,
      height: 60
    }
  },
  cap: {
    front: ['frontal'],
    frontal: {
      x: 263,
      y: 300,
      width: 275,
      height: 100
    }
  },
  popsocket: {
    front: ['fullfrontal'],
    fullfrontal: {
      x: 256,
      y: 256,
      scale: 'fill',
      radius: "max",
      width: 288,
      height: 288,
    }
  },
  pillow: {
    front: ['fullfrontal'],
    fullfrontal: {
      width: 475,
      height: 430,
      crop: 'scale'
    }
  },
  mug: {
    right: ['rightfullfrontal', 'rightcenterfront'],
    rightfullfrontal: {
      x: 289,
      y: 247,
      width: 300,
      height: 280
    },
    rightcenterfront: {
      x: 339,
      y: 287,
      width: 200,
      height: 200
    },

    left: ['leftfullfrontal', 'leftcenterfront'],
    leftfullfrontal: {
      x: 266,
      y: 286,
      width: 300,
      height: 280
    },
    leftcenterfront: {
      x: 216,
      y: 246,
      width: 200,
      height: 200
    }
  }
}
