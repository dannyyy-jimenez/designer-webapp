const cloudinary = require('cloudinary').v2;
cloudinary.config({
  cloud_name: 'lgxy',
  api_key: process.env.CLOUDINARY_KEY,
  api_secret: process.env.CLOUDINARY_SECRET
});
const Colors = require('./colors');
const easypost = require('./EasyPost');
const crypto = require('crypto');
const twilio = require('twilio')(process.env.TWILIO_SID, process.env.TWILIO_AUTH);
const stripe = require('stripe')(process.env.STRIPE_SECRET);
const Notifications = require('./Notifications');
const moment = require('moment');

const Admin = require('../models/admin');

module.exports = {
  CalculatePrice: CalculatePrice,
  approval: Approval,
  shipping: Shipping,
  order: Order,
  payout: Payout
};

async function CalculatePrice(royalty, type, production = 0) {
  let DEF = 4.5;
  let multiplier = 0.1;

  if (type === 'hoodie') {
    DEF = 5.5;
  }
  if (type === 'shirtTxB') {
    DEF = 10;

    return royalty + DEF + production;
  }
  if (type === 'shoe_AF') {
    DEF = 115;
  }

  const Tailori = DEF + (Math.round((royalty * multiplier) * 100) / 100);

  return royalty + Tailori + production;
}

async function Approval(release, tokenSet = null) {
  const day = moment().date();
  const month = moment().month() + 1;
  const designers = await Admin.find({permissions: 'trinum'}).exec();
  if (release.design.mode === 'experimental') {
    return "TLRI*EXP*" + ((month < 10 ? "0"+month.toString() : month.toString()) + day.toString()) + crypto.randomBytes(5).toString("hex").toUpperCase();
  }
  const token = tokenSet && tokenSet !== 'TRNM*TLRI*' ? tokenSet : "TRNM*TLRI*" + ((month < 10 ? "0"+month.toString() : month.toString()) + day.toString()) + crypto.randomBytes(5).toString("hex").toUpperCase();
  let message =  `Tailori: Design Approval Needed #${token}`;

  await Promise.all(release.design.metrics.sides.map(async (metric) => {
    message += `\n\nArtwork: ${await cloudinary.url(metric.artwork)}\nSide: ${metric.side}`
  }))

  designers.forEach(designer => {
    twilio.messages.create({
      body: message,
      from: process.env.TWILIO_NUMBER,
      to: designer.phoneNumber
    });
  })
  Notifications.send(designers.map(designer => designer.sockets).reduce((aggregate, next) => [...aggregate, ...next], []), {body: `Design Approval Needed #${token}!`, data: {}});
  return token;
}

async function Shipping(address, items) {
  try {
    const {cost, id} = await easypost.shipment(address, items);

    return { isError: false, cost, id};
  } catch(e) {
    console.log(e);
    return { isError: true, cost: 0, id: null }
  }
}

async function Payout(payout, token) {
  if (payout === 0) return;

  await stripe.transfers.create({
    amount: parseInt(payout * 100),
    currency: 'usd',
    destination: process.env.STRIPE_TRINUM_DESIGN,
    transfer_group: token
  })
}

async function Order(payout) {
  const token = "TRNM*" + crypto.randomBytes(8).toString("hex") + "*TLRI*" + crypto.randomBytes(6).toString("hex");

  try {
    if (payout === 0) {
      return "TLRI*" + crypto.randomBytes(14).toString("hex");
    };

    const designers = await Admin.find({permissions: 'trinum'}).exec();
    await stripe.transfers.create({
      amount: parseInt(payout * 100),
      currency: 'usd',
      destination: process.env.STRIPE_TRINUM_DESIGN,
      transfer_group: token
    })
    Notifications.send(designers.map(designer => designer.sockets).reduce((aggregate, next) => [...aggregate, ...next], []), {body: `New order #${token}!`, data: {}});

    return token;
  } catch(e) {
    return token;
  }
}
