const crypto = require('crypto');
const twilio = require('twilio')(process.env.TWILIO_SID, process.env.TWILIO_AUTH);
const cloudinary = require('cloudinary').v2;
const cloudinaryFunctions =  require('./Cloudinary');
const moment = require('moment');
const stripe = require('stripe')(process.env.STRIPE_SECRET);
const Notifications = require('./Notifications');
const { Expo } = require('expo-server-sdk');
const Colors = require('./colors');
const pathLib = require('path');
const taxJar = require('./TaxJar');
const trinumDesign = require('./TrinumDesign');

// MODELS
const Release = require('../models/release');
const User = require('../models/user');
const Brand = require('../models/brand');
const Design = require('../models/design');
const Order = require('../models/order');
const Admin = require('../models/admin');
const Model = require('../models/model');

cloudinary.config({
  cloud_name: 'lgxy',
  api_key: process.env.CLOUDINARY_KEY,
  api_secret: process.env.CLOUDINARY_SECRET
});

module.exports.order = PlaceOrder;

// get
module.exports.load = {
  store: LoadStore,
  brand: LoadBrand,
  zoom: LoadZoom,
  colorChange: LoadColorChange,
  bag: LoadBag,
  pay: LoadPay
};

module.exports.metadata = {
  brand: MetadataBrand,
  release: MetadataRelease,
  formatShot: MetadataShot
}

async function MetadataBrand(identifier) {
  try {
    let brand = await Brand.findOne({identifier: identifier}).exec();
    if (!brand) throw 'no_brand';

    return brand;
  } catch(e) {
    return "Tailori";
  }
}

async function MetadataRelease(identifier) {
  try {
    let release = await Release.findOne({identifier: identifier}).populate('design').populate('designer').exec();
    if (!release) throw 'no_brand';

    return release;
  } catch(e) {
    return "Tailori";
  }
}

function MetadataShot(shot) {
  return cloudinary.url(shot, {secure: true, height: 627, width: 1200, crop: 'pad', background: '#DB113B'});
}

async function LoadStore(key, params) {
  try {
    let type = params.type;

    let releases = await Release.find({approved: true, printable: true}).sort({updatedAt: -1}).populate('design').populate('designer').exec();

    releases = releases.filter(release => release.design.type.includes(type));

    return respond({
      body: {
        _r: releases.map((release) => release.prettyFormat([]))
      }
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function LoadBrand(key, identifier) {
  try {
    const brand = await Brand.findOne({identifier: identifier}).populate({path: 'releases', populate: ['design', 'designer']}).exec();
    if (!brand) throw 'INEX_BRAND';

    brand.releases = brand.releases.filter(release => release.approved && release.printable);

    return respond({
      body: brand.profilize([])
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function LoadZoom(key, releaseID) {
  try {
    const release = await Release.findOne({identifier: releaseID, approved: true, printable: true}).populate('design').populate('designer').exec();
    if (!release) throw 'INEX_RELEASE';

    return respond({
      body: {
        _r: release.prettyFormat([]),
        _bc: release.design.metrics.base
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function LoadColorChange(key, releaseID, color) {
  try {
    const release = await Release.findOne({identifier: releaseID, approved: true, printable: true}).populate('design').populate('designer').exec();
    if (!release) throw 'INEX_RELEASE';

    let shots = [];

    shots = await cloudinaryFunctions.GenerateColorChangeShots(release.design.metrics.sides, release.design.type, color);

    return respond({
      body: {
        _s: shots
      }
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function LoadBag(key, bag, raw = false) {
  try {
    const identifiers = [];
    const cartRaw = (bag).split("$").map((item) => {
      let parts = item.split("_");

      let formatted = {
        identifier: "",
        size: 'default',
        color: 'default',
        quantity: 1,
        valid: false
      };

      for (let part of parts) {
        if (part.slice(0,1) === '#') {
          formatted.quanitity = part.slice(1);
        } else if (part.slice(0,1) === '@') {
          formatted.size = part.slice(1);
        } else if (part.slice(0,1) === '&') {
          formatted.color = part.slice(1);
        } else {
          formatted.identifier = part;
          identifiers.push(formatted.identifier);
        }
      }

      return formatted;
    });

    const releases = await Release.find({identifier: identifiers}).populate('design').populate('designer').exec();

    let formattedBag = [];

    for (let item of cartRaw) {
      let release = releases.find(raw => raw.identifier === item.identifier);
      if (release && raw) {
        formattedBag.push({
          release: release,
          quantity: item.quantity,
          size: item.size,
          color: item.color === 'default' ? release.design.metrics.base : item.color,
          total: item.quantity * release.price
        });
      } else if (release) {
        formattedBag.push({
          release: release.prettyFormat([]),
          quantity: item.quantity,
          size: item.size,
          color: item.color === 'default' ? release.design.metrics.base : item.color,
          total: (item.quantity * release.price).toFixed(2)
        });
      }
    }

    if (raw) {
      return formattedBag;
    }

    return respond({
      body: {
        _b: formattedBag,
        _t: formattedBag.reduce((total, item) => total + parseFloat(item.total), 0).toFixed(2)
      }
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function CalculateShipping(name, streetAddress, streetAddressCont, city, state, postal, country, bag) {
  let shipping = {
    name: name,
    street1: streetAddress,
    street2: streetAddressCont,
    city: city,
    state: state,
    zip: postal,
    country: country
  };
  let items = bag.map((item) => {
    return {
      type: item.release.design.type,
      quantity: item.quantity
    }
  });
  const shippingRequest = await trinumDesign.shipping(shipping, items);
  if (shippingRequest.isError) throw 'TRINUM_ERROR';

  return {cost: shippingRequest.cost, id: shippingRequest.id};
}

async function GetDiscount(scheme, items) {
  let cost = items.reduce((total, next) => total + next.release.price * next.quantity, 0);

  if (scheme === 'minthirty' && cost >= 30) {
     return 3;
  } else if (scheme === 'minfifty' && cost >= 50) {
    return 6;
  } else if (scheme === 'multiplier') {
    return Math.round(cost * 0.05 * 100) / 100;
  }

  return 0;
}

async function LoadPay(key, query) {
  const bag = query.bag;
  const name = query.name;
  const streetAddress = query.bag;
  const streetAddressCont = query.bag;
  const city = query.city;
  const state = query.state;
  const postal = query.postal;
  const country = query.country;
  const modelCode = query.modelCode;

  try {

    const formattedBag = await LoadBag(key, bag, true);

    let subtotal = formattedBag.reduce((total, item) => total + (item.quantity * item.release.price), 0);
    subtotal = Math.ceil(subtotal * 100) / 100;
    let shipping = (await CalculateShipping(name, streetAddress, streetAddressCont, city, state, postal, country, formattedBag)).cost;
    let discount = 0;

    if (modelCode !== "" && modelCode !== "TDTMNK") {
      const model = await Model.findOne({identifier: modelCode});
      if (model) {
        if (model.scheme !== 'default') {
          discount = await GetDiscount(model.scheme, formattedBag);
        }

        subtotal -= discount;

        if (model.scheme === 'default') {
         discount = Math.round((shipping - 1) * 100) / 100;
         shipping = 1;
        }
      }
    }

    if (modelCode === "TDTMNK") {
      shipping = 0;
    }

    if (subtotal >= 25) {
      shipping = 0;
    }

    const taxReq = await taxJar.calculate(subtotal, {streetAddress, city, postal, country, state, cost: shipping});
    let tax = taxReq.tax;
    let taxRate = taxReq.rate;

    return respond({
      body: {
        _d: discount.toFixed(2),
        _st: subtotal.toFixed(2),
        _s: shipping.toFixed(2),
        _tx: tax.toFixed(2),
        _ta: taxRate * 100,
        _t: (subtotal + shipping + tax).toFixed(2)
      }
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function GetModelPayout(scheme, items) {
  let itemsAmount = items.reduce((total, next) => total + next.quantity, 0);
  let cost = items.reduce((total, next) => total + (next.quantity * next.release.price), 0);

  if (scheme === 'default') {
    return 2;
  } else if (scheme === 'minthirty') {
    return Math.floor(cost / 30) * 4;
  } else if (scheme === 'minfifty') {
    return Math.floor(cost / 50) * 8;
  }

  return 2 * itemsAmount;
}

function CreateSerial() {
  const day = moment().date();
  const month = moment().month() + 1;
  let number = Math.floor(Math.random() * (99999999 - 1)).toString();

  for (let i = 0; i < 8 - number.length; i++) {
    number = "0" + number;
  }

  return (month < 10 ? "0"+month.toString() : month.toString()) + day.toString() + number;
}

async function PlaceOrder(body) {
  const key = body.key;
  const paymentToken = body.paymentToken;
  const rawTotal = body.total;
  const bag = body.bag;
  const name = body.name;
  const phoneNumber = body.phoneNumber.replace(/\D/g,'');
  const streetAddress = body.streetAddress;
  const streetAddressCont = body.streetAddressCont;
  const city = body.city;
  const state = body.state;
  const postal = body.postal;
  const country = body.country;
  const modelCode = body.modelCode;

  try {
    const formattedBag = await LoadBag(key, bag, true);

    let subtotal = formattedBag.reduce((total, item) => total + (item.quantity * item.release.price), 0);
    subtotal = Math.ceil(subtotal * 100) / 100;

    if (phoneNumber.length !== 10) throw 'INVALID_NUM';

    const shippingRes = await CalculateShipping(name, streetAddress, streetAddressCont, city, state, postal, country, formattedBag);
    let shipping = shippingRes.cost;
    let discount = 0;
    let model;
    let modelPayout = 0;

    if (modelCode && modelCode !== "" && modelCode !== "TDTMNK") {
      model = await Model.findOne({identifier: modelCode});
      if (model) {
        if (model.scheme !== 'default') {
          discount = await GetDiscount(model.scheme, formattedBag);
        }

        subtotal -= discount;

        if (model.scheme === 'default') {
         discount = Math.round((shipping - 1) * 100) / 100;
         shipping = 1;
        }
        modelPayout = await GetModelPayout(model.scheme, formattedBag);
        modelPayout = Math.round(modelPayout * 100) / 100;
      }
    }

    if (modelCode && modelCode === "TDTMNK") {
      shipping = 0;
    }

    if (subtotal >= 25) {
      shipping = 0;
    }

    const taxReq = await taxJar.calculate(subtotal, {streetAddress, city, postal, country, state, cost: shipping});
    let tax = taxReq.tax;
    let taxRate = taxReq.rate;

    if (rawTotal !== (subtotal + shipping + tax).toFixed(2)) throw 'MISMATCH_TOTAL';

    let brands = [];
    let details = [];

    formattedBag.forEach((item) => {
      if (brands.indexOf(item.release.designer) == -1) {
        brands.push(item.release.designer);
      }
      details.push({
        seller: item.release.designer._id,
        design: item.release.design,
        color: item.color,
        size: item.size,
        reactives: item.reactives,
        production: item.release.production * item.quantity,
        payout: item.release.royalty * item.quantity,
        quantity: item.quantity,
        price: item.release.price * item.quantity
      });
    });

    const order = new Order({
      guest: {
        phoneNumber,
        name
      },
      details: details,
      updates: [{
        message: "Order Received and Processing"
      }],
      serial: CreateSerial(),
      cost: {
        production: details.reduce((total, next) => total + (next.production), 0),
        subtotal: subtotal,
        tax: tax,
        taxRate: taxRate,
        shipping: shipping,
        discount: model ? discount : null,
        total: Math.round((subtotal + tax + shipping) * 100) / 100,
      },
      supporting: model ? model._id : null,
      shipping: {
        details: {
          streetAddress,
          streetAddressCont,
          city,
          state,
          postal,
          country
        },
        label: {
          identifier: shippingRes.id
        }
      }
    });

    const charge = await stripe.charges.create({
      amount: parseInt(order.cost.total * 100),
      currency: 'usd',
      description: `Tailori #${order.serial}`,
      metadata: {
        orderID: order._id.toString(),
        orderSerial: order.serial,
        "Production(L)": order.cost.production,
        "Tax Collected": order.cost.tax,
        "Total Pre-Tax(R)": Math.round((order.cost.total - order.cost.tax) * 100) / 100,
        "Model Payout(L)": modelPayout,
        "Brands Payout(L)": order.details.reduce((total, next) => total + (next.payout), 0),
        "Tailori (P)": Math.round((order.cost.subtotal + order.cost.shipping - shipping - modelPayout - order.cost.production - order.details.reduce((total, next) => total + (next.payout), 0)) * 100) / 100
      },
      source: paymentToken,
      statement_descriptor: 'Tailori',
      statement_descriptor_suffix: `T${order.serial}`
    });

    const orderToken = await trinumDesign.order(order.cost.production);

    if (model && model.connect !== 'NA') {
      await stripe.transfers.create({
        amount: parseInt(modelPayout * 100),
        currency: 'usd',
        destination: model.connect,
        transfer_group: order.serial
      });
      let modelLink = await stripe.accounts.createLoginLink(model.connect);
      twilio.messages.create({
        body: `Cha-Ching! ${model.firstName}, you're an amazing model! Check out your most recent payout using this single use link\n\n${modelLink}`,
        from: process.env.TWILIO_NUMBER,
        to: model.phoneNumber
      });
    }

    order.stripe = charge.id;
    order.trinumDesign = orderToken;

    const user = await User.findOne({phoneNumber: phoneNumber}).exec();
    if (user) {
      user.orders.push(order._id);
      order.buyer = user._id;
      await user.save();
    }

    await order.save();
    twilio.messages.create({
      body: `Thank you for using Tailori! Here is the receipt for your order #${order.serial}\n\n${charge.receipt_url}`,
      from: process.env.TWILIO_NUMBER,
      to: phoneNumber
    });

    let brandsFound = await Brand.find({_id: {$in : brands}}).populate("creator").exec();

    // Send notifications
    try {
      Notifications.send(brandsFound.map((brand) => brand.creator.sockets).reduce((aggregate, next) => [...aggregate, ...next], []), {body: 'Someone bought your merch!', data: {}});
    } catch(e) {

    }

    try {
      const admins = await Admin.find({permissions: "developer"}).exec();
      Notifications.send(admins.map(admin => admin.sockets).reduce((aggregate, next) => [...aggregate, ...next], []), {body: 'A new order has arrived!', data: {}});
    } catch(e) {

    }

    return respond({
      body: {
        _o: order.serial
      }
    });

  } catch (e) {
    console.log(e);
    if (e.type && (e.type == "StripeInvalidRequestError" || e.type === 'StripeCardError')) {
      e = "PAYMENT_ERROR";
    }
    return onCatch(e);
  }
}

function onCatch(e) {
  let code = 500;
  const catchCases = ['DUP_NUMBER', 'AGE_ERR', 'PASS_LEN', 'INVALID_NUM', 'INVALID_PASS', 'TRINUM_ERROR', 'INEX_BRAND', 'UNVERIFIED_BRAND', 'STRIPE_ERROR', 'INEX_DESIGN', 'NO_CONNECT_SETUP', 'BAD_MODE', 'BAD_METRICS', 'BAD_TYPE', 'MERCH_RELEASED', 'NOT_RELEASED', 'INEX_RELEASE', 'BAD_SHIP', 'PAYMENT_ERROR', 'INEX_LIM_RELEASE', 'INEX_ORDER', 'NO_PRICING', 'BAD_SHIPPING', 'NO_MODEL', 'MISMATCH_TOTAL', 'MISMATCH_COUNTRY'];

  if (e === 'MISMATCH_AUTH') {
    code = 401;
  } else if (catchCases.includes(e)) {
    code = 200;
  }

  return respond({
    isError: true,
    code: code,
    error: e
  });
}

function respond({isError = false, code = 200, error = null, message = isError ? 'API encountered error' : 'API finished task successfully', body = {}} = {}) {
  return {
    _hE: isError,
    _c: code,
    _e: error,
    _m: message,
    _body: body
  }
}
