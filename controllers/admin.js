const crypto = require('crypto');
const twilio = require('twilio')(process.env.TWILIO_SID, process.env.TWILIO_AUTH);
const jwt = require('jsonwebtoken');
const cloudinary = require('cloudinary').v2;
const cloudinaryFunctions =  require('./Cloudinary');
const fs = require('fs');
const moment = require('moment');
const stripe = require('stripe')(process.env.STRIPE_SECRET);
const Notifications = require('./Notifications');
const trinumDesign = require('./TrinumDesign');
const { Expo } = require('expo-server-sdk');
const Colors = require('./colors');
const easypost = require('./EasyPost');

// MODELS
const Admin = require('../models/admin');
const User = require('../models/user');
const Model = require("../models/model");
const Brand = require('../models/brand');
const Design = require('../models/design');
const Release = require('../models/release');
const Notification = require('../models/notification');
const Order = require('../models/order');

module.exports.login = Login;

// Forest
module.exports.forest = {
  setPassword: SetPassword,
  remindValidate: RemindValidate,
  payoutsReview: PayoutsReview,
  redoDesign: RedoDesign,
  fixWhite: FixWhite,
  notify: Notify,
  resendBrandActivation: ResendBrandActivation,
  deleteBrand: DeleteBrand,
  deleteUser: DeleteUser,
  modelLogin: ModelStripeLogin
}

// Authorized
module.exports.updateSockets = UpdateSockets;
module.exports.removeSocket = RemoveSocket;
module.exports.deleteUser = DeleteUser;
module.exports.releaseVerdict = ReleaseVerdict;
module.exports.releaseDesignVerdict = ReleaseDesignVerdict;

module.exports.orders = {
  fulfillItem: FulfillItem,
  update: UpdateMessage,
  easypostUpdate: EasyPostUpdate,
  payout: Payout
};

// get
module.exports.load = {
  dashboard: LoadDashboard,
  orders: LoadOrders,
  order: LoadOrder,
  orderPayees: LoadOrderPayees,
  update: LoadUpdate,
  releases: LoadReleases,
  release: LoadRelease,
  designs: LoadDesigns,
  design: LoadDesign,
  users: LoadUsers,
  user: LoadUser,
  brands: LoadBrands,
  brand: LoadBrand
};

async function Login(body) {
  const phoneNumber = body.phoneNumber.replace(/\D/g,'');
  const password = body.password;

  try {
    const admin = await Admin.findOne({phoneNumber: phoneNumber}).exec();

    if (!admin) throw 'INVALID_NUM';
    if (!admin.validPassword(password)) throw 'INVALID_PASS';

    admin.actions.push({
      description: "Logged In"
    });
    await admin.save();

    return respond({
      body: admin.toAuth()
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function SetPassword(body) {
  const id = body.data.attributes.ids[0];
  const password = body.data.attributes.values.password;

  const admin = await Admin.findOne({_id: id}).exec();
  if (!admin) throw 'NO_ADMIN';

  await admin.setPassword(password);
  await admin.save();

  twilio.messages.create({
    mediaUrl: ['https://res.cloudinary.com/lgxy/raw/upload/tailori/documents/Tailori.vcf'],
    body: `LGXY Tailori Admin Interface Password\n\n${password}\n\nNever share this password with anyone.`,
    from: process.env.TWILIO_NUMBER,
    to: admin.phoneNumber
  });
}

async function PayoutsReview(body) {
  const id = body.data.attributes.ids[0];

  const admin = await Admin.findOne({_id: id}).exec();
  if (!admin) throw 'NO_ADMIN';
  if (!admin.permissions.includes('payouts')) throw 'NO_PERMISSION';

  const link = await stripe.accounts.createLoginLink(admin.stripe);

  twilio.messages.create({
    body: `LGXY Tailori Admin Interface\n\nHere is your Stripe payouts login url\n\n${link}`,
    from: process.env.TWILIO_NUMBER,
    to: admin.phoneNumber
  });
}

async function RemindValidate(body) {
  const id = body.data.attributes.ids[0];

  const admin = await Admin.findOne({_id: id}).exec();
  if (!admin) throw 'NO_ADMIN';
  if (!admin.permissions.includes('releases-approve') && !admin.permissions.includes('trinum')) throw 'NO_PERMISSION';

  // twilio.messages.create({
  //   body: `LGXY Tailori Admin REMINDER:\n\nMultiple designs need your attention`,
  //   from: process.env.TWILIO_NUMBER,
  //   to: admin.phoneNumber
  // });

  Notifications.send(admin.sockets, {body: 'Multiple designs need your attention', data: {}});
}

function GenRan() {
  return (Math.random() * (9 - 1) + 1).toFixed(0);
}

function CreateIdentifier(name) {
  let beginning = GenRan();
  let ending = GenRan();
  const consonants = name.replace(/[^a-zA-Z0-9 ]/g, "").replace(/[aeiou ]/gi, '').split('').filter(function(item, pos, self) {
    return self.indexOf(item) == pos;
  }).join('').toUpperCase();

  return beginning + consonants;
}

async function RedoDesign(body) {
  const id = body.data.attributes.ids[0];

  const design = await Design.findOne({_id: id}).populate('creator').exec();
  if (!design) throw 'NO_DESIGN';

  for (let shot of design.shots) {
    await cloudinary.uploader.destroy(shot);
  }

  let tags = [design.creator._id, CreateIdentifier(design.name), design.mode, design.type, 'design', 'portfolio', 'redo'];

  design.shots = [];

  if (design.type === "shirtT") {
    design.shots = await cloudinaryFunctions.GenerateTShirtShots(design.creator._id, design.name, design.metrics.sides, design.type, design.metrics.base, tags);
  } else if (design.type === 'hoodie') {
    design.shots = await cloudinaryFunctions.GenerateHoodieShots(design.creator._id, design.name, design.metrics.sides, design.type, design.metrics.base, tags);
  } else if (design.type === 'shirtTsleeve') {
    design.shots = await cloudinaryFunctions.GenerateTShirtSleeveShots(design.creator._id, design.name, design.metrics.sides, design.type, design.metrics.base, tags);
  } else if (design.type === 'cap') {
    design.shots = await cloudinaryFunctions.GenerateCapShots(design.creator._id, design.name, design.metrics.sides, design.type, design.metrics.base, tags)
  } else if (design.type === 'crewneck') {
    design.shots = await cloudinaryFunctions.GenerateCrewneckShots(design.creator._id, design.name, design.metrics.sides, design.type, design.metrics.base, tags)
  } else if (design.type === 'popsocket') {
    design.shots = await cloudinaryFunctions.GeneratePopSocketShots(design.creator._id, design.name, design.metrics.sides, design.type, design.metrics.base, tags)
  } else if (design.type === 'pillow') {
    design.shots = await cloudinaryFunctions.GeneratePillowShots(design.creator._id, design.name, design.metrics.sides, design.type, design.metrics.base, tags);
  } else if (design.type === 'mug') {
    design.shots = await cloudinaryFunctions.GenerateMugShots(design.creator._id, design.name, design.metrics.sides, design.type, design.metrics.base, tags);
  } else if (design.type === 'beanie') {
    design.shots = await cloudinaryFunctions.GenerateBeanieShots(design.creator._id, design.name, design.metrics.sides, design.type, design.metrics.base, tags);
  }

  design.save();
}

async function FixWhite(body) {
  const ids = body.data.attributes.ids;

  ids.forEach(async (id) => {
    const design = await Design.findOne({_id: id}).exec();
    if (design) {
      design.metrics.base = "WHTT";

      design.save();
    }
  });
}

async function Notify(body) {
  const ids = body.data.attributes.ids;
  const content = body.data.attributes.values.content;
  const title = body.data.attributes.values.title !== "" ? body.data.attributes.values.title : null;

  const users = await User.find({_id: ids}).exec();
  if (!users) throw 'NO_USERS';

  let notifTokens = users.reduce((total, next) => [...total, ...next.sockets], []);

  Notifications.send(notifTokens, {body: content, data: {}, title: title});
}

function FormatDetails(details) {
  return details.map(detail => {
    return {
      release: {
        identifier: detail._id.toString(),
        price: Math.round((detail.price / detail.quantity) * 100) / 100
      },
      quantity: detail.quantity
    }
  });
}

async function ResendBrandActivation(body) {
  const id = body.data.attributes.ids[0];

  const brand = await Brand.findOne({_id: id}).populate('creator').exec();
  if (!brand) throw 'NO_BRAND';
  if (brand.connect && brand.connect !== '') throw 'ACCOUNT ALREADY SET';

  twilio.messages.create({
    body: `${brand.name}, finish activating your brand! Click this link to set up your Stripe Connect Account! We await your merch!\n\nhttps://connect.stripe.com/express/oauth/authorize?client_id=${process.env.STRIPE_CONNECT_CLIENT_ID}`,
    from: process.env.TWILIO_NUMBER,
    to: brand.creator.phoneNumber
  }).then(message => console.log(message));
}

async function DeleteBrand(body) {
  const id = body.data.attributes.ids[0];

  const brand = await Brand.findOne({_id: id}).populate('creator').exec();
  if (!brand) throw 'NO_BRAND';

  if (brand.connect && brand.connect !== "") {
    await stripe.accounts.del(brand.connect);
  }

  await Release.deleteMany({designer: brand._id});
  await cloudinary.uploader.destroy(brand.logo);
  await Brand.deleteOne({_id: brand._id});
}

async function DeleteUser(body) {
  const id = body.data.attributes.ids[0];

  const user = await User.findOne({_id: id}).exec();
  if (!user) throw 'NO_BRAND';

  if (user.payment.source && user.payment.source !== "") {
    await stripe.customers.deleteSource(user.payment.customer, user.payment.source);
    await stripe.payment.source.del(user.payment.source);
  }

  if (user.payment.customer && user.payment.customer !== "") {
    await stripe.customers.del(user.payment.customer);
  }

  await Design.deleteMany({creator: user._id});
  await User.deleteOne({_id: user._id});
}

async function ModelStripeLogin(body) {
  const id = body.data.attributes.ids[0];

  const model = await Model.findOne({_id: id}).exec();
  if (!model || model.connect === 'NA') throw 'NO_BRAND';

  let modelLink = await stripe.accounts.createLoginLink(model.connect);

  twilio.messages.create({
    body: `${model.firstName}, use this url to log into Stripe and see your most recent payout or edit your account.\n\n${modelLink}`,
    from: process.env.TWILIO_NUMBER,
    to: model.phoneNumber
  }).then(message => console.log(message));
}

async function UpdateSockets(body) {
  const auth = jwt.decode(body.auth);
  const token = body.token;

  try {
    const admin = await Admin.findOne({crypto: auth.crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';

    if (admin.sockets.indexOf(token) === -1 && Expo.isExpoPushToken(token)) {
      admin.sockets.push(token);
      await admin.save();
    }
    return respond({});
  } catch (e) {
    return onCatch(e);
  }
}

async function RemoveSocket(body) {
  const auth = jwt.decode(body.auth);
  const token = body.token;

  try {
    const admin = await Admin.findOne({crypto: auth.crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';

    if (admin.sockets.indexOf(token) > -1) {
      admin.sockets.splice(admin.sockets.indexOf(token), 1);
      admin.actions.push({
        description: "Logged Out"
      });
      await admin.save();
    }
    return respond({});
  } catch (e) {
    return onCatch(e);
  }
}

async function DeleteUser(body) {
  const auth = jwt.decode(body.auth);
  const id = body.id;

  try {
    const admin = await Admin.findOne({crypto: auth.crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('users-delete')) throw 'NO_PERMISSION';

    const user = await User.findOne({_id: id}).exec();
    if (!user) throw 'INEX_USER';

    let now = moment();
    let diff = now.diff(moment(user.updatedAt), 'days');

    if (diff < 21) throw 'DELETION_PENDING';

    await Release.deleteMany({designer: user.brand}).exec();
    await Design.deleteMany({creator: user._id});
    await Brand.deleteOne({creator: user._id}).exec();
    await stripe.customers.del(user.payment.customer);
    await User.deleteOne({_id: user._id}).exec();

    admin.actions.push({
      description: "Deleted User Account",
      user: user._id
    })
    await admin.save();

    return respond({});
  } catch (e) {
    return onCatch(e);
  }
}

async function NotifyVerdict(user, design, decision) {
  try {
    await new Notification({
      recipient: user,
      header: `${design.name}\'s release update`,
      subheader: decision ? 'Approved' : 'Denied',
      action: 'verdict',
      metadata: {
        design: design._id
      }
    }).save();
  } catch (e) {

  }
  try {
    await Notifications.send(user.sockets, {body: decision ? 'Your release was approved' : 'Your release was denied', data: {}});
  } catch (e) {
    console.log(e);
  }
}

async function ReleaseVerdict(body) {
  const auth = jwt.decode(body.auth);
  const identifier = body.identifier;
  const decision = body.decision == 'true';

  try {
    const admin = await Admin.findOne({crypto: auth.crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('releases-approve')) throw 'NO_PERMISSION';

    const release = await Release.findOne({identifier: identifier}).populate({path: 'design', populate: {path: 'creator'}}).exec();
    if (!release) throw 'INEX_ITEM';
    const design = await Design.findOne({_id: release.design._id}).exec();

    if (decision) {
      release.approved = true;

      try {
        const token = await trinumDesign.approval(release, design.trinum);
        design.trinum = token;
        if (design.trinum.includes('TLRI*EXP*')) {
          release.printable = true;
          release.production = 0;

          await NotifyVerdict(release.design.creator, release.design, true);
        }
        design.save();
      } catch (e) {
        console.log(e);
        throw 'TRINUM_ERROR';
      }
      await release.save();
    } else {
      await Release.deleteOne({identifier: identifier}).exec();
      NotifyVerdict(release.design.creator, release.design, decision);
    }

    admin.actions.push({
      description: decision ? "Approved a Release" : "Denied a Release",
      release: release._id,
      design: release.design._id
    });
    await admin.save();

    return respond({});
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function ReleaseDesignVerdict(body) {
  const auth = jwt.decode(body.auth);
  const identifier = body.identifier;
  const decision = body.decision == 'true';
  const price = body.price;

  try {
    const admin = await Admin.findOne({crypto: auth.crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('trinum')) throw 'NO_PERMISSION';

    const release = await Release.findOne({identifier: identifier}).populate({path: 'design', populate: {path: 'creator'}}).exec();
    if (!release) throw 'INEX_ITEM';

    if (decision) {
      release.printable = true;
      release.production = parseFloat(price);
      release.price = Math.round(await trinumDesign.CalculatePrice(release.royalty, release.design.type, release.production) * 100) / 100;
      await release.save();
    } else {
      await Release.deleteOne({identifier: identifier}).exec();
    }

    NotifyVerdict(release.design.creator, release.design, decision);

    admin.actions.push({
      description: decision ? "Validated a Design " : "Marked Design as Invalid",
      release: release._id,
      design: release.design._id
    });
    await admin.save();

    return respond({
      body: {
        _cop: release.production.toFixed(2),
        _price: release.price.toFixed(2)
      }
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function FulfillItem(body) {
  const auth = jwt.decode(body.auth);
  const identifier = body.identifier;
  const item = body.item;

  try {
    const admin = await Admin.findOne({crypto: auth.crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('orders')) throw 'NO_PERMISSION';

    const order = await Order.findOne({serial: identifier, "details._id": item}).exec();
    if (!order) throw 'INEX_ITEM';

    for (let i = 0; i < order.details.length; i++) {
      if (order.details[i]._id.toString() !== item) continue;

      order.details[i].fulfilled = true;
      admin.actions.push({
        description: "Marked order item as fulfilled",
        order: order._id
      });
      await admin.save();
    }
    await order.save();

    return respond({});
  } catch (e) {
    return onCatch(e);
  }
}

async function NotifyUpdate(order, subheader) {
  if (!order.buyer) {
    twilio.messages.create({
      body: `Your Tailori order #${order.serial} recieved a new message!\n\n${subheader}`,
      from: process.env.TWILIO_NUMBER,
      to: order.guest.phoneNumber
    });
    return;
  }
  try {
    await new Notification({
      recipient: order.buyer._id,
      header: subheader,
      subheader: `Order #${order.serial}`,
      action: 'track',
      metadata: {
        orderNumber: order.serial
      }
    }).save();
  } catch (e) {

  }
  try {
    await Notifications.send(order.buyer.sockets, {body: subheader, data: {action: 'track', serial: order.serial}});
  } catch (e) {

  }
}

async function UpdateMessage(body) {
  const auth = jwt.decode(body.auth);
  const orderIdentifier = body.identifier;
  let message = body.message;

  try {
    const admin = await Admin.findOne({crypto: auth.crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('orders-notify')) throw 'NO_PERMISSION';
    const order = await Order.findOne({serial: orderIdentifier}).populate('buyer').exec();
    if (!order) throw 'INEX_ORDER'

    let response = '';

    if (message === 'DELIVERED') {
      order.shipping.carrier = 'DELIVERED';
      order.shipping.label.url = 'NONE';
      order.shipping.tracking = 'NONE';
      PayoutBrands(order);
      order.fulfilled = true;
      response = 'DELIVERED';
      message = "Time to Explore! 👽🛸 Open for a Surpise! ☄️";

      if (!order.buyer) {
        message += `\n\nYour order has been delivered`;
      }
    }

    if (message === 'BOXED') {
      const shipmentData = await easypost.labelize(order.shipping.label.identifier);
      order.shipping.carrier = shipmentData.carrier;
      order.shipping.label.url = shipmentData.label;
      order.shipping.tracking = shipmentData.tracking;
      PayoutBrands(order);
      response = order.shipping.label.url;
      message = "Dressing Up 👨‍🚀";
      order.fulfilled = true;

      if (!order.buyer) {
        message += `\n\nYou can track your shipment through the following link\n${order.shipping.tracking}`;
      }
    }

    if (message === 'DROPPED') {
      message = 'Dropped off at Station 🌎';
    }

    admin.actions.push({
      description: `Updated Order: ${message}`,
      order: order._id
    });
    await admin.save();

    order.updates.push({
      message: message,
      provider: admin.permissions.includes('trinum') ? 'Trinum Design' : 'Tailori'
    });
    await order.save();
    NotifyUpdate(order, message);
    return respond({
      body: {
        _r: response
      }
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

function GetMessage(message) {
  if (message === 'pre_transit' || message === 'pending') {
    return 'Awaiting Takeoff 🛰⏳'
  }
  if (message === 'in_transit') {
    return 'Taking Off! 🌎🚀';
  }
  if (message === 'out_for_delivery') {
    return 'Almost Landing! 🪐';
  }
  if (message === 'delivered') {
    return 'Time to Explore! 👽🛸 Open for a Surpise! ☄️';
  }

  if (message === 'error' || message === 'failure') {
    return 'Chicago We Have a Problem! 🤕';
  }

  if (message === 'cancelled') {
    return 'Mission Cancelled 😔';
  }

  return 'Checking Atmosphere 🌑☄️'
}

async function EasyPostUpdate(body) {
  try {
    if (body.description !== 'tracker.updated') return;

    const shipmentID = body.result.shipment_id;
    const message = body.result.status;

    const order = await Order.findOne({"shipping.label.identifier": shipmentID}).populate('buyer').exec();

    order.updates.push({
      message: GetMessage(message),
      provider: 'EasyPost'
    });
    await order.save();
    NotifyUpdate(order, GetMessage(message));
    return respond({});
  } catch (e) {
    console.log(e);
    e = "EASY_POST_HOOK_ERROR";
    return onCatch(e);
  }
}

async function Payout(body) {
  const auth = jwt.decode(body.auth);
  const orderIdentifier = body.identifier;
  let amount = body.amount;
  const account = body.account;

  try {
    amount = parseFloat(body.amount);
    const admin = await Admin.findOne({crypto: auth.crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('finances')) throw 'NO_PERMISSION';
    const order = await Order.findOne({serial: orderIdentifier}).exec();
    const receiver = await Admin.findOne({_id: account}).exec();
    if (!receiver || !order) throw 'MISMATCH_AUTH';


    admin.actions.push({
      description: `Payed out: $${amount.toFixed(2)} - ${receiver.firstName}`,
      order: order._id
    });

    if (!order.cost.experimental) {
      order.cost.experimental = 0;
    }
    order.cost.experimental += amount;
    await stripe.charges.update(order.stripe, {metadata: {
      "Experimental(L)": order.cost.experimental
    }})
    await stripe.transfers.create({
      amount: amount * 100,
      destination: receiver.stripe,
      currency: 'usd',
      transfer_group: order.serial,
      metadata: {
        order: `Order #${order.serial}`
      }
    });
    receiver.actions.push({
      description: `Received pay out: $${amount.toFixed(2)}`,
      order: order._id
    });
    await admin.save();
    await receiver.save();
    await order.save();
    return respond();
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function PayoutBrands(order) {
  let brands = [];
  order.details.forEach((item) => {
    if (brands.indexOf(item.seller) == -1) {
      brands.push(item.seller);
    }
  });

  let brandsFound = await Brand.find({_id: {$in : brands}}).populate("creator").exec();

  brandsFound.forEach(async (brand) => {
    if (brand.connect === "ADMIN") return;

    let brandItems = order.details.filter((item) => item.seller._id.toString() === brand._id.toString());
    let royalty = brandItems.reduce((total, item) => total + item.payout, 0);
    let distribution = brandItems.reduce((total, item) => total + item.quantity, 0);
    brand.royalties += royalty;
    brand.distribution += distribution;

    let payout = await stripe.transfers.create({
      amount: parseInt(royalty * 100),
      currency: 'usd',
      destination: brand.connect,
      transfer_group: order.serial
    })
    brand.payouts.push({
      amount: royalty,
      transfer: payout.id,
    });
    await brand.save();

    await new Notification({
      recipient: brand.creator._id,
      header: "CHA-CHING! Your royalty is on it's way!",
      subheader: `$${royalty.toFixed(2)}`,
      action: "sale",
      metadata: {
        orderSerial: order.serial
      }
    }).save();
  });

  try {
    Notifications.send(brandsFound.map((brand) => brand.creator.sockets).reduce((aggregate, next) => [...aggregate, ...next], []), {body: 'Your royalty is on it\'s  way!', data: {}});
  } catch(e) {

  }
}

async function LoadDashboard(data) {
  const crypto = data.crypto;

  try {
    const admin = await Admin.findOne({crypto: crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('brands')) throw 'NO_PERMISSION';

    const users = await User.estimatedDocumentCount();
    const releases = await Release.estimatedDocumentCount();
    const brands = await Brand.estimatedDocumentCount();

    const ordersPending = await Order.countDocuments({fulfilled: false}).exec();
    const releasesPending = await Release.countDocuments({approved: false}).exec();

    let monthlyRevenue = 0;

    const dayOfMonth = moment().date();
    let taxDue = 0;
    let revenue = 0;
    let daysTillFilling = -1;

    if (dayOfMonth > 14 && dayOfMonth < 21) {
      // TAX FILING NEEDED SO SHOW IT

      const twentieth = moment().date(20);
      const beginningOfLastMonth = moment().subtract(1, 'months').startOf('month');
      const endOfLastMonth = moment().subtract(1, 'months').endOf('month');
      daysTillFilling = twentieth.diff(moment(), 'd');

      const ordersInMonth = await Order.find({createdAt: {$gte : beginningOfLastMonth, $lte: endOfLastMonth}}).exec();

      taxDue = ordersInMonth.reduce((total, order) => total += order.cost.tax, 0);
      taxDue = Math.round(taxDue * 100) / 100;

      revenue = ordersInMonth.reduce((total, order) => total += order.cost.total, 0);
      revenue = Math.round(revenue * 100) / 100;
    }

    const ordersThisMonth = await Order.find({createdAt: {$gte : moment().startOf('month')}}).exec();
    monthlyRevenue = ordersThisMonth.reduce((total, order) => total += order.cost.total, 0);
    monthlyRevenue = Math.round(monthlyRevenue * 100) / 100;

    return respond({
      body: {
        _u: users,
        _b: brands,
        _r: releases,
        _op: ordersPending,
        _rp: releasesPending,
        _td: taxDue,
        _mr: monthlyRevenue,
        _dtf: daysTillFilling,
        _rv: revenue
      }
    });

  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function LoadOrders(data, params) {
  const crypto = data.crypto;

  try {
    let sort = params.sortBy;
    let query = {};
    let sortQuery = {};

    if (typeof params.displayFulfilled != 'undefined' && params.displayFulfilled != 'true') {
      query.fulfilled = false;
    }

    if (sort === 'newest') {
      sortQuery = { createdAt : -1 };
    } else if (sort === 'oldest') {
      sortQuery = { createdAt : 1 };
    }

    const admin = await Admin.findOne({crypto: crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('orders')) throw 'NO_PERMISSION';

    const orders = await Order.find(query).sort(sortQuery).exec();

    return respond({
      body: {
        _o: orders.map((order) => order.lgxyQuickFormat())
      }
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function LoadOrder(data, params) {
  const crypto = data.crypto;
  const identifier = params.identifier;

  try {
    const admin = await Admin.findOne({crypto: crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('orders')) throw 'NO_PERMISSION';

    const order = await Order.findOne({serial: identifier}).populate('buyer').populate('details.design').exec();
    if (!order) throw 'INEX_ITEM';

    return respond({
      body: order.lgxyFormat()
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function LoadOrderPayees(data, params) {
  const crypto = data.crypto;
  const identifier = params.identifier;

  try {
    const admin = await Admin.findOne({crypto: crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('finances')) throw 'NO_PERMISSION';

    const payees = await Admin.find({permissions: 'payouts'}).exec();

    return respond({
      body: {
        _p: true,
        _a: payees.map((payee) => payee.lgxyPayee())
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function LoadUpdate(data, params) {
  const crypto = data.crypto;
  const identifier = params.identifier;

  try {
    const admin = await Admin.findOne({crypto: crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('orders')) throw 'NO_PERMISSION';

    const order = await Order.findOne({serial: identifier}).exec();
    if (!order) throw 'INEX_ITEM';

    return respond({
      body: {
        _s: order.process,
        _p: admin.permissions.includes('orders-notify'),
        _aF: order.shippingFriendly()
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

function capitalize(s) {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}

async function LoadReleases(data, params) {
  const crypto = data.crypto;

  try {
    let sort = data.sort;
    let query = {};
    let sortQuery = {};

    if (sort === 'newest') {
      sort.createdAt = 'desc';
    } else if (sort === 'oldest') {
      sort.createdAt = 'asc';
    }

    const admin = await Admin.findOne({crypto: crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('releases') && !admin.permissions.includes('trinum')) throw 'NO_PERMISSION';

    if (typeof params.displayApproved != 'undefined' && params.displayApproved != 'true') {
      query.approved = false;
      query.printable = false;
    }

    if (admin.permissions.includes('trinum')) {
      query.approved = true;
    }

    const releases = await Release.find(query).sort(sortQuery).populate('design').exec();

    return respond({
      body: {
        _r: releases.map((release) => release.lgxyQuickFormat(admin.permissions))
      }
    });
  } catch (e) {
    console.log(e);
    return onCatch(e);
  }
}

async function LoadRelease(data, params) {
  const crypto = data.crypto;
  const identifier = params.identifier;

  try {
    const admin = await Admin.findOne({crypto: crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('releases')) throw 'NO_PERMISSION';

    const release = await Release.findOne({identifier: identifier}).populate('designer').populate('design').exec();
    if (!release) throw 'INEX_ITEM';

    return respond({
      body: {
        ...release.lgxyFormat(admin.permissions),
        _hP: admin.permissions.includes('releases-approve') && !release.approved,
        _hV: admin.permissions.includes('trinum') && !release.printable,
      },
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function LoadDesigns(data, params) {
  const crypto = data.crypto;

  try {
    let sort = data.sort;
    let query = {};
    let sortQuery = {};

    if (typeof params.displayDeleted != 'undefined' && params.displayDeleted != 'true') {
      query.deleted = false;
    }

    if (sort === 'newest') {
      sortQuery.createdAt = 'desc';
    } else if (sort === 'oldest') {
      sortQuery.createdAt = 'asc';
    }

    const admin = await Admin.findOne({crypto: crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('designs')) throw 'NO_PERMISSION';

    const designs = await Design.find(query).sort(sortQuery).exec();

    return respond({
      body: {
        _d: designs.map((design) => design.lgxyQuickFormat())
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function LoadDesign(data, params) {
  const crypto = data.crypto;
  const id = params.id;

  try {
    const admin = await Admin.findOne({crypto: crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('designs')) throw 'NO_PERMISSION';

    const design = await Design.findOne({_id: id}).populate('designer').populate('creator').exec();
    if (!design) throw 'INEX_ITEM';

    return respond({
      body: design.lgxyFormat(),
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function LoadBrands(data, params) {
  const crypto = data.crypto;

  try {
    let sort = data.sort;
    let query = {};
    let sortQuery = {};

    if (typeof params.hasReleases != 'undefined' && params.hasReleases == 'true') {
      query["releases.0"] = { "$exists": true };
    }

    if (sort === 'newest') {
      sortQuery.createdAt = 'desc';
    } else if (sort === 'oldest') {
      sortQuery.createdAt = 'asc';
    }

    const admin = await Admin.findOne({crypto: crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('brands')) throw 'NO_PERMISSION';

    const brands = await Brand.find(query).sort(sortQuery).exec();

    return respond({
      body: {
        _b: brands.map((brand) => brand.lgxyQuickFormat())
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function LoadBrand(data, params) {
  const crypto = data.crypto;
  const identifier = params.identifier;

  try {
    const admin = await Admin.findOne({crypto: crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('brands')) throw 'NO_PERMISSION';

    const brand = await Brand.findOne({identifier: identifier}).populate('creator').exec();
    if (!brand) throw 'INEX_ITEM';

    return respond({
      body: brand.lgxyFormat(),
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function LoadUsers(data, params) {
  const crypto = data.crypto;

  try {
    let sort = data.sort;
    let query = {};
    let sortQuery = {};

    if (typeof params.displayDeleted != 'undefined' && params.displayDeleted != 'true') {
      query.deleted = false;
    }

    if (sort === 'newest') {
      sortQuery.createdAt = 'desc';
    } else if (sort === 'oldest') {
      sortQuery.createdAt = 'asc';
    }

    const admin = await Admin.findOne({crypto: crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('users')) throw 'NO_PERMISSION';

    const users = await User.find(query).sort(sortQuery).populate('brand').exec();

    return respond({
      body: {
        _u: users.map((user) => user.lgxyQuickFormat())
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

async function LoadUser(data, params) {
  const crypto = data.crypto;
  const id = params.id;

  try {
    const admin = await Admin.findOne({crypto: crypto}).exec();
    if (!admin) throw 'MISMATCH_AUTH';
    if (!admin.permissions.includes('users')) throw 'NO_PERMISSION';

    const user = await User.findOne({_id: id}).populate('brand').exec();
    if (!user) throw 'INEX_ITEM';

    return respond({
      body: {
        ...user.lgxyFormat(),
        _cd: admin.permissions.includes('users-delete')
      }
    });
  } catch (e) {
    return onCatch(e);
  }
}

function onCatch(e) {
  let code = 500;
  const catchCases = ['MISMATCH_AUTH', 'NO_PERMISSION', 'INEX_ITEM', 'TRINUM_ERROR', 'EASY_POST_HOOK_ERROR', 'ITEM_NOT_FULFILLED'];

  if (e === 'MISMATCH_AUTH') {
    code = 401;
  } else if (catchCases.includes(e)) {
    code = 200;
  }

  return respond({
    isError: true,
    code: code,
    error: e
  });
}

function respond({isError = false, code = 200, error = null, message = isError ? 'API encountered error' : 'API finished task successfully', body = {}} = {}) {
  return {
    _hE: isError,
    _c: code,
    _e: error,
    _m: message,
    _body: body
  }
}
