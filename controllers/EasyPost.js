const Easypost = require('@easypost/api');
const api = new Easypost(process.env.EASYPOST_API_KEY);
const FROM = {
  street1: '2729 S Karlov Ave',
  city: 'Chicago',
  state: 'IL',
  zip: '60623',
  country: 'USA',
  company: 'Tailori',
  email: 'support@tailorii.app'
}
module.exports = {
  shipment: Shipment,
  labelize: Labelize
};

function GetWeight(type) {
  if (type === 'shirtT') return 2;

  if (type === 'shirtTxB') return 2;

  if (type === 'hoodie') return 5;

  if (type === 'popsocket') return 0.2;

  if (type === 'mug') return 12.2;

  if (type === 'pillow') return 17.2;

  return 1;
}

function GetBoxType(items, baseWeight) {
  if (items.find(item => item.type === 'pillow')) {
    /// big box
  }

  if (items.find(item => item.type === 'mug') && items.length === 1) {
    /// mug box
  }

  return {
      weight: 0.2
  }
}

async function Shipment(address, items) {
  let baseWeight = items.reduce((total, next) => total + GetWeight(next.type) * next.quantity, 0);
  let boxType = GetBoxType(items, baseWeight);
  let weight = boxType.weight + baseWeight;
  weight = Math.round(weight * 10) / 10;
  boxType.weight = weight;
  const toAddress = new api.Address({ ...address });
  const fromAddress = new api.Address({ ...FROM });
  const parcel = new api.Parcel({ ...boxType });

  const shipment = new api.Shipment({
    to_address: toAddress,
    from_address: fromAddress,
    parcel: parcel
  });

  const res = await shipment.save();
  const cheapestRate = await res.rates.sort((a, b) => a.rate - b.rate)[0];
  return {cost: parseFloat(cheapestRate.rate), id: res.id};
}

async function Labelize(id) {
  const shipment = await api.Shipment.retrieve(id);
  const cheapestRate = await shipment.rates.sort((a, b) => a.rate - b.rate)[0];
  const res = await shipment.buy(cheapestRate.id);

  return {carrier: 'USPS', label: res.postage_label.label_url, tracking: res.tracker.public_url};
}
