module.exports = {
  calculate: Calculate
};

async function Calculate(subtotal, shipping) {
  try {
    let rate = 0.00;
    let tax = 0.00

    if (shipping.state === "IL") {
      rate = 0.1025;
      tax = (subtotal + shipping.cost) * rate;
    }
    return { isError: false, tax, rate}
  } catch(e) {
    console.log(e);
    return { isError: true, tax: null, rate: null }
  }
}

// OLD TAX JAR
//
// const Taxjar = require('taxjar');
// const client = new Taxjar({
//   apiKey: process.env.TAXJAR_API_KEY,
//   apiUrl: process.env.TAXJAR_ENDPOINT
// });
// const moment = require('moment');
// const CountryCodeFormalize = require("country-iso-3-to-2");
//
// module.exports = {
//   calculate: Calculate,
//   transaction: Transaction,
//   rate: Rate
// };
//
// function FormatLineItems(items, discount) {
//   return items.map(item => {
//     let rand = Math.round(8 * Math.random()) + 1;
//     return {
//       id: item.release.identifier+"_"+rand.toString(),
//       quantity: item.quantity,
//       product_tax_code: '20010',
//       unit_price: item.release.price,
//       discount: discount ? discount / items.length : null
//     }
//   });
// }
//
// /*
//
//   INSTANCE FOR TaxJar AND Simple API USAGE
//
//   FOR MORE INFORMATION VISIT AND FULL DOCUMENTATION https://developers.taxjar.com/api/reference/?javascript#introduction
// */
//
// /*
//
//   @Calulate(subtotal: subtotal, shipping: cart shipping object, items: cart items)
//     Calcuates the amount of tax and returns correct tax amount as well as total
//
//   returns {
//       isError      boolean   Did the release work successfully
//       tax          float    Amount of Taxes
//       rate        float    Tax Rate
//   }
// */
//
// async function Rate(zipcode) {
//   try {
//     const res = await client.ratesForLocation(zipcode);
//
//     return { isError:false, rate: res.rate.combined_rate };
//   } catch(e) {
//     return { isError: true }
//   }
// }
//
// async function Transaction(serial, subtotal, tax, shipping, items, discount = false, date = false) {
//
//   try {
//     const res = await client.createOrder({
//       transaction_id: serial,
//       transaction_date: date ? date : moment(),
//       from_country: 'US',
//       from_zip: '60623',
//       from_state: 'IL',
//       from_city: 'Chicago',
//       to_country: CountryCodeFormalize(shipping.country),
//       to_zip: shipping.postal,
//       to_state: shipping.state,
//       to_city: shipping.city,
//       to_street: shipping.streetAddress,
//       amount: subtotal + shipping.cost,
//       shipping: shipping.cost,
//       sales_tax: tax,
//       line_items: FormatLineItems(items, discount)
//     });
//     return { isError: false }
//   }  catch(e) {
//     console.log(e);
//     return { isError: true }
//   }
// }
