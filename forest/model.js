const Liana = require('forest-express-mongoose');

Liana.collection('Model', {
  actions: [{
    name: 'Send Stripe Login',
    type: 'single',
    fields: [],
    endpoint: '/api/actions/lgxy/model-stripe'
  }],
});
