const Liana = require('forest-express-mongoose');

Liana.collection('Design', {
  actions: [
    {
      name: 'Fix White',
      type: 'bulk',
      fields: [],
      endpoint: '/api/actions/lgxy/fix-white'
    },
    {
      name: 'Redo Design',
      type: 'single',
      fields: [],
      endpoint: '/api/actions/lgxy/redo-design'
    }
  ]
});
