const Liana = require('forest-express-mongoose');

Liana.collection('Admin', {
  actions: [
    {
      name: 'Set Password',
      type: 'single',
      fields: [{
        field: 'password',
        type: 'String',
        description: 'Helpful password generating site: https://www.lastpass.com/password-generator',
        isRequired: true
      }],
      endpoint: '/api/actions/lgxy/set-password'
    },
    {
      name: 'Remind Validate',
      type: 'single',
      fields: [],
      endpoint: '/api/actions/lgxy/remind-validate'
    },
    {
      name: 'Send Stripe Payouts Review',
      type: 'single',
      fields: [],
      endpoint: '/api/actions/lgxy/payouts-review'
    }
  ]
});
