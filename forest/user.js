const Liana = require('forest-express-mongoose');

Liana.collection('User', {
  actions: [{
    name: 'Send Notification',
    type: 'bulk',
    fields: [
      {
        field: 'title',
        type: 'String',
        description: 'Title should be short and fun!',
        isRequired: false
      },
      {
        field: 'content',
        type: 'String',
        description: 'Make sure notification is friendly and encourages user to open app!',
        isRequired: true
      }
    ],
    endpoint: '/api/actions/lgxy/notify'
  },
  {
    name: 'Deep Delete',
    type: 'single',
    fields: [
      {
        field: 'cloudinary-done',
        type: 'Boolean',
        description: 'Have you deleted cloudinary folder?',
        isRequired: true
      }
    ],
    endpoint: '/api/actions/lgxy/user-deletion'
  }],
});
