const Liana = require('forest-express-mongoose');

Liana.collection('Brand', {
  actions: [{
    name: 'Resend Activation',
    type: 'single',
    fields: [],
    endpoint: '/api/actions/lgxy/brand-activation'
  }, {
    name: "Deep Delete Brand",
    type: 'single',
    fields: [],
    endpoint: '/api/actions/lgxy/brand-deletion'
  }],
});
