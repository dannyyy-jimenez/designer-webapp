require('dotenv').config();
const express = require('express');
const app = express();
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const compression = require('compression');
const mongoose = require('mongoose');
const appRoutes = require('./routes/api.js');
const dynamicRoutes = require('./routes/dynamic.js');
const Liana = require('forest-express-mongoose');
const path = require('path');
const PORT = 80;
const fs = require('fs');

const allowedOrigins = [
  'http://localhost',
  'http://localhost:8080',
  '*.forestadmin.com',
  'http://app.forestadmin.com',
  'https://app.forestadmin.com',
  'https://www.tailorii.app',
  'https://tailori.herokuapp.com'
];

const corsOptions = {
  origin: (origin, callback) => {
    if (allowedOrigins.includes(origin) || !origin || process.env.NODE_ENV !== 'production') {
      callback(null, true);
    } else {
      console.log(origin);
      callback(new Error('Origin not allowed by CORS'));
    }
  },
  allowedHeaders: ['Authorization', 'X-Requested-With', 'Content-Type'],
  credentials: true // This is important.
}

app.use(express.static(path.join(__dirname, "client", "build")));
app.set('view cache', true);
app.use(cookieParser(process.env.COOKIE_SECRET));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cors(corsOptions));
app.use(helmet({
  frameguard: {
    action: 'deny'
  }
}));
app.use(compression());
app.set('etag', false);
app.use(Liana.init({
  modelsDir: path.join(__dirname, '/models'),
  configDir: path.join(__dirname, '/forest'),
  envSecret: process.env.FOREST_ENV_SECRET,
  authSecret: process.env.FOREST_AUTH_SECRET,
  mongoose: mongoose
}));

const options = {
  poolSize: 10, // Maintain up to 10 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: true,
  useCreateIndex: true
};
mongoose.connect(process.env.MONGO_URI, options);
const connection = mongoose.connection;

connection.on('error', () => {
  console.log('Error connecting to MongoDB');
});
connection.once('open', () => {
  console.log('MongoDB database connection established succesfully');
})

app.use('/api', appRoutes);
app.use('/shop', dynamicRoutes);
app.get('/.well-known/apple-app-site-association', (req, res) => {
  res.sendFile(path.join(__dirname, "client", "build", "apple-app-site-association"));
});
app.get('/*', (req, res) => {
  const raw = fs.readFileSync(path.join(__dirname, "client", "build", "index.html"), 'utf8');
  let defaultMetadata = `<meta property="og:image" content="https://res.cloudinary.com/lgxy/image/upload/tailori/banner.png"/><meta property="og:url" content="https://www.tailorii.app/"/><meta property="og:type" content="website"/><meta property="og:title" content="Tailori — Kickstart Your Merch 🚀"/><meta property="og:description" content="Start selling your designs on clothing at no cost. We'll handle production and delivery. You handle making awesome designs!"/>`;
  const updated = raw.replace('<meta name="__DYNAMIC_META__"/>', defaultMetadata);

  res.status(200).send(updated);
});

app.listen(process.env.PORT || PORT, () => {
  console.log('Server is running on port: ' + PORT);
});
