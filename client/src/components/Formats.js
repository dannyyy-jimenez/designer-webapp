import Colors from '../models/colors';

export const Formats = {
  formatPhoneNumber : (phoneNumberString) => {
    var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
    var match = cleaned.match(/^(\d{3})(\d{1,3})?(\d{1,4})?$/)
    if (match) {
      return '(' + match[1] + (match[1] && match[2] ? ') ' : '') + (match[2] || '') + (match[2] && match[3] ? '-' : '') + (match[3] || '')
    }
    return phoneNumberString;
  },
  formatDob : (dobString) => {
    var cleaned = ('' + dobString).replace(/\D/g, '')
    var match = cleaned.match(/^(0[1-9]|1[0-2])(0[1-9]?|1\d?|2\d?|3[01]?)((19?\d{0,2})|(20?\d{0,2}))?$/);
    if (match) {
      return match[1] + (match[2] ? '/' + match[2] : '') + (match[3] && match[2] && match[2].length === 2 ? '/' + match[3] : '');
    }
    return dobString;
  },
  ccNumber: (number) => {
    number = number.replace(/\D/g, '');
    let groups = number.match(/.{1,4}/g);
    if (!groups) return '';
    return groups.join(' ');
  },
  ccExp: (date, previous) => {
    if (date.length < previous.length && date.length === 2) {
      // backspace
      date = date.substring(0,1);
    } else if (date.length === 2) {
      date += '/';
    }

    return date;
  },
  ccExpPretty: (date) => {
    let parts = date.split('/');

    if (parts[1] && parts[1].length > 2) {
      parts[1] = parts[1].substring(2);
    }

    return parts.join('/');
  },
  cardIcon: (number) => {
    if (number.match(/^4/g) != null)
        return "cc-visa";

    if (/^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/.test(number))
        return "cc-mastercard";

    if (number.match(/^3[47]/g) != null)
        return "cc-amex";

    let re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
    if (number.match(re) != null)
      return "cc-discover";

    re = new RegExp("^36");
    if (number.match(re) != null)
      return "cc-diners-club";

    re = new RegExp("^35(2[89]|[3-8][0-9])");
    if (number.match(re) != null)
      return "cc-jcb";

    return 'credit-card';
  },
  abbreviate: (quantity) => {
    if (quantity >= 1000000000) {
      return `${(quantity / 1000000000).toFixed(1)}B`;
    } else if (quantity >= 1000000) {
      return `${(quantity / 1000000).toFixed(1)}B`;
    } else if (quantity >= 10000) {
      return `${(quantity / 1000).toFixed(1)}K`;
    }
    return quantity.toString();
  },
  cloudinarize: (uri, transformations = '', png = false) => {

    if (uri && uri.includes('https://res.cloudinary.com/lgxy')) {
      return uri;
    }

    if (png) {
      uri = uri.split(".")[0] + '.png';
    }
    return `https://res.cloudinary.com/lgxy/image/upload/${transformations}${uri}`;
  },
  getType: (type) => {
    if (type === "shirtTxB") return "Tie Dye Shirt";
    if (type === "shirtT") return "T-Shirt";
    if (type.includes("shirt")) return 'Shirt';
    if (type.includes("hoodie")) return 'Hoodie';
    if (type === "shoe_AF") return 'Shoe';
    if (type === "popsocket") return 'PopSocket';
    if (type === "mug") return 'Mug';
    if (type === "pillow") return 'Square Pillow';
    if (type === "keychain") return 'Keychain';

    return 'Item';
  },
  getAvailableColors: (type, color = 'default') => {
    if (color !== 'default') {
      let updatedColors = Colors[type] ? Colors[type].slice() : [];
      let index = updatedColors.findIndex(c => c.value === color);
      let colorInfo = updatedColors[index];
      if (!colorInfo) return updatedColors;

      colorInfo.prioritize = true;
      updatedColors.splice(index, 1);
      updatedColors = [colorInfo, ...updatedColors];

      return updatedColors;
    }
    return Colors[type] ? Colors[type] : [];
  },
  getColor: (type, color, raw = false, labelize = false) => {
    if (raw) {
      return Colors[type].find(colorMap => colorMap.value === color).color.slice(1);
    }

    if (labelize) {
      return (Colors[type].find((colorMap) => colorMap.value === color) || {label: 'TBD'}).label;
    }

    return Colors[type].find(colorMap => colorMap.value === color).color;
  },
  formatSize: (size) => {
    if (size === 'XS') {
      return 'Extra Small';
    }

    if (size === 'S') {
      return 'Small';
    }

    if (size === 'M') {
      return 'Medium';
    }

    if (size === 'L') {
      return 'Large';
    }

    if (size === 'XL') {
      return 'Extra Large';
    }

    if (size === 'XXL') {
      return 'Extra Extra Large';
    }

    return 'Default';
  },
  verifyType: (type) => {
    if (['shirtT', 'hoodie', 'mug'].includes(type)) return type;

    return 'Icons';
  },
  getAvailableSizes: (type) => {
    if (type === 'hoodie') return ['S', 'M', 'L', 'XL'];
    if (type === 'shirtT_Sleeve') return ['S', 'M', 'L', 'XL'];
    if (type === 'cap') return [];
    if (type === 'shoe_AF') return ['M3.5', 'M4', 'M4.5', 'M5', 'M5.5', 'M6', 'M6.5', 'M7', 'M7.5', 'M8', 'M8.5', 'M9', 'M9.5', 'M10', 'M10.5', 'M11', 'M11.5', 'M12', 'M12.5', 'M13', 'M13.5'];

    return ['XS', 'S', 'M', 'L', 'XL', 'XXL'];
  },
  getReactivePlaceholder: (reactivity) => {
    if (reactivity === 'songname') return 'Interlude - SteezyLaFlame';

    return 'Reactive Text';
  },
  formatSide: (side) => {
    if (side === 'fullfrontal') return 'Full Frontal';
    if (side === 'leftchest') return 'Left Chest';
    if (side === 'rightchest') return 'Right Chest';
    if (side === 'mediumfront') return 'Medium Front';
    if (side === 'centerfront') return 'Center Front';
    if (side === 'acrosschest') return 'Across Chest';
    if (side === 'leftvertical') return 'Left Vertical';
    if (side === 'rightvertical') return 'Right Vertical';
    if (side === 'bottomleft') return 'Bottom Left';
    if (side === 'bottomright') return 'Bottom Right';
    if (side === 'fullback') return 'Full Back';
    if (side === 'mediumback') return 'Medium Back';
    if (side === 'lockerpatch') return 'Locker Patch';
    if (side === 'acrossback') return 'Across Back';
    if (side === 'fullbottomback') return 'Full Bottom Back';
    if (side === 'leftsleeve') return 'Left Sleeve';
    if (side === 'rightsleeve') return 'Right Sleeve';
    if (side === 'frontal') return 'Frontal';
    if (side === 'accrosspocket') return 'Accross Pocket';
    if (side === 'leftwrist') return 'Left Wrist';
    if (side === 'leftsleeve') return 'Left Sleeve';
    if (side === 'leftshoulder') return 'Left Shoulder';
    if (side === 'rightwrist') return 'Right Wrist';
    if (side === 'rightsleeve') return 'Right Sleeve';
    if (side === 'rightshoulder') return 'Right Shoulder';
    if (side === 'leftbackwrist') return 'Left Back Wrist';
    if (side === 'leftbacksleeve') return 'Left Back Sleeve';
    if (side === 'leftbackshoulder') return 'Left Back Shoulder';
    if (side === 'rightbackwrist') return 'Right Back Wrist';
    if (side === 'rightbacksleeve') return 'Right Back Sleeve';
    if (side === 'rightbackshoulder') return 'Right Back Shoulder';
    if (side === 'rightfullfrontal') return 'Right (Out) Full Frontal';
    if (side === 'rightcenterfront') return 'Right (Out) Center Front';
    if (side === 'leftfullfrontal') return 'Left (Out) Full Frontal';
    if (side === 'leftcenterfront') return 'Left (Out) Center Front';
    if (side === 'songname') return 'Song Name';

    return '';
  }
}
