import React from 'react';
import {
  isIOS,
  isAndroid,
  CustomView
} from "react-device-detect";
import { NavLink } from 'react-router-dom'

// Styles
import '../styles/Landing.css';

function Landing() {
  return (
    <div className="astro">
      <div className="astro-pacity"></div>
      <nav className="has-shadow main isOpaque flexible defaultRowContainer">
        <div className="spacer10"></div>
        <NavLink activeClassName="navbar-active" className="nunito navbar-tab fullHeight center tertiary" to="/shop/shirtT">T-Shirts</NavLink>
        <NavLink activeClassName="navbar-active" className="navbar-tab fullHeight center tertiary" to="/shop/hoodie">Hoodies</NavLink>
        <NavLink activeClassName="navbar-active" className="navbar-tab fullHeight center tertiary" to="/shop/pillow">Pillows</NavLink>
        <NavLink activeClassName="navbar-active" className="navbar-tab fullHeight center tertiary" to="/shop/mug">Mugs</NavLink>
        <NavLink activeClassName="navbar-active" className="navbar-tab fullHeight center tertiary" to="/shop/popsocket">PopSockets</NavLink>
        <div className="spacer10"></div>
      </nav>
      <section className="defaultSection astro-height fullWidth center scrollable-resize reversable-resize defaultColumnContainer">
        <h1 className="nunito bold tertiary landingHeader">Kickstart Your Merch</h1>
        <div className="spacer5"></div>
        <h3 className="tertiary nunito textCenter landingDescription">Start selling your designs on clothing at no cost.<br/>We'll handle production and delivery.<br/>You handle making awesome designs!</h3>
        <div className="spacer10"></div>
        <div className="blast-off clickable">
          <CustomView condition={isIOS}>
            <a className="flexible center nunito bold" href='https://apps.apple.com/us/app/id1520164347'>Blast Off!</a>
          </CustomView>
          <CustomView condition={isAndroid}>
            <a className="flexible center nunito bold" href='https://play.google.com/store/apps/details?id=com.lgxy.tailori&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'>Blast Off!</a>
          </CustomView>
          <CustomView condition={!isAndroid && !isIOS}>
            <NavLink className="flexible center nunito bold" to="download">Blast Off!</NavLink>
          </CustomView>
        </div>
      </section>
    </div>
  );
}

export default Landing;
