import React from 'react';
import qs from 'qs';

import config from '../Constants';

function BrandSuccess(props) {

  return (
    <section id="success-refresh-brand" className="defaultBanner center defaultColumnContainer primaryFill">
      <div className="spacer"></div>
      {
        !props.refresh &&
        <>
          <h1 className="center tertiary nunito header">You're all set!</h1>
          <div className="spacer10"></div>
          <h4 className="spacer center tertiary halfWidth textCenter para">
            <span>You have finished setting up your account. Now, simply go back to the app and tap 'I did it!' to load your brand! Enjoy!</span>
          </h4>
          <br />
          <div className="blast-off clickable">
            <a className="flexible center nunito bold" href='tailori://'>Tap to Open Tailori!</a>
          </div>
        </>
      }
      {
        props.refresh &&
        <>
          <h1 className="center tertiary nunito header">Whoops!</h1>
          <div className="spacer10"></div>
          <h4 className="spacer center tertiary halfWidth textCenter para">
            <span>This link can only be used once! If you're here by mistake, don't worry simply go back to the app. Thank You!</span>
          </h4>
        </>
      }
      <div className="spacer"></div>
    </section>
  );
}

export default BrandSuccess;
