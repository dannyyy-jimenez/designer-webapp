import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMobile, faHome, faCreditCard, faReceipt, faHeart, faHandHoldingHeart } from '@fortawesome/free-solid-svg-icons'
import {
  CardElement,
  useStripe,
  useElements,
} from '@stripe/react-stripe-js';
import Download from './Download';
import API from '../Api';
import {
  isIOS,
  isAndroid,
  CustomView
} from "react-device-detect";

// Styles
import '../styles/Landing.css';

const formats = require('./Formats').Formats;

function Pay() {
  const [bag, setBag] = React.useState('');
  const [hasCart, setHasCart] = React.useState(false);
  const [isLoading, setIsLoading] =  React.useState(true);
  const [section, setSection] = React.useState(1);
  const [phoneNumber, setPhoneNumber] = React.useState('');
  const [name, setName] = React.useState('');
  const [streetAddress, setStreetAddress] = React.useState('');
  const [streetAddressCont, setStreetAddressCont] = React.useState('');
  const [city, setCity] = React.useState('');
  const [state, setState] = React.useState('');
  const [postal, setPostal] = React.useState('');
  const [country, setCountry] = React.useState('');
  const [shippingValid, setShippingValid] = React.useState(false);
  const [validPayment, setValidPayment] = React.useState(false);
  const [paymentToken, setPaymentToken] = React.useState('');
  const [modelCode, setModelCode] = React.useState('');
  const [modelCodeHasFocus, setModelCodeHasFocus] = React.useState(false);
  const [orderIdentifier, setOrderIdentifier] = React.useState('');
  const [error, setError] = React.useState('');
  const [subtotal, setSubtotal] = React.useState('');
  const [shipping, setShipping] = React.useState('');
  const [tax, setTax] = React.useState('');
  const [taxAmount, setTaxAmount] = React.useState('');
  const [total, setTotal] = React.useState('');
  const [modelDiscount, setModelDiscount] = React.useState('');

  const stripe = useStripe();
  const elements = useElements();

  const formatPhoneNumber = (phoneNumberString) => {
    var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
    var match = cleaned.match(/^(\d{3})(\d{1,3})?(\d{1,4})?$/)
    if (match) {
      return '(' + match[1] + (match[1] && match[2] ? ') ' : '') + (match[2] || '') + (match[2] && match[3] ? '-' : '') + (match[3] || '')
    }
    return phoneNumberString;
  };

  const onPhoneChange = (e) => {
    setPhoneNumber(formatPhoneNumber(e.target.value));
  };

  const onPaymentChange = (e) => {
    setValidPayment(e.complete);
  }

  const onPaymentValidate = async () => {
    setIsLoading(true);
    const cardElement = await stripe.createToken(elements.getElement(CardElement));
    if (cardElement.error) {
      setError('Error validating payment');
      setIsLoading(false);
      return;
    }

    setPaymentToken(cardElement.token.id);

    const res = await API.get('store/pay', {bag, name, streetAddress, streetAddressCont, city, state, postal, country, modelCode});
    if (res.isError) {
      setError('Error getting checkout details');
      setIsLoading(false);
      return;
    }

    setError('');
    setSubtotal(res.data._st);
    setShipping(res.data._s);
    setTax(res.data._tx);
    setTaxAmount(res.data._ta);
    setTotal(res.data._t);
    setModelDiscount(res.data._d);
    setSection(4);
    setIsLoading(false);
  }

  const formatURIQuery = () => {
    return [streetAddress, city, state, country].join(' ');
  };

  const onShippingValidate = () => {
    API.address(formatURIQuery()).then(async ([res, coordinates]) => {
      setIsLoading(true);
      setStreetAddress(res.Label.split(',')[0]);
      setCity(res.City);
      setState(res.State);
      setPostal(res.PostalCode);
      setCountry(res.Country);

      setSection(3);
      setIsLoading(false);
    }).catch((e) => {
      setShippingValid(false);
      setIsLoading(false);
    });
  }

  const onPay = async () => {
    setIsLoading(true);
    setError('');

    try {
      const res = await API.post('store/order', {bag, modelCode, paymentToken, name, phoneNumber, streetAddress, streetAddressCont, city, state, postal, country, total});
      if (res.isError) {
        if (res.response === "MISMATCH_TOTAL") throw 'Please refresh page';
        if (res.response === "MISMATCH_COUNTRY") throw 'Unfortunately, we do not support shipping out of the country, yet!';
        if (res.response === "INVALID_NUM") throw 'Invalid Phone Number';
        if (res.response === "PAYMENT_ERROR") throw 'Error processing payment';

        throw 'Error placing order';
        return;
      }
      localStorage.removeItem("_CRT_TLR");
      setOrderIdentifier(res.data._o);
      setIsLoading(false);
      setSection(5);
    } catch (e) {
      setError(e);
      setIsLoading(false);
    }
  }

  React.useEffect(() => {
    if (streetAddress === '') {
      setShippingValid(false);
      return;
    }

    if (city === '') {
      setShippingValid(false);
      return;
    }

    if (state === '') {
      setShippingValid(false);
      return;
    }

    if (country === '') {
      setShippingValid(false);
      return;
    }

    if (postal === '') {
      setShippingValid(false);
      return;
    }

    setShippingValid(true);
  }, [streetAddress, city, country, state, postal]);

  React.useEffect(() => {
    if (!localStorage.getItem("_CRT_TLR")) {
      setIsLoading(false);
      return;
    };

    setBag(localStorage.getItem("_CRT_TLR"))
    setHasCart(true);
    setIsLoading(false);
  }, []);

  return (
    <section className="defaultSection tertiaryFill fullWidth center hasContainer">
      <br/>
      <br/>
        {
          !hasCart && !isLoading &&
          <h4 className="tertiary fullWidth textCenter">You have no active cart</h4>
        }
        {
          hasCart && section === 1 &&
          <div className="defaultColumnContainer center container">
            <FontAwesomeIcon className="tertiary headerIcon" icon={faMobile} />
            <br/>
            <br/>
            <input type="text" autoComplete="name" value={name} onChange={(e) => setName(e.target.value)} placeholder="Full Name" className="defaultInput tertiary textCenter" />
            <br/>
            <input type="tel" autoComplete="tel" value={phoneNumber} onChange={(e) => onPhoneChange(e)} placeholder="Phone Number" className="defaultInput tertiary textCenter" />
            <br/>
            <button disabled={phoneNumber.length < 9 || name.length === 0} className="defaultButton rounded tertiary primaryFill textCenter" onClick={() => setSection(2)}>Continue</button>
          </div>
        }
        {
          section === 2 &&
          <div className="defaultColumnContainer center container">
            <FontAwesomeIcon className="tertiary headerIcon" icon={faHome} />
            <br/>
            <br/>
            <input type="text" autoComplete="street-address" value={streetAddress} onChange={(e) => setStreetAddress(e.target.value)} placeholder="Street Address" className="defaultInput tertiary textCenter" />
            <br/>
            <input type="text" autoComplete="street-address-cont" value={streetAddressCont} onChange={(e) => setStreetAddressCont(e.target.value)} placeholder="Street Address Cont." className="defaultInput tertiary textCenter" />
            <br/>
            <div className="defaultRowContainer defaultInputCont">
              <input type="text" autoComplete="address-level2" value={city} onChange={(e) => setCity(e.target.value)} placeholder="City" className="defaultInput inline tertiary textCenter big" />
              <input type="text" autoComplete="address-level1" value={state} onChange={(e) => setState(e.target.value)} placeholder="State" className="defaultInput inline tertiary textCenter small" />
            </div>
            <br/>
            <div className="defaultRowContainer defaultInputCont">
              <input type="text" autoComplete="postal-code" value={postal} onChange={(e) => setPostal(e.target.value)} placeholder="Postal Code" className="defaultInput inline tertiary textCenter big" />
              <input type="text" autoComplete="country" value={country} onChange={(e) => setCountry(e.target.value)} placeholder="Country" className="defaultInput inline tertiary textCenter small" />
            </div>
            <br/>
            <button disabled={!shippingValid || isLoading} className="defaultButton rounded tertiary primaryFill textCenter" onClick={() => onShippingValidate()}>Continue</button>
          </div>
        }
        {
          section === 3 &&
          <div className="defaultColumnContainer center container">
            <FontAwesomeIcon className="tertiary headerIcon" icon={modelCodeHasFocus ? faHandHoldingHeart : faCreditCard} />
            <br/>
            <br/>
            <CardElement
              onChange={(e) => onPaymentChange(e)}
              className="fullWidth"
              options={{
                style: {
                  base: {
                    fontFamily: 'Open Sans, sans-serif',
                    iconColor: '#fff',
                    fontSize: '16px',
                    color: '#ffffff',
                    '::placeholder': {
                      color: 'rgba(255, 255, 255, 0.7)',
                    },
                  },
                  invalid: {
                    color: 'red',
                  },
                },
              }}
            />
            <br/>
            {/* <br/> */}
            {/* <input onFocus={() => setModelCodeHasFocus(true)} onBlur={() => setModelCodeHasFocus(false)} type="text" autoComplete="discount-code" value={modelCode} onChange={(e) => setModelCode(e.target.value)} placeholder="Have Model Code?" className="defaultInput tertiary textCenter" /> */}
            <br/>
            <button disabled={!validPayment || isLoading} className="defaultButton rounded tertiary primaryFill textCenter" onClick={() => onPaymentValidate()}>Continue</button>
          </div>
        }
        {
          section === 4 &&
          <div className="defaultColumnContainer totalCont container">
            <div className="defaultRowContainer center">
              <FontAwesomeIcon className="tertiary headerIcon" icon={faReceipt} />
            </div>
            <br/>
            <br/>
            <div className="defaultRowContainer">
              <p className="tertiary">Subtotal</p>
              <div className="spacer"></div>
              <p className="tertiary">${subtotal}</p>
            </div>
            <div className="defaultRowContainer">
              <p className="tertiary">Shipping</p>
              <div className="spacer"></div>
              <p className="tertiary">${shipping}</p>
            </div>
            {
              modelDiscount !== "" && modelDiscount !== "0.00" &&
              <div className="defaultRowContainer">
                <p className="tertiary">Discount</p>
                <div className="spacer"></div>
                <p className="tertiary">-${modelDiscount}</p>
              </div>
            }
            <div className="defaultRowContainer">
              <p className="tertiary">Tax ({taxAmount}%)</p>
              <div className="spacer"></div>
              <p className="tertiary">${tax}</p>
            </div>
            <br/>
            <br/>
            <div className="defaultRowContainer">
              <p className="tertiary bold">Total</p>
              <div className="spacer"></div>
              <p className="tertiary bold">${total}</p>
            </div>
            <br/>
            {
              error !== '' &&
              <p className="tertiary bold fullWidth textCenter">{error}</p>
            }
            <button disabled={isLoading} className="defaultButton rounded tertiary primaryFill textCenter" onClick={() => onPay()}>Pay</button>
            <p className="tertiary opaque tiny fullWidth textCenter">By clicking 'Pay' you agree to our Terms of Service and Privacy Policy</p>
          </div>
        }
        {
          section === 5 &&
          <div className="defaultColumnContainer center container">
            <FontAwesomeIcon className="tertiary headerIcon" icon={faHeart} />
            <br/>
            <br/>
            <p className="tertiary bold fullWidth textCenter">Order #{orderIdentifier}</p>
            <br/>
            <p className="tertiary opaque tiny fullWidth textCenter">Order should be sent out for delivery in 5-7 business days</p>
            <br/>
            <p className="tertiary opaque tiny fullWidth textCenter">Updates will be provided through SMS or in-app updates</p>
            <br/>
            <p className="tertiary bold fullWidth textCenter">👇👇 Download Now 👇👇</p>
            <CustomView condition={isIOS}>
              <a className="download-icon" href='https://apps.apple.com/us/app/id1520164347'><img className="download-icon" alt="Get it on the Apple App Store" src="https://res.cloudinary.com/lgxy/image/upload/w_200/tailori/app-store-badge.svg" alt="app store badge"/></a>
            </CustomView>
            <CustomView condition={isAndroid}>
              <a className="download-icon" href='https://play.google.com/store/apps/details?id=com.lgxy.tailori&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/></a>
            </CustomView>
          </div>
        }
    </section>
  );
}

export default Pay;
