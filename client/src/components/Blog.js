import React from 'react';
import { MailFilled, HeartOutlined, HeartFilled, InstagramOutlined, TwitterOutlined, FacebookOutlined } from '@ant-design/icons';
import { NavLink } from 'react-router-dom'
import LoadingBar from 'react-top-loading-bar'

// Styles
import '../styles/Landing.css';

function Blog() {
  const ref = React.useRef(null)

  const [hearted, setHearted] = React.useState(false);
  const [hearts, setHearts] = React.useState(1);

  const [socials, setSocials] = React.useState([]);
  const [effects, setEffects] = React.useState([]);
  const [title, setTitle] = React.useState("Lorem Ipsum");
  const [sections, setSections] = React.useState([
    {
      title: "ded",
      content: [
        {
          type: "uri",
          content: "hi"
        }
      ]
    }
  ]);

  React.useEffect(() => {
    setHearts(hearted ? hearts + 1 : hearts - 1);
    // api call
  }, [hearted])

  const GetSocialUri = (social) => {
    return social.split(":").slice(1).join('');
  }

  return (
    <>
      <LoadingBar color='#FFF' ref={ref} height={3} className="loader-bar" />
      <section className="defaultSection fullWidth scrollable defaultColumnContainer">
        <div className="blog-promo defaultColumnContainer">
          <h4 className="fullWidth primary bold nunito">Follow Us!</h4>
          <h5 className="fullWidth secondary light nunito">Check out our social media channels to keep up with our blogs!</h5>
          <div onClick={() => setHearted(!hearted)} className="socials clickable flexible center">
            {
              hearted &&
              <HeartFilled className="filled" />
            }
            {
              !hearted &&
              <HeartOutlined />
            }
            {
              hearts > 0 &&
              <h4 className="social-label nunito secondary opaque">{hearts}</h4>
            }
          </div>
          <div className="socials flexible center">
            <a href="https://www.instagram.com/tailoriapp" target="_blank" rel="noopener noreferrer" ></a>
            <InstagramOutlined />
          </div>
          <div className="socials flexible center">
            <a href="https://www.facebook.com/tailoriapp" target="_blank" rel="noopener noreferrer" ></a>
            <FacebookOutlined />
          </div>
          <div className="socials flexible center">
            <a href="https://www.twitter.com/tailoriapp" target="_blank" rel="noopener noreferrer" ></a>
            <TwitterOutlined />
          </div>
        </div>
        <br/>
        <h1 className="center primary header bold nunito">
          {effects.includes('congrats') ? '🎉 ' : ''}
          {title}
          {effects.includes('congrats') ? ' 🎉' : ''}
        </h1>
        {
          socials.length > 0 &&
          <div className="halfWidth forced defaultRowContainer center">
            {
              socials.map(social => {
                return (
                  <div className="socials big flexible center">
                    <a href={GetSocialUri(social)} target="_blank" rel="noopener noreferrer" ></a>
                    {
                      social.includes('mail') &&
                      <MailFilled />
                    }
                    {
                      social.includes('fb') &&
                      <FacebookOutlined />
                    }
                    {
                      social.includes('ig') &&
                      <InstagramOutlined />
                    }
                    {
                      social.includes('tw') &&
                      <TwitterOutlined />
                    }
                  </div>
                )
              })
            }
          </div>
        }
        <br/>
        {
          sections.map(section => {
            return (
              <div className="halfWidth forced defaultRowContainer center">
                <h2 className="nunito fullWidth">{section.title}</h2>
                {
                  section.content.map(cont => {
                    if (cont.type === 'uriximg') {
                      return <img src={cont.content} alt="blog spotlights" className="blog-image"/>
                    } else if (cont.type === 'uri') {
                      return <a href={cont.content} alt="blog link" className="fullWidth primary nunito light">{cont.content}</a>
                    }

                    return <p className="fullWidth light nunito">{cont.content}</p>
                  })
                }
              </div>
            )
          })
        }
        <br/>
        <h4 className="center halfWidth textCenter para">
          <span>
            <span>For any spelling errors or questions, contact us at </span>
            <a href={"mailto:support@tailorii.app"} className='primary'>support@tailorii.app</a>
          </span>
        </h4>
      </section>
    </>
  );
}

export default Blog;
