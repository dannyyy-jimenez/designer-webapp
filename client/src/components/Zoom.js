import React from 'react';
import { NavLink } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMobile, faMinusCircle, faPlusCircle, faEyeSlash } from '@fortawesome/free-solid-svg-icons'
import Constants from '../Constants';
import Loader from 'react-loader-spinner'
import {Helmet} from "react-helmet";
import ReactPixel from 'react-facebook-pixel';

import API from '../Api';
import Merch from '../models/Merch';

// Styles
import '../styles/Store.css';

const formats = require('./Formats').Formats;

function Zoom({match, history}) {
  const [isLoading, setIsLoading] = React.useState(true);
  const [release, setRelease] = React.useState({
    getIdentifier: () => "",
    getShots: () => [],
    getName: () => "",
    getDescription: () => "",
    getType: () => "",
    getPrice: () => "",
    designer: {
      getIdentifier: () => "",
      getName: () => ""
    },
    isLimited: () => false,
    actions: []
  });
  const [size, setSize] = React.useState('default');
  const [color, setColor] = React.useState('default');
  const [quantity, setQuantity] = React.useState(1);
  const [actions, setActions] = React.useState(release.actions.slice());
  const [firstLoad, setFirstLoad] = React.useState(true);
  const [noRelease, setNoRelease] = React.useState(false);
  const [showAllColors, setShowAllColors] = React.useState(false);
  const [baseColor, setBaseColor] = React.useState('BLCK');
  const [shots, setShots] = React.useState([]);
  let [baseShots, setBaseShots] = React.useState([]);
  const [currentShot, setCurrentShot] = React.useState('');
  const [canPurchase, setCanPurchase] = React.useState(false);
  const [showModal, setShowModal] = React.useState(false);

  const load = async () => {
    try {
      const res =  await API.get('store/zoom', {release: match.params.identifier});
      if (res.isError) throw 'error';

      setRelease(new Merch(res.data._r.design, res.data._r.designer, res.data._r.identifier, res.data._r.price, res.data._r.remaining, res.data._r.customizables, res.data._r.actions));
      setActions(res.data._r.actions);
      setBaseColor(res.data._bc);
      setColor(res.data._bc);
      setBaseShots(res.data._r.design.shots);
      setCurrentShot(res.data._r.design.shots[0]);
      setShots(res.data._r.design.shots);
      setIsLoading(false);
      ReactPixel.track('ViewContent', {content_name: res.data._r.design.name, content_ids: [res.data._r.identifier], content_type: 'product', value: (parseFloat(res.data._r.price) - 2), currency: 'USD'});
    } catch (e) {
      setIsLoading(false);
      setNoRelease(true);
    };
  };

  const formatCart = (activeCart) => {
    return (activeCart.length > 0 ? '$' : '') + `${release.identifier}_#${quantity}_@${size}_&${color}`;
  };

  const onSelect = async () => {
    if (!canPurchase) return;
    const activeCart = localStorage.getItem('_CRT_TLR') || '';
    localStorage.setItem('_CRT_TLR', activeCart + formatCart(activeCart));
    history.push("/shop/checkout/bag");
  }

  const onBackDrop = (e) => {
    e.persist();
    if (e.target.className === 'modal-container') {
      setShowModal(false);
    }
  }

  React.useEffect(() => {
    load();
  }, [match.params.identifier])

  React.useEffect(() => {
    if (actions.includes('sizeable') && size === 'default') {
      setCanPurchase(false);
      return;
    }

    setCanPurchase(true);
  }, [size, color, actions])

  React.useEffect(() => {
    if (color === 'default' || color === baseColor) {
      if (baseShots.length > 0) {
        setCurrentShot(baseShots[0]);
        setShots(baseShots);
      }
      return;
    }

    setIsLoading(true);

    API.get('store/zoom/color', {release: match.params.identifier, color: color}).then((res) => {
      if (res.isError) throw 'error';

      setCurrentShot(res.data._s[0]);
      setShots(res.data._s);
      setIsLoading(false);
    }).catch(e => {
      setIsLoading(false);
    });

  }, [color]);

  return (
    <>
      {
        !noRelease && !isLoading &&
        <Helmet defaultTitle="Tailori" defer={false}>
            <title>{release.getName()} from {release.designer.getName()}</title>
            <meta name="title" content={`${release.getName()} from ${release.designer.getName()}`} />
            <meta property="og:title" content={`${release.getName()} from ${release.designer.getName()}`} />
            <meta property="og:url" content={`https://www.tailorii.app/shop/zoom/${release.getIdentifier()}`} />
            <meta property="twitter:title" content={`${release.getName()} from ${release.designer.getName()}`} />
            <meta property="product:brand" content={release.designer.getName()} />
            <meta property="product:availability" content="in stock" />
            <meta property="product:condition" content="new" />
            <meta property="og:image" content={formats.cloudinarize(release.getShots()[0])} />
            <meta property="og:price:amount" content={release.getPrice()} />
            <meta property="product:price:amount" content={release.getPrice()} />
            <meta property="product:price:currency" content="USD" />
        </Helmet>
      }
      <section className="defaultSection fullWidth scrollable scrollable-resize tertiaryFill defaultRowContainer">
        {
          isLoading &&
          <div className="absoluteSection flexible center">
            <Loader type="ThreeDots" color="#DB113B" height={30} width={30} />
          </div>
        }
        {
          noRelease &&
          <div className="absoluteSection defaultColumnContainer flexible center">
            <FontAwesomeIcon className="inex primary" icon={faEyeSlash} />
            <br/>
            <h3 className="secondary">What You are Looking for Does Not Exist</h3>
          </div>
        }
        <div className="spacer defaultColumnContainer center">
          <div className="resize-show">
            <h1 className="secondary center fullWidth bold catalogHeader">{release.getName()}</h1>
            <h3 className="secondary textCenter fullWidth light catalogDesc">{release.getType()} from <NavLink to={"/shop/brand/"+release.designer.getIdentifier()} className="primary bold">{release.designer.getName()}</NavLink></h3>
            <br/>
          </div>
          <div className="catalogMainImageCont">
            {
              currentShot !== "" &&
              <img className="catalogMainImage" src={formats.cloudinarize(currentShot)} alt="shot" />
            }
            <div className="catalogPrice center primaryFill tertiary bold">${release.getPrice()}</div>
          </div>
          <div className="spacer10"></div>
          <div className="defaultRowContainer catalogShotsAltCont center fullWidth">
            {
              shots.map((shot) => {
                return <img onClick={() => setCurrentShot(shot)} className={`catalogAltImg clickable ${currentShot === shot ? 'activeCatShot' : ''}`} src={formats.cloudinarize(shot)} alt="shot"/>
              })
            }
          </div>
        </div>
        <div className="defaultColumnContainer catalogContainer fullHeight scrollable landingText">
          <div className="spacer20"></div>
          <div className="spacer10"></div>
          <div className="resize-hide">
            <h1 className="secondary bold catalogHeader">{release.getName()}</h1>
            <h3 className="secondary light catalogDesc">{release.getType()} from <NavLink to={"/shop/brand/"+release.designer.getIdentifier()} className="primary bold">{release.designer.getName()}</NavLink></h3>
            <br/>
          </div>
            <h3 className="secondary light catalogDesc">{release.getDescription()}</h3>
            <br/>
          <div className="resize-hide defaultColumnContainer">
            <p className="catalogMessage secondary">Free Shipping on Orders Over $25</p>
            <br/>
            {
              actions.includes('colorable') &&
              <>
                <p className="secondary bold">Choose {release.getType()} Color</p>
                <div className="defaultRowContainer center">
                  {
                    formats.getAvailableColors(release.type, color).map(c => {
                      if (!c.prioritize && !showAllColors) {
                        return <></>
                      }
                      return <div className={`size-container color flexible center secondary showing ${color === c.value ? 'disabled' : ''}`} onClick={() => {setColor(c.value); setShowAllColors(false)}} style={{backgroundColor: c.color}}><p className="flexible center">{c.label}</p></div>
                    })
                  }
                  <br/>
                  <br/>
                  {
                    !showAllColors &&
                    <p onClick={() => setShowAllColors(true)} className="flexible fullWidth primary clickable bold center marginize-top">More Colors</p>
                  }
                  {
                    showAllColors &&
                    <p onClick={() => setShowAllColors(false)} className="flexible fullWidth primary clickable bold center">Less Colors</p>
                  }
                </div>
              </>
            }
            {
              actions.includes('sizeable') &&
              <>
                <p className="secondary bold">Choose {release.getType()} Size</p>
                <div className="defaultRowContainer center">
                  {
                    formats.getAvailableSizes(release.type).map(size => {
                      return <div className="size-container flexible center secondary" onClick={() => setSize(size)}>{size}</div>
                    })
                  }
                </div>
              </>
            }
            <br/>
            <br/>
            <div className="defaultRowContainer center">
              <FontAwesomeIcon disabled={quantity === 1} className="iconpart secondary quantityIcon" onClick={() => setQuantity(quantity - 1)} icon={faMinusCircle} />
              <h1 className="primary iconpart">{quantity}</h1>
              <FontAwesomeIcon className="iconpart secondary quantityIcon" onClick={() => setQuantity(quantity + 1)} icon={faPlusCircle} />
            </div>
            <br/>
            <br/>
            {
              actions.includes('reactive') &&
              <>
                <p className="catalogMessage center textCenter secondary">Download our App to Purchase Reactive Releases</p>
                <br/>
              </>
            }
            {
              !actions.includes('reactive') &&
              <button disabled={!canPurchase} className="defaultButton rounded tertiary primaryFill textCenter" onClick={() => onSelect()}>Add to Cart</button>
            }
          </div>
          <div className="resize-show">
            {
              actions.includes('reactive') &&
              <>
                <p className="catalogMessage center textCenter secondary">Download our App to Purchase Reactive Releases</p>
                <br/>
              </>
            }
            {
              !actions.includes('reactive') &&
              <button className="defaultButton rounded tertiary primaryFill textCenter" onClick={() => setShowModal(true)}>Add to Cart</button>
            }
          </div>
        </div>
        <div className="spacer20"></div>
        <div className="spacer20"></div>
        {
          showModal &&
          <div className="modal-container" onClick={(e) => onBackDrop(e)}>
            <div className="modal">
              <p className={`fullWidth primary bold textCenter toCart ${canPurchase ? '' : 'disabled'}`} onClick={() => onSelect()}>Add to Cart</p>
              <p className="catalogMessage secondary">Free Shipping on Orders Over $25</p>
              {
                actions.includes('colorable') &&
                <>
                  <p className="secondary bold">Choose {release.getType()} Color</p>
                  <div className="defaultRowContainer center">
                    {
                      formats.getAvailableColors(release.type, color).map(c => {
                        if (!c.prioritize && !showAllColors) {
                          return <></>
                        }
                        return <div className={`size-container color flexible center secondary showing ${color === c.value ? 'disabled' : ''}`} onClick={() => {setColor(c.value); setShowAllColors(false)}} style={{backgroundColor: c.color}}><p className="flexible center">{c.label}</p></div>
                      })
                    }
                    <br/>
                    <br/>
                    {
                      !showAllColors &&
                      <p onClick={() => setShowAllColors(true)} className="flexible fullWidth primary clickable bold center">More Colors</p>
                    }
                    {
                      showAllColors &&
                      <p onClick={() => setShowAllColors(false)} className="flexible fullWidth primary clickable bold center">Less Colors</p>
                    }
                  </div>
                </>
              }
              {
                actions.includes('sizeable') &&
                <>
                  <p className="secondary bold">Choose {release.getType()} Size</p>
                  <div className="defaultRowContainer center">
                    {
                      formats.getAvailableSizes(release.type).map(s => {
                        return <div className={`size-container sizee flexible center secondary ${size === s ? 'disabled' : ''}`} onClick={() => setSize(s)}>{s}</div>
                      })
                    }
                  </div>
                </>
              }
              <br/>
              <br/>
              <div className="defaultRowContainer center">
                <FontAwesomeIcon disabled={quantity === 1} className="iconpart secondary quantityIcon" onClick={() => setQuantity(quantity - 1)} icon={faMinusCircle} />
                <h1 className="primary iconpart">{quantity}</h1>
                <FontAwesomeIcon className="iconpart secondary quantityIcon" onClick={() => setQuantity(quantity + 1)} icon={faPlusCircle} />
              </div>
            </div>
          </div>
        }
      </section>
    </>
  );
}

export default Zoom;
