import React from 'react';
import { NavLink } from 'react-router-dom'

import API from '../Api';
import Merch from '../models/Merch';

// Styles
import '../styles/Store.css';

const formats = require('./Formats').Formats;

function Catalog({match}) {
  const [isLoading, setIsLoading] = React.useState(true);
  const [baseMerch, setBaseMerch] = React.useState([]);

  const load = async () => {
    try {
      const res =  await API.get('store', {type: match.params.type});
      if (res.isError) throw 'error';

      setBaseMerch(res.data._r.map(release => new Merch(release.design, release.designer, release.identifier, release.price, release.remaining, release.customizables, release.actions)));
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
    };
  };

  React.useEffect(() => {
    load();
  }, [match.params.type])

  return (
    <>
      <section id="catalog-image" style={{backgroundImage: `url(https://res.cloudinary.com/lgxy/image/upload/h_440/tailori/${formats.verifyType(match.params.type)}.png)`}} className="smallBanner scrollable defaultColumnContainer primaryFill">
        <h1 className="center tertiary bold nunito header">{formats.getType(match.params.type)}s</h1>
        <h4 className="spacer center tertiary halfWidth bold opaque textCenter para">
          <span>Quality {formats.getType(match.params.type)}s made for you by various creators! Perfect for gifts and everyday wear.</span>
        </h4>
      </section>
      <section className="defaultSection scrollable centered padded insetWidth defaultRowContainer">
        {
          baseMerch.map(release => {
            return (
              <div className="card defaultColumnContainer">
                <img src={formats.cloudinarize(release.getCover(), '', true)} alt="release" className="card-image"/>
                <p className="price-fit secondary bold tiny">${release.getPrice()}</p>
                <div className="content-container">
                  <p className="nunito secondary title cutoff">{release.getName()}</p>
                  <p className="primary bold tiny">{release.designer.getName()}</p>
                </div>
                <NavLink className="card-director" exact to={"/shop/zoom/"+release.identifier}></NavLink>
              </div>
            )
          })
        }
      </section>
    </>
  );
}

export default Catalog;
