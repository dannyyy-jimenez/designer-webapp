import React from 'react';
import { Steps } from 'antd';

function Royalties() {
  return (
    <section id="royalties" className="defaultSection fullWidth center scrollable-resize reversable-resize defaultRowContainer">
      <div className="spacer20"></div>
      <div className="spacer10"></div>
      <div className="videoContainer">
        <img src="https://res.cloudinary.com/lgxy/image/upload/v1606372298/tailori/web-snap.png" alt="tailori snapshot"/>
      </div>
      <div className="spacer20"></div>
      <div className="defaultColumnContainer padded-resize center fullHeight landingText">
        <h1 className="nunito bold primary landingHeader">Starting to Sell is Easy</h1>
        <div className="spacer10"></div>
        <Steps className="steps nunito" current={1} direction="vertical">
          <Steps.Step title="Discover Tailori" description="You already did the hardest step, finding out about us!" />
          <Steps.Step title="Finish perfecting your designs" description="Completely up to you! Once you've finished up your designs you'll be 50% done!" />
          <Steps.Step title="Upload your designs and launch!" description="It takes less than 2 minutes to launch your designs on Tailori!" />
          <Steps.Step title="Share!" description="Share your designs with your audience, friends, and/or family!" />
        </Steps>
        <div className="spacer10"></div>
        <h2 className="nunito primary landingDescription">What are you waiting for ?</h2>
      </div>
      <div className="spacer10"></div>
      <div className="spacer10"></div>
    </section>
  );
}

export default Royalties;
