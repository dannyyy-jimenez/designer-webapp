import React from 'react';
import { HeartOutlined, HeartFilled } from '@ant-design/icons';
import { NavLink } from 'react-router-dom'
import { Steps, Badge } from 'antd';
import LoadingBar from 'react-top-loading-bar'

// Styles
import '../styles/Landing.css';

function Blogs(props) {
  const ref = React.useRef(null)

  const [isLoading, setIsLoading] = React.useState(false);
  const [heartedIndexes, setHeartedIndexes] = React.useState([]);
  const [papers, setPapers] = React.useState([
    {
      title: 'Lorem Impsum',
      date: '01 / 20 / 21',
      cover: 'https://picsum.photos/200/300',
      hearts: 0
    }
  ]);

  React.useEffect(() => {
    if (!isLoading) {
      ref.current.complete()
    } else {
      ref.current.continuousStart();
    }
  }, [isLoading])

  React.useEffect(() => {
    API.get('/blogs', {}).then(res => {

    }).catch(e => {
      
    })
  }, []);

  return (
    <>
      <LoadingBar color='#FFF' ref={ref} height={3} className="loader-bar" />
      <section id="blogs" className="defaultBanner scrollable center defaultColumnContainer">
        <h1 className="center primary bold header nunito">Blogs</h1>
        <div className="spacer10"></div>
        <br/>
        <div className="spacer center">
          {
            papers.map((paper, index) => {
              return (
                <Badge.Ribbon text={`${paper.hearts} 🤍`}>
                  <div className="card clickable defaultColumnContainer popup">
                    <img src={paper.cover} alt="card cover" className="card-image"/>
                    <p className="price-fit secondary bold super tiny">{paper.date}</p>
                    <h4 className="center spacer primary bold nunito">{paper.title}</h4>
                    <div className="card-actions flexible center defaultRowContainer">
                      <div className="card-action clickable flexible center">
                        <HeartOutlined />
                      </div>
                    </div>
                    <NavLink className="card-director" exact to={"/blogs/id"}></NavLink>
                  </div>
                </Badge.Ribbon>
              )
            })
          }
        </div>
      </section>
      <br/>
      <br/>
      <section id="blogs-extra" className="defaultBanner scrollable center tertiaryFill defaultColumnContainer">
        <div className="spacer"></div>
        <h2 className="center primary bold header nunito">Next Blog Entry is for Your Merch...</h2>
        <div className="spacer10"></div>
        <div className="spacer center">
          <h4 className="nunito light primary textCenter halfWidth">After you launch your merch on Tailori, contact us (our info is right below), and we will write a blog entry about your merch! It's that easy! 🎉</h4>
        </div>
      </section>
    </>
  );
}

export default Blogs;
