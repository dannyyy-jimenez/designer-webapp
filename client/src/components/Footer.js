import React from 'react';
import { Link } from 'react-router-dom'
import { FacebookFilled, InstagramFilled, TwitterCircleFilled, MailFilled } from '@ant-design/icons';

function Footer() {
  return (
    <footer id="contact" className="defaultBanner scrollable tertiaryFill defaultRowContainer">
      <div className="spacer20"></div>
      <div className="spacer20"></div>
      <div className="defaultColumnContainer fullOnResize fullHeight">
        <p className="secondary bold">About</p>
        <div className="spacer10"></div>
        <a href="https://www.dannyyy-jimenez.xyz" className="secondary opaque">Creator</a>
        <a href="https://www.trinumdesign.com/" target="_blank" rel="noopener noreferrer" className="secondary opaque">Our Clothes Customizer</a>
        <Link to={{pathname: '/document', search: '?name=styles'}} className="secondary opaque">Printing Styles</Link>
      </div>
      <div className="spacer20"></div>
      <div className="defaultColumnContainer fullOnResize fullHeight">
        <p className="secondary bold">Documents</p>
        <div className="spacer10"></div>
        <Link to={{pathname: '/document', search: '?name=royalties-explanation'}} className="secondary opaque">How Royalties Work?</Link>
        <Link to={{pathname: '/document', search: '?name=guidelines'}} className="secondary opaque">Uploaded Design Guideliness</Link>
        <Link to={{pathname: '/document', search: '?name=merchandise'}} className="secondary opaque">Merchandise Description</Link>
        <Link to={{pathname: '/document', search: '?name=attributions'}} className="secondary opaque">Attributions</Link>
      </div>
      <div className="spacer20"></div>
      <div className="defaultColumnContainer fullOnResize fullHeight">
        <p className="secondary bold">Support</p>
        <div className="spacer10"></div>
        <Link to={{pathname: '/document', search: '?name=terms'}} className="secondary opaque">Terms of Service</Link>
        <Link to={{pathname: '/document', search: '?name=privacy'}} className="secondary opaque">Privacy Policy</Link>
        <a href="https://www.cookiepolicygenerator.com/live.php?token=1wK1nvvYgn6Wo3yUvpNtPmEPRA1JWoyb" target="_blank" rel="noopener noreferrer" className="secondary opaque">Cookies Policy</a>
        <Link to={{pathname: '/document', search: '?name=dmca'}} className="secondary opaque">DMCA Policy</Link>
      </div>
      <div className="spacer20"></div>
      <div className="defaultColumnContainer row-on-resize fullOnResize fullHeight">
        <div className="socials flexible center">
          <a href="https://www.facebook.com/tailoriapp" target="_blank" rel="noopener noreferrer" ></a>
          <FacebookFilled />
        </div>
        <div className="socials flexible center">
          <a href="https://www.instagram.com/tailoriapp" target="_blank" rel="noopener noreferrer" ></a>
          <InstagramFilled />
        </div>
        <div className="socials flexible center">
          <a href="https://twitter.com/TailoriApp" target="_blank" rel="noopener noreferrer" ></a>
          <TwitterCircleFilled />
        </div>
        <div className="socials flexible center">
          <a href="mailto:support@tailorii.app" target="_blank" rel="noopener noreferrer" ></a>
          <MailFilled />
        </div>
      </div>
      <div className="spacer center">
        <img className="footer-image" src="https://res.cloudinary.com/lgxy/image/upload/w_800,q_100,c_scale/v1606368574/tailori/iconion-flat.png" alt="icon"/>
      </div>
    </footer>
  );
}

export default Footer;
