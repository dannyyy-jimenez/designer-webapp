import React from 'react';
import { NavLink } from 'react-router-dom'
import {
  isIOS,
  isAndroid,
  CustomView
} from "react-device-detect";
import { MenuOutlined, UpOutlined } from '@ant-design/icons';

// Style
import '../styles/Toolbar.css';

function Toolbar() {
  const [drawerOpen, setDrawerOpen] = React.useState(false);

  return (
    <>
      <CustomView condition={isIOS}>
        <div className="download-nav flexible defaultRowContainer">
          <h4 className="primary nunito">Download Tailori for iOS</h4>
          <div className="spacer"></div>
          <a className="get-nav-btn" href="https://apps.apple.com/us/app/id1520164347" alt="app store">GET</a>
        </div>
      </CustomView>
      <CustomView condition={isAndroid}>
        <div className="download-nav flexible defaultRowContainer">
          <h4 className="primary nunito">Download Tailori for android</h4>
          <div className="spacer"></div>
          <a className="get-nav-btn" href="https://play.google.com/store/apps/details?id=com.lgxy.tailori&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" alt="play store">GET</a>
        </div>
      </CustomView>
      <nav className="flexible defaultRowContainer">
        <div className="spacer10"></div>
        <img className="navbar-iconion" src="https://res.cloudinary.com/lgxy/image/upload/w_400,q_100/v1610409846/tailori/iconion-white.png" alt="iconion"/>
        <h1 className="nunito bold tertiary fullHeight center">Tailori</h1>
        <div className="spacer"></div>
        <div className="spacer resize-show"></div>
        <NavLink activeClassName="navbar-active" className="nunito navbar-tab fullHeight center tertiary" exact to="/">Home</NavLink>
        <NavLink activeClassName="navbar-active" className="nunito navbar-tab fullHeight center tertiary" to="/royalties">Start Selling</NavLink>
        <NavLink activeClassName="navbar-active" className="nunito navbar-tab fullHeight center tertiary" to="/explainer">Merch</NavLink>
        <NavLink activeClassName="navbar-active" className="nunito navbar-tab fullHeight center tertiary" exact to="/blogs">Blogs</NavLink>
        <NavLink activeClassName="navbar-active" className="nunito navbar-tab fullHeight center tertiary" to="/contact">Contact</NavLink>
        <MenuOutlined onClick={() => setDrawerOpen(!drawerOpen)} className="menu clickable resize-show" />
        <div className="spacer10"></div>
        <div className={"resize-show collapsible-menu" + (drawerOpen ? ' open' : '')}>
          <NavLink activeClassName="navbar-active" className="nunito navbar-tab keep mini fullHeight center tertiary" exact to="/">Home</NavLink>
          <NavLink activeClassName="navbar-active" className="nunito navbar-tab keep mini fullHeight center tertiary" to="/royalties">Start Selling</NavLink>
          <NavLink activeClassName="navbar-active" className="nunito navbar-tab keep mini fullHeight center tertiary" to="/explainer">Merch</NavLink>
          <NavLink activeClassName="navbar-active" className="nunito navbar-tab keep mini fullHeight center tertiary" to="/shop/shirtT">Shop</NavLink>
          <NavLink activeClassName="navbar-active" className="nunito navbar-tab keep mini fullHeight center tertiary" exact to="/blogs">Blogs</NavLink>
          <NavLink activeClassName="navbar-active" className="nunito navbar-tab keep mini fullHeight center tertiary" to="/contact">Contact</NavLink>
          <div className="close-menu fullWidth flexible navbar-tab keep mini center">
            <UpOutlined onClick={() => setDrawerOpen(false)} className="menu clickable resize-show" />
          </div>
        </div>
      </nav>
    </>
  );
}

export default Toolbar;
