import React from 'react';

// Styles
import '../styles/Landing.css';

function LandingShop() {
  return (
    <section className="defaultSection fullWidth scrollable-resize tertiaryFill defaultRowContainer reversable-resize">
      <div className="spacer defaultRowContainer center imagesContainer">
        <img src="https://res.cloudinary.com/lgxy/image/upload/tailori/portfolios/5f187457f9f9020017d3961b/659cab8907e331cdDXPDfab73c319b4c04f9DXDMGRW
" alt="tailori snapshot"/>
        <img src="https://res.cloudinary.com/lgxy/image/upload/tailori/portfolios/5f67b7caab19260017151620/3d473b60d816bde5TRPD897c7441972fe649TRDM2PRTYNK" alt="tailori snapshot"/>
        <img src="https://res.cloudinary.com/lgxy/image/upload/tailori/portfolios/5f6123b73d088e00170f9357/4b1917e94bf89467TRPD624ab17e96bae997TRDM8LRS" alt="tailori snapshot"/>
      </div>
      <div className="defaultColumnContainer fullHeight landingText">
        <div className="spacer20"></div>
        <div className="spacer10"></div>
        <h1 className="primary landingHeader">Discover Designers</h1>
        <br/>
        <h2 className="secondary light landingDescription">Everyday Apparel Made by Designers Like You</h2>
      </div>
      <div className="spacer10"></div>
    </section>
  );
}

export default LandingShop;
