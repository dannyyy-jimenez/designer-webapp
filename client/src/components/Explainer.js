import React from 'react';
import { Link } from 'react-router-dom'
import { NavLink } from 'react-router-dom'

// Styles
import '../styles/Landing.css';

function Explainer() {
  return (
    <>
      <section id="explainer" className="defaultSection scrollable fullWidth defaultColumnContainer">
        <div className="spacer10"></div>
        <div className="spacer10"></div>
        <h1 className="center primary header nunito">Merch Designed by Your Favorite Creators</h1>
        <h4 className="halfWidth secondary textCenter para">
          Purchase merchandise from upcoming designers, your favorite restaurants, schools, and many more designers. Simply find the designs you love the most and we'll take care of the rest.
        </h4>
        <div className="spacer10"></div>
        <div className="spacer defaultRowContainer center">
          <div className="templateContainer center">
            <img src="https://res.cloudinary.com/lgxy/image/upload/c_scale,h_300,w_300/tailori/portfolios/5f597ac1a308bb0017b95d04/5f428fe640d64b05TRPD2c715704ec35cc97TRDM4PHNTMFLSMRC" alt="template 1"/>
          </div>
          <div className="templateContainer center">
            <img src="https://res.cloudinary.com/lgxy/image/upload/c_scale,h_300,w_300/tailori/templates/static/template-webapp_h623aS.png" alt="template 2"/>
          </div>
          <div className="templateContainer center">
            <img src="https://res.cloudinary.com/lgxy/image/upload/c_scale,h_300,w_300/tailori/portfolios/5f8e887248bbce0017f90ca4/136785030f1356aaTRPDcd9a7336619386e7TRDM7MTHRN" alt="template 3"/>
          </div>
        </div>
        <div className="spacer10"></div>
      </section>
      <section id="tailored" className="defaultBanner padded-resize scrollable defaultColumnContainer primaryFill">
        <div className="spacer"></div>
        <h1 className="center tertiary nunito bold header"><span className="mojis">✨</span>Tailored to Your Style&nbsp;<span className="mojis">✨</span></h1>
        <h4 className="spacer center tertiary halfWidth textCenter para">
          <span>With a variety of styles of clothing and a plethora of colors, you will find a fit that matches your style!</span>
        </h4>
        <div className="videoContainer">
          <img src="https://res.cloudinary.com/lgxy/image/upload/v1610486478/tailori/colors-snap.png" alt="tailori snapshot"/>
        </div>
        <h4 className="center tertiary"><NavLink to="/shop" className="tertiary">Check Out What's in Store for You!</NavLink></h4>
        <div className="spacer"></div>
      </section>
    </>
  );
}

export default Explainer;
