import React from 'react';
import { NavLink } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEyeSlash, faLightbulb, faHeart, faParachuteBox } from '@fortawesome/free-solid-svg-icons';
import { faYoutube, faInstagram, faSnapchatGhost, faFacebookF } from '@fortawesome/free-brands-svg-icons';
import Loader from 'react-loader-spinner'
import {Helmet} from "react-helmet";

import API from '../Api';
import Merch from '../models/Merch';

// Styles
import '../styles/Store.css';

const formats = require('./Formats').Formats;

function BrandProfile({match, history}) {
  const [isLoading, setIsLoading] = React.useState(true);
  const [noBrand, setNoBrand] = React.useState(false);
  const [name, setName] = React.useState("");
  const [description, setDescription] = React.useState("");
  const [logo, setLogo] = React.useState("");
  const [ideas, setIdeas] = React.useState(0);
  const [hearts, setHearts] = React.useState(0);
  const [deliveries, setDeliveries] = React.useState(0);
  const [yt, setYT] = React.useState(false);
  const [fb, setFB] = React.useState(false);
  const [ig, setIG] = React.useState(false);
  const [sc, setSC] = React.useState(false);
  const [merch, setMerch] = React.useState([]);

  const load = async () => {
    try {
      const res =  await API.get('store/brand', {identifier: match.params.identifier});
      if (res.isError) throw 'error';

      setName(res.data._name);
      setLogo(res.data._logo);
      setDescription(res.data._des);

      setHearts(res.data._h);
      setIdeas(res.data._i);
      setDeliveries(res.data._d);

      setYT(res.data._sm._yt);
      setFB(res.data._sm._fb);
      setIG(res.data._sm._ig);
      setSC(res.data._sm._sc);

      setMerch(res.data._m.map(release => new Merch(release.design, release.designer, release.identifier, release.price, release.remaining, release.customizables, release.actions)));
      setIsLoading(false);
    } catch (e) {
      setNoBrand(true);
      setIsLoading(false);
    };
  };

  React.useEffect(() => {
    load();
  }, [match.params.identifier])

  return (
    <>
      <Helmet defaultTitle="Tailori" defer={false}>
          <title>{name}</title>
          <meta name="title" content={name} />
          <meta property="og:title" content={name} />
          <meta property="twitter:title" content={name} />
      </Helmet>
      <section className="defaultSection fullWidth scrollable brand-profile-container tertiaryFill defaultColumnContainer">
        {
          isLoading &&
          <div className="absoluteSection tertiaryFill flexible center">
            <Loader type="ThreeDots" color="#DB113B" height={30} width={30} />
          </div>
        }
        {
          noBrand &&
          <div className="absoluteSection tertiaryFill defaultColumnContainer flexible center">
            <FontAwesomeIcon className="inex primary" icon={faEyeSlash} />
            <br/>
            <h3 className="secondary">What You are Looking for Does Not Exist</h3>
          </div>
        }
        <div className="defaultRowContainer dabbedWidth profile-header">
          <img src={formats.cloudinarize(logo)} className="brand-logo" alt="brand logo"/>
          <div className="spacer"></div>
          <div className="defaultColumnContainer center">
            <FontAwesomeIcon className="stat-icon primary" icon={faLightbulb} />
            <p className="secondary  bold">{ideas}</p>
          </div>
          <div className="spacer20"></div>
          <div className="defaultColumnContainer center">
            <FontAwesomeIcon className="stat-icon primary" icon={faHeart} />
            <p className="secondary  bold">{hearts}</p>
          </div>
          <div className="spacer20"></div>
          <div className="defaultColumnContainer center">
            <FontAwesomeIcon className="stat-icon primary" icon={faParachuteBox} />
            <p className="secondary  bold">{deliveries}</p>
          </div>
          <div className="spacer"></div>
        </div>
        <h4 className="secondary bold dabbedWidth name">{name}</h4>
        <p className="secondary dabbedWidth des">{description}</p>
        <div className="defaultRowContainer socials-brand dabbedWidth space-evenly">
          {
            yt &&
            <a href={"https://www.youtube.com/"+yt} rel="noopener noreferrer" target="_blank">
              <FontAwesomeIcon className="social-icon primary" icon={faYoutube} />
            </a>
          }
          {
            ig &&
            <a href={"https://www.instagram.com/"+ig} rel="noopener noreferrer" target="_blank">
              <FontAwesomeIcon className="social-icon primary" icon={faInstagram} />
            </a>
          }
          {
            fb &&
            <a href={"https://www.facebook.com/"+fb} rel="noopener noreferrer" target="_blank">
              <FontAwesomeIcon className="social-icon primary" icon={faFacebookF} />
            </a>
          }
          {
            sc &&
            <a href={"https://www.snapchat.com/add/"+sc} rel="noopener noreferrer" target="_blank">
              <FontAwesomeIcon className="social-icon primary" icon={faSnapchatGhost} />
            </a>
          }
        </div>
        <div className="defaultRowContainer merch-container dabbedWidth">
          {
            merch.map(release => {
              return (
                <div key={release.getIdentifier()} className="card small defaultColumnContainer">
                  <img src={formats.cloudinarize(release.getCover(), '', true)} alt="release" className="card-image"/>
                  <p className="primaryFill tertiary priceable bold tiny">${release.getPrice()}</p>
                  <div className="content-container">
                    <p className="nunito secondary  title cutoff">{release.getName()}</p>
                  </div>
                  <NavLink className="card-director" exact to={"/shop/zoom/"+release.identifier}></NavLink>
                </div>
              )
            })
          }
        </div>
      </section>
    </>
  );
}

export default BrandProfile;
