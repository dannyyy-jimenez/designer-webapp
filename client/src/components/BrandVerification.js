import React from 'react';
import qs from 'qs';

import config from '../Constants';

function BrandVerification(props) {
  const [phoneNumber, setPhoneNumber] = React.useState('');
  const [connectToken, setConnectToken] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);

  const formatPhoneNumber = (phoneNumberString) => {
    var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
    var match = cleaned.match(/^(\d{3})(\d{1,3})?(\d{1,4})?$/)
    if (match) {
      return '(' + match[1] + (match[1] && match[2] ? ') ' : '') + (match[2] || '') + (match[2] && match[3] ? '-' : '') + (match[3] || '')
    }
    return phoneNumberString;
  };

  const onPhoneChange = (e) => {
    setPhoneNumber(formatPhoneNumber(e.target.value));
  };

  React.useEffect(() => {
    const token = qs.parse(props.location.search, { ignoreQueryPrefix: true }).code;
    if (!token) return setIsLoading(true);
    setConnectToken(token);
  }, [props.location]);

  const onVerify = () => {
    setIsLoading(true);
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ phoneNumber, connectToken })
    };
    fetch(config.API_ENDPOINT + 'api/brand/connect', requestOptions)
        .then(response => response.json())
        .then(data => {
          setIsLoading(false);
          setSuccess(true);
        }).catch((e) => {

        });
  }

  return (
    <section id="verify-brand" className="defaultBanner defaultColumnContainer primaryFill">
      <div className="spacer"></div>
      {
        !success &&
        <>
          <h1 className="center tertiary nunito header">One More Step!</h1>
          <div className="spacer10"></div>
          <h4 className="spacer center light tertiary halfWidth textCenter para">
            <span>Enter the phone number linked to your Tailori account to verify your brand</span>
          </h4>
          <input type="tel" disabled={isLoading} onChange={onPhoneChange} value={phoneNumber} autoComplete="tel" placeholder="Enter phone number..." className="defaultInput tertiary textCenter nunito" />
          {
            phoneNumber.length === 14 && !isLoading &&
            <button onClick={onVerify} className="defaultButton tertiary primaryFill textCenter">Verify</button>
          }
        </>
      }
      {
        success &&
        <>
          <h1 className="center tertiary nunito header">You have a Brand!</h1>
          <div className="spacer10"></div>
          <h4 className="spacer center light tertiary halfWidth textCenter para">
            <span>You have finished setting up your account. Now, simply go back to the app and tap 'I did it!' to load your brand! Enjoy!</span>
          </h4>
        </>
      }
      <div className="spacer"></div>
    </section>
  );
}

export default BrandVerification;
