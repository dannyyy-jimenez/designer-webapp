import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStore, faChevronRight } from '@fortawesome/free-solid-svg-icons'
import { NavLink } from 'react-router-dom'
import {
  isIOS,
  isAndroid,
  CustomView
} from "react-device-detect";
import { Rate } from 'antd';

function Download() {
  return (
    <>
      <section id="download-banner" className="defaultBanner center primaryFill defaultColumnContainer">
        <div className="spacer"></div>
        <h1 className="center tertiary bold header nunito">Download Now!</h1>
        <div className="spacer10"></div>
        <Rate disabled defaultValue={5} />
        <br/>
        <div className="spacer center">
          <CustomView condition={isIOS}>
            <a className="download-icon" href='https://apps.apple.com/us/app/id1520164347'><img className="download-icon" alt="Get it on the Apple App Store" src="https://res.cloudinary.com/lgxy/image/upload/w_200/tailori/app-store-badge.svg" alt="app store badge"/></a>
          </CustomView>
          <CustomView condition={isAndroid}>
            <a className="download-icon" href='https://play.google.com/store/apps/details?id=com.lgxy.tailori&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/></a>
          </CustomView>
          <CustomView condition={!isAndroid && !isIOS}>
            <div className="fullWidth center flexible">
              <a className="download-icon" href='https://apps.apple.com/us/app/id1520164347'><img className="download-icon" alt="Get it on the Apple App Store" src="https://res.cloudinary.com/lgxy/image/upload/w_200/tailori/app-store-badge.svg" alt="app store badge"/></a>
              <div className="spacer10"></div>
              <a className="download-icon" href='https://play.google.com/store/apps/details?id=com.lgxy.tailori&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/></a>
            </div>
          </CustomView>
        </div>
      </section>
    </>
  );
}

export default Download;
