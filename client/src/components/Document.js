import React from 'react';
import qs from 'qs';

// Styles

function Document(props) {
  const [isValidDocument, setIsValidDocument] = React.useState(false);
  const [documentData, setDocumentData] = React.useState({});
  const validDocuments = ['pricing', 'guidelines', 'royalties-explanation', 'privacy', 'terms', 'attributions', 'dmca', 'post-release', 'merchandise'];
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    setDocumentData({});
    setIsLoading(true);

    const name = qs.parse(props.location.search, { ignoreQueryPrefix: true }).name;
    if (!name) {
      setIsValidDocument(false);
      setIsLoading(false);
      return;
    }
    if (!validDocuments.includes(name) && !name.includes('update')) return setIsValidDocument(false);

    fetch(`https://res.cloudinary.com/lgxy/raw/upload/tailori/documents/${name}.min.json`, {headers: {'Cache-Control': 'no-cache'}}).then(response => response.json()).then(data => {
      setDocumentData(data);
      setIsValidDocument(true);
      setIsLoading(false);
    }).catch((e) => {
      console.log(e);
      setIsLoading(false);
    });
  }, [props.location]);

  return (
    <section className="defaultSection fullWidth scrollable defaultColumnContainer">
      {
        !isValidDocument && !isLoading &&
        <>
          <div className="spacer"></div>
          <h1 className="center primary">Document not found!</h1>
          <div className="spacer"></div>
        </>
      }
      {
        isValidDocument && documentData.title &&
        <>
          <div className="spacer"></div>
          <br/>
          <h1 className="center primary header nunito">{documentData.title}</h1>
          <br/>
          <br/>
          <div className="halfWidth">
            {
              documentData.content.map((text, index) => {
                if (text.type === 'uri') {
                  return (
                    <>
                      <h4 key={"CONTENT_"+index} className="fullWidth para">
                        <a target="_blank" rel="noopener noreferrer" href={text.external} className="primary">
                          {text.content}
                        </a>
                      </h4>
                      <br key={"BREAK_"+index} />
                    </>
                  )
                }

                if (text.type === 'subheader') {
                  return (
                    <>
                      <h2 key={"CONTENT_"+index} className="fullWidth primary para subheader">
                        {text.content}
                      </h2>
                      <br key={"BREAK_"+index} />
                    </>
                  )
                }

                if (text.type === 'base-inset') {
                  return (
                    <>
                      <h4 key={"CONTENT_"+index} className="para insetWidth">
                        {text.content}
                      </h4>
                      <br key={"BREAK_"+index} />
                    </>
                  )
                }

                return (
                  <>
                    <h4 key={"CONTENT_"+index} className="para">
                      {text.content}
                    </h4>
                    <br key={"BREAK_"+index} />
                  </>
                )
              })
            }
          </div>
          <br/>
          <h4 className="center halfWidth textCenter para">
            <span>
              <span>{documentData.footer[0]} </span>
              <a href={"mailto:"+documentData.footer[1]} className='primary'>{documentData.footer[1]}</a>
            </span>
          </h4>
          <div className="spacer"></div>
        </>
      }
    </section>
  );
}

export default Document;
