import React from 'react';

// Styles
import '../styles/Landing.css';
import API from '../Api';
import { NavLink } from 'react-router-dom'

const formats = require('./Formats').Formats;

function Bag() {
  const [cart, setCart] = React.useState([]);
  const [isLoading, setIsLoading] =  React.useState(true);
  const [total, setTotal] = React.useState('');

  const updateCart = (raw) => {
    let stringified = raw.map(item => {
      return `${item.release.identifier}_#${item.quantity}_@${item.size}_&${item.color}`
    }).join("$");

    localStorage.setItem("_CRT_TLR", stringified);
  }

  const onRemove = (index) => {
    let copy = cart.slice();
    copy.splice(index, 1);
    let newto = copy.reduce((total, next) => total + parseFloat(next.total), 0);
    updateCart(copy);
    setCart(copy);
    setTotal(newto.toFixed(2));
  }

  const load = async (raw) => {
    try {
      const res =  await API.get('store/bag', {bag: raw});
      if (res.isError) throw 'error';

      setCart(res.data._b);
      updateCart(res.data._b);
      setTotal(res.data._t);
      setIsLoading(false);
    } catch (e) {
      console.log(e);
      setCart([]);
      //localStorage.removeItem("_CRT_TLR");
      setIsLoading(false);
    };
  }

  React.useEffect(() => {
    if (!localStorage.getItem("_CRT_TLR")) {
      setIsLoading(false);
      return;
    };
    load(localStorage.getItem("_CRT_TLR"));
  }, []);

  return (
    <section className="defaultSection scrollable dabbedWidth">
      <br/>
      {
        cart.length > 0 &&
        <h1 className="secondary">Review your bag - <span className="primary">${total}</span></h1>
      }
      {
        cart.length === 0 &&
        <h1 className="secondary">Review your bag</h1>
      }
      <br/>
      <br/>
      {
        cart.length === 0 && !isLoading &&
        <p className="secondary">Your bag is empty</p>
      }
      {
        cart.map((item, index) => {
          return (
            <div className="defaultRowContainer fullWidth bag-container">
              <img className="bag-cover" src={formats.cloudinarize(item.release.design.shots[0])} alt="bag"/>
              <div className="spacer5"></div>
              <div className="defaultColumnContainer">
                <div className="spacer20"></div>
                <h2 className="secondary">{item.release.design.name}</h2>
                <p className="secondary bagatt title">Color: {formats.getColor(item.release.design.type, item.color, false, true)}</p>
                <p className="secondary bagatt title">Size: {formats.formatSize(item.size)}</p>
                <p className="secondary bagatt title">Quantity: {item.quantity}</p>
                <p className="secondary bagatt opaque tiny">#{item.release.identifier}</p>
              </div>
              <div className="spacer"></div>
              <div className="defaultColumnContainer center">
                <div className="spacer"></div>
                <h3 className="primary">${item.total}</h3>
                <div className="spacer"></div>
                <p className="red clickable tiny" onClick={() => onRemove(index)}>Remove</p>
                <div className="spacer20"></div>
              </div>
            </div>
          )
        })
      }
      <br/>
      <br/>
      {
        cart.length > 0 &&
        <div className="defaultRowContainer fullWidth">
          <NavLink className="defaultButton rounded tertiary primaryFill textCenter flexible center" to="/shop/checkout/pay">Pay</NavLink>
        </div>
      }
    </section>
  );
}

export default Bag;
