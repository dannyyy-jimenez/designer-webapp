const formats = require('../components/Formats').Formats;

export default class Design {

  constructor(id, type, shots, name, description, hasDropped, reactive, reactives = null) {
    this.id = id;
    this.shots = shots;
    this.name = name;
    this.description = description;
    this.type = type;
    this.reactive = reactive;
    this.reactives = reactives;
    this.hasDropped = hasDropped;
  }

  getID() {
    return this.id;
  }

  getName() {
    return this.name;
  }

  getDescription() {
    return this.description;
  }

  isMultiShot() {
    return this.shots.length > 1;
  }

  getShots() {
    return this.shots;
  }

  getCover() {
    return this.shots[0];
  }

  getNextShot(currentShot) {
    if (this.shots.indexOf(currentShot) === this.shots.length - 1) {
      return this.shots[0];
    }

    return this.shots[this.shots.indexOf(currentShot) + 1];
  }

  getType() {
    return formats.getType(this.type);
  }

  isReactive() {
    return this.reactive;
  }

  setHasDropped(hasDropped) {
    this.hasDropped = hasDropped;
  }
}
