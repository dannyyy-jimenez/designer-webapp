import React from 'react';
import qs from 'qs';
import config from './Constants';
import { withRouter, Switch, Route } from 'react-router-dom';
import {loadStripe} from '@stripe/stripe-js';
import {
  Elements
} from '@stripe/react-stripe-js';
import Constants from './Constants';
import {Helmet} from "react-helmet";
import ReactPixel from 'react-facebook-pixel';
import CookieConsent from "react-cookie-consent";
import { BackTop } from 'antd';
import { CaretUpFilled } from '@ant-design/icons';

// Styles
import './App.less';
import 'antd/dist/antd.less';

// Components
import Toolbar from './components/Toolbar';
import Landing from './components/Landing';
import Explainer from './components/Explainer';
import Royalties from './components/Royalties';
import Download from './components/Download';
import Document from './components/Document';
import Footer from './components/Footer';
import BrandVerification from './components/BrandVerification';
import BrandSuccess from './components/BrandSuccess';

import Blogs from './components/Blogs';
import Blog from './components/Blog';

import ToolbarShop from './components/ToolbarShop';
import LandingShop from './components/LandingShop';
import Catalog from './components/Catalog';
import Zoom from './components/Zoom';
import BrandProfile from './components/BrandProfile';
import Pay from './components/Pay';
import Bag from './components/Bag';

const stripePromise = loadStripe(Constants.STRIPE_KEY);

const options = {
  autoConfig: true, // set pixel's autoConfig
  debug: false, // enable logs
};

ReactPixel.init('4374468709294960', {}, options);
ReactPixel.revokeConsent();
ReactPixel.pageView();

class App extends React.Component {
  state = {
    showBrandVerification: false,
    showBrandRefresh: false,
    showBrandSuccess: false,
    connectQuery: '',
    isDocument: false
  };

  constructor(props) {
    super(props);
    this.state = {
      showBrandVerification: false,
      showBrandRefresh: false,
      showBrandSuccess: false,
      connectQuery: '',
      isDocument: false,
      location: props.location
    };
    props.history.listen((data) => {
      setTimeout(() => {
        this.onRouteChange(data, props.history);
      }, 500);
    });
    if (!window.location.hostname.includes('tailorii.app') && config.ENVIRONMENT === 'prod') {
      window.location = "https://www.tailorii.app";
    }
  }

  componentDidMount() {
    if (this.props.history.location.pathname !== '') {
      setTimeout(() => {
        this.onRouteChange(this.props.history.location, this.props.history);
      }, 300);
    }
  }

  onRouteChange(data, history) {
    if (history.action === "REPLACE") return;
    let element;

    switch(data.pathname) {
      case '/':
        window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
        this.setState({
          isDocument: false,
        });
        break;
      case '/download':
        element = document.getElementById('download-banner');
        this.setState({
          isDocument: false,
        });
        break;
      case '/explainer':
        element = document.getElementById('explainer');
        this.setState({
          isDocument: false,
        });
        break;
      case '/brand/verify':
        this.setState({
          showBrandVerification: true,
          connectQuery: this.props.location
        }, () => {
          element = document.getElementById('verify-brand');
        })
        this.setState({
          isDocument: false,
        });
        break;
      case '/brand/success':
        this.setState({
          showBrandSuccess: true,
          showBrandRefresh: false
        }, () => {
          element = document.getElementById('success-refresh-brand');
        })
        this.setState({
          isDocument: false,
        });
        break;
      case '/brand/refresh':
        this.setState({
          showBrandRefresh: true,
          showBrandSuccess: false
        }, () => {
          element = document.getElementById('success-refresh-brand');
        })
        this.setState({
          isDocument: false,
        });
        break;
      case '/royalties':
        element = document.getElementById('royalties');
        this.setState({
          isDocument: false,
        });
        break;
      case '/contact':
        element = document.getElementById('contact');
        this.setState({
          isDocument: false,
        });
        break;
      case '/download':
        element = document.getElementById('download');
        this.setState({
          isDocument: false,
        });
        break;
      case '/document':
        this.setState({
          location: data,
          isDocument: true
        });
        window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
        break;
      case '/reroute':
        window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
        this.reroute(data);
        this.setState({
          isDocument: false,
        });
      break;
      default:
        this.setState({
          location: data,
          isDocument: false,
        });
        break;
    }

    if (!element) return;
    element.scrollIntoView({behavior: 'smooth'});
  }

  reroute(data) {
    const regProd = new RegExp('^tailori://.*', 'gi');
    const regExpo = new RegExp('^exp://');
    const scheme = qs.parse(data.search, { ignoreQueryPrefix: true }).scheme;
    if (!scheme || (!regProd.test(scheme) && regExpo.test(scheme))) return;
    window.location = scheme;
    setTimeout(function () { window.location = scheme; }, 2000);
  }

  render() {
    return (
      <React.Fragment>
        <Helmet defer={false}>
            <title>Tailori</title>
            <meta name="title" content="Tailori - Kickstart Your Brand 🚀" />
            <meta property="og:title" content="Tailori - Kickstart Your Brand 🚀" />
            <meta property="twitter:title" content="Tailori - Kickstart Your Brand 🚀" />
        </Helmet>
        <Switch>
          <Route path="/document">
            <Toolbar formalize={this.state.isDocument} />
            <Document location={this.state.location} />
          </Route>

          <Route path="/shop">
            <ToolbarShop location={this.state.location} />

            <Route exact path="/shop">
              <LandingShop />
            </Route>

            <Route exact path="/shop/checkout/pay">
              <Elements stripe={stripePromise}>
                <Pay />
              </Elements>
            </Route>

            <Route exact path="/shop/checkout/bag">
              <Bag />
            </Route>

            <Route path="/shop/brand/:identifier" component={BrandProfile}>
            </Route>

            <Route path="/shop/zoom/:identifier" component={Zoom}>
            </Route>

            <Route exact path="/shop/:type" component={Catalog}>
            </Route>
          </Route>

          <Route path="/blogs" exact>
            <Toolbar formalize={false} />
            <Blogs />
          </Route>

          <Route path="/blogs/:id" >
            <Toolbar formalize={false} />
            <Blog />
            <BackTop visibilityHeight={70}>
              <div className="lgxy-bckttp flexible center tertiary">
                <CaretUpFilled />
              </div>
            </BackTop>
          </Route>

          <Route path={['/', '/explainer', '/royalties', '/download', '/brand/verify', '/contact']}>
            <Toolbar formalize={this.state.isDocument} />
            <Landing />
            <Royalties />
            {
              this.state.showBrandVerification &&
              <BrandVerification location={this.state.connectQuery} />
            }
            {
              this.state.showBrandSuccess &&
              <BrandSuccess />
            }
            {
              this.state.showBrandRefresh &&
              <BrandSuccess refresh={true} />
            }
            <Explainer />
            <Download />
          </Route>
        </Switch>
        <Footer />
        <CookieConsent buttonText="Sounds Good!" style={{ background: "#DB113B" }} enableDeclineButton declineButtonText="No Cookies for Me!" declineButtonStyle={{background: "#DB113B", color: "#FFF", opacity: 0.8 }} onAccept={ReactPixel.grantConsent} buttonStyle={{ color: "#DB113B", background: "#FFF", padding: "8px", outline: "none", borderRadius: "4px", fontSize: "13px" }} flipButtons={true}>This website uses cookies to enhance the user experience!</CookieConsent>
      </React.Fragment>
    )
  }
}

export default withRouter(App);
