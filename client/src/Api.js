import axios from 'axios';
import qs from 'qs';
import Constants from './Constants';

const headers = {
  'content-type': 'application/x-www-form-urlencoded',
  'X-Requested-With': 'XMLHttpRequest'
};

const client = axios.create({
  baseURL: Constants.API_ENDPOINT + "api/public/"
});

const onSuccess = (res) => {
  if (res.data._hE) {
    return {
      isError: true,
      responseCode: res.status,
      response: res.data._e,
      data: res.data._body,
      date: new Date().getTime()
    }
  }

  return {
    isError: false,
    responseCode: res.status,
    response: 'success',
    data: res.data._body,
    date: new Date().getTime()
  }
};

const onError = (error) => {
  if (!error.response) {
    error.response = {
      status: 500
    };
  }
  return {
    isError: true,
    responseCode: error.response.status,
    response: error.response.data ? error.response.data._e : 'error',
    data: null,
    date: new Date().getTime()
  }
};

export default {
  get: async (query, data) => {
    try {
      const res = await client.get(query, {
        params: {...data, key: Constants.KEY},
        headers: headers
      });
      return onSuccess(res);
    } catch (error) {
      console.log(error);
      return onError(error);
    }
  },
  post: async (uri, data) => {
    try {
      const res = await client({
        method: 'post',
        headers: headers,
        responseType: 'json',
        url: uri,
        data: qs.stringify({...data, key: Constants.KEY})
      });
      return onSuccess(res);
    } catch (error) {
      return onError(error);
    }
  },
  address: async (query) => {
    try {
      const res = await axios.get(`https://geocoder.ls.hereapi.com/6.2/geocode.json?apiKey=kQiJ84IqU8PK3Fbj97aUQtT3_UF_2MGTI3zzigFX8ZM&searchtext=${query.replace(/ /g, "+")}`);
      return [res.data.Response.View[0].Result[0].Location.Address, [res.data.Response.View[0].Result[0].Location.DisplayPosition.Longitude, res.data.Response.View[0].Result[0].Location.DisplayPosition.Latitude]];
    } catch (error) {
      return {
        error: error,
        isError: true
      }
    }
  }
}
