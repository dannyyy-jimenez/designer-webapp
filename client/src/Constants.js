// Constants.js

const prod = {
  API_ENDPOINT: 'https://www.tailorii.app/',
  APP_ENDPOINT: 'tailori://',
  ENVIRONMENT: 'prod',
  STRIPE_KEY: 'pk_live_8Er8mHNsXc7fXfzp1uobJw0q002IqK04Qx',
  KEY: 'pk_5GuFfyAL0ZYD1Tqjkm8pcmHBCMZdC8hH'
};

const dev = {
  API_ENDPOINT: 'http://10.1.10.102:80/',
  APP_ENDPOINT: 'expo://',
  ENVIRONMENT: 'dev',
  STRIPE_KEY: 'pk_test_pHZsXAaoeTMMuc0l0kPZ5Hxh00IiHmUG8H',
  KEY: 'pdk_BBfHZxcHCD63ShXul7YrcrfuCwBAjGbq'
};

const config = process.env.NODE_ENV === 'development' ? dev : prod;
export default config;
