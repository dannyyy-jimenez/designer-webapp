const crypto = require('crypto');

function GenRan() {
  return (Math.random() * (9 - 1) + 1).toFixed(0);
}

function CreateIdentifier(name) {
  let beginning = GenRan();
  let ending = GenRan();
  const consonants = name.replace(/[^a-zA-Z0-9 ]/g, "").replace(/[aeiou ]/gi, '').split('').filter(function(item, pos, self) {
    return self.indexOf(item) == pos;
  }).join('').toUpperCase();

  return beginning + consonants;
}

let name = process.argv.slice(2).join(' ');

let id = `${crypto.randomBytes(8).toString("hex")}TRPD${crypto.randomBytes(8).toString("hex")}TRDN${CreateIdentifier(name, false)}`

console.log(id);
