const express = require ('express');
const router = express.Router();
const mongoose = require('mongoose');
const path = require('path');
const jwt = require('jsonwebtoken');
const User = require('../controllers/user');
const Guest = require('../controllers/guest');
const Admin = require('../controllers/admin');
const Liana = require('forest-express-mongoose');
const fs = require('fs');

module.exports = router;

// <editor-fold> FOREST ADMIN
router.post('/actions/lgxy/set-password', Liana.ensureAuthenticated, (req, res) => {
  Admin.forest.setPassword(req.body).then(() => {
    res.send({ success: 'Admin password updated!' });
  }).catch((e) => {
      console.log(e);
     res.status(400).send({ error: 'Error updating admin password!' });
  });
});

router.post('/actions/lgxy/payouts-review', Liana.ensureAuthenticated, (req, res) => {
  Admin.forest.payoutsReview(req.body).then(() => {
    res.send({ success: 'Admin Stripe Link Sent!' });
  }).catch((e) => {
      console.log(e);
     res.status(400).send({ error: 'Error sending!' });
  });
});

router.post('/actions/lgxy/report-tax', Liana.ensureAuthenticated, (req, res) => {
  Admin.forest.reportTax(req.body).then(() => {
    res.send({ success: 'Tax Reported!' });
  }).catch((e) => {
      console.log(e);
     res.status(400).send({ error: 'Error reporting!' });
  });
});

router.post('/actions/lgxy/remind-validate', Liana.ensureAuthenticated, (req, res) => {
  Admin.forest.remindValidate(req.body).then(() => {
    res.send({ success: 'Admin Reminder Sent!' });
  }).catch((e) => {
      console.log(e);
     res.status(400).send({ error: 'Error reminding!' });
  });
});

router.post('/actions/lgxy/notify', Liana.ensureAuthenticated, (req, res) => {
  Admin.forest.notify(req.body).then(() => {
    res.send({ success: 'Notification sent out!' });
  }).catch((e) => {
      console.log(e);
     res.status(400).send({ error: 'Error sending notification!' });
  });
});


router.post('/actions/lgxy/redo-design', Liana.ensureAuthenticated, (req, res) => {
  Admin.forest.redoDesign(req.body).then(() => {
    res.send({ success: 'Design Updated!' });
  }).catch((e) => {
      console.log(e);
     res.status(400).send({ error: 'Error updating design!' });
  });
});

router.post('/actions/lgxy/fix-white', Liana.ensureAuthenticated, (req, res) => {
  Admin.forest.fixWhite(req.body).then(() => {
    res.send({ success: 'White Fixed!' });
  }).catch((e) => {
      console.log(e);
     res.status(400).send({ error: 'Error fixing!' });
  });
});

router.post('/actions/lgxy/brand-activation', Liana.ensureAuthenticated, (req, res) => {
  Admin.forest.resendBrandActivation(req.body).then(() => {
    res.send({ success: 'Message Sent!' });
  }).catch((e) => {
      console.log(e);
     res.status(400).send({ error: 'Error sending message!' });
  });
});

router.post('/actions/lgxy/brand-deletion', Liana.ensureAuthenticated, (req, res) => {
  Admin.forest.deleteBrand(req.body).then(() => {
    res.send({ success: 'Brand Deleted! Remember to delete user and cloudinary folder is needed' });
  }).catch((e) => {
     res.status(400).send({ error: 'Error deleting' });
  });
});

router.post('/actions/lgxy/user-deletion', Liana.ensureAuthenticated, (req, res) => {
  Admin.forest.deleteUser(req.body).then(() => {
    res.send({ success: 'User Deleted! Delete Cloudinary Folder!' });
  }).catch((e) => {
     res.status(400).send({ error: 'Error deleting' });
  });
});

router.post('/actions/lgxy/model-stripe', Liana.ensureAuthenticated, (req, res) => {
  Admin.forest.modelLogin(req.body).then(() => {
    res.send({ success: 'Design Updated!' });
  }).catch((e) => {
      console.log(e);
     res.status(400).send({ error: 'Error updating design!' });
  });
});
// </editor-fold>

// <editor-fold> WEBHOOKS
router.route('/hooks/easypost').post((req, res) => {
  Admin.orders.easypostUpdate(req.body).then(response => {
    return Respond(res, { ...response });
  });
});
// </editor-fold>

// <editor-fold> ADMIN
//<editor-fold> POST
router.route('/lgxy/login').post((req, res) => {
  if (!(req.body.phoneNumber) || !(req.body.password)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  Admin.login(req.body).then(response => {
    return Respond(res, { ...response });
  });
});

router.route('/lgxy/settings/notifications').post((req, res) => {
  if (!(req.body.auth) || !(req.body.token)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.updateSockets(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/settings/notifications/remove').post((req, res) => {
  if (!(req.body.auth) || !(req.body.token)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.removeSocket(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/user/delete').post((req, res) => {
  if (!(req.body.auth) || !(req.body.id)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.deleteUser(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/order/payout').post((req, res) => {
  if (!(req.body.auth) || !(req.body.identifier) || !(req.body.amount) || !(req.body.account)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.orders.payout(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/order/update').post((req, res) => {
  if (!(req.body.auth) || !(req.body.identifier) || !(req.body.messageType) || !(req.body.message)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.orders.update(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/release/approve').post((req, res) => {
  if (!(req.body.auth) || !(req.body.identifier) || (typeof req.body.decision == 'undefined')) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.releaseVerdict(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/release/validate').post((req, res) => {
  if (!(req.body.auth) || !(req.body.identifier) || (typeof req.body.price === "undefined") || (typeof req.body.decision == 'undefined')) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.releaseDesignVerdict(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});
//</editor-fold>
//<editor-fold> GET
router.route('/lgxy/dashboard').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.load.dashboard(decoded).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/orders').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.load.orders(decoded, req.query).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/order/payees').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error || !req.query.identifier) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.load.orderPayees(decoded, req.query).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/order/update').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error || !req.query.identifier) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.load.update(decoded, req.query).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/releases').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.load.releases(decoded, req.query).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/designs').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.load.designs(decoded, req.query).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/brands').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.load.brands(decoded, req.query).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/users').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.load.users(decoded, req.query).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/user').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error || !req.query.id) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.load.user(decoded, req.query).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/brand').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error || !req.query.identifier) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.load.brand(decoded, req.query).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/release').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error || !req.query.identifier) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.load.release(decoded, req.query).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/design').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error || !req.query.id) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.load.design(decoded, req.query).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/lgxy/order').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_ADMIN_SECRET, function(error, decoded) {
    if (error || !req.query.identifier) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    Admin.load.order(decoded, req.query).then(response => {
      return Respond(res, { ...response });
    });
  });
});

//</editor-fold>
// </editor-fold>

// <editor-fold> POST
router.route('/register').post((req, res) => {
  if (!(req.body.firstName) || !(req.body.lastName) || !(req.body.phoneNumber) || !(req.body.dob) || !(req.body.password)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  User.register(req.body).then(response => {
    return Respond(res, { ...response });
  });
});

router.route('/login').post((req, res) => {
  if (!(req.body.phoneNumber) || !(req.body.password)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  User.login(req.body).then(response => {
    return Respond(res, { ...response });
  });
});

router.route('/forgot').post((req, res) => {
  if (!(req.body.phoneNumber)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  User.forgot(req.body).then(response => {
    return Respond(res, { ...response });
  });
});

router.route('/settings/password').post((req, res) => {
  if (!(req.body.auth) || !(req.body.password)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.updatePassword(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/settings/permissions').post((req, res) => {
  if (!(req.body.auth) || typeof req.body.smsUpdates == 'undefined') {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.updatePermissions(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/settings/payment').post((req, res) => {
  if (!(req.body.auth) || !(req.body.token)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.updatePayment(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/settings/notifications').post((req, res) => {
  if (!(req.body.auth) || !(req.body.token)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.updateSockets(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/settings/notifications/remove').post((req, res) => {
  if (!(req.body.auth) || !(req.body.token)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.removeSocket(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/brand/starter').post((req, res) => {
  if (!(req.body.auth) || !(req.body.name) || !(req.body.description) || !(req.body.logo)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.brand.create(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/brand/connect').post((req, res) => {
  if (!(req.body.phoneNumber) || !(req.body.connectToken)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  User.brand.connect(req.body).then(response => {
    return Respond(res, { ...response });
  });
});

router.route('/brand/verify').post((req, res) => {
  if (!(req.body.auth)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.brand.verify(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/brand/edit/logo').post((req, res) => {
  if (!(req.body.auth) || !(req.body.logo)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.brand.logo(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/brand/edit').post((req, res) => {
  if (!(req.body.auth) || !(req.body.description) || (typeof req.body.yt == "undefined") || (typeof req.body.ig == "undefined") || (typeof req.body.fb == "undefined") || (typeof req.body.sc == "undefined")) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.brand.edit(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/brand/dashboard').post((req, res) => {
  if (!(req.body.auth)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.brand.dashboard(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/portfolio/design').post((req, res) => {
  if (!(req.body.auth) || !(req.body.name) || !(req.body.description) || !(req.body.base) || !(req.body.type) || !(req.body.mode) || !(req.body.metrics)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.portfolio.design(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/portfolio/release').post((req, res) => {
  if (!(req.body.auth) || !(req.body.design) || (typeof req.body.limited == "undefined")  || !(req.body.limit) || (typeof req.body.colorable == "undefined") || !(req.body.royalty)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.portfolio.release(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/portfolio/release/externals').post((req, res) => {
  if (!(req.body.auth) || !(req.body.design) || !(req.body.files)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.portfolio.externals(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/portfolio/withhold').post((req, res) => {
  if (!(req.body.auth) || !(req.body.design)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.portfolio.withhold(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/portfolio/pricing').post((req, res) => {
  if (!(req.body.auth) || !(req.body.design) || !(req.body.targetPrice)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.portfolio.pricing(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/portfolio/remove').post((req, res) => {
  if (!(req.body.auth) || !(req.body.design)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.portfolio.remove(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/store/favorite').post((req, res) => {
  if (!(req.body.auth) || !(req.body.designer)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.store.favorite(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/store/bag').post((req, res) => {
  if (!(req.body.auth) || !(req.body.release) || !(req.body.size) || !(req.body.color) || !(req.body.quantity)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.store.bag(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/store/unbag').post((req, res) => {
  if (!(req.body.auth) || !(req.body.item)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.store.unbag(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/store/shipping').post((req, res) => {
  if (!(req.body.auth) || !(req.body.coordinates) || !(req.body.streetAddress) || (typeof req.body.streetAddressCont == "undefined") || !(req.body.city) || !(req.body.state) || !(req.body.postalCode) || !(req.body.country)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.store.shipping(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/store/shipping/remove').post((req, res) => {
  if (!(req.body.auth)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.store.shippingRemove(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/store/model').post((req, res) => {
  if (!(req.body.auth) || !(req.body.modelCode)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.store.model(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/store/checkout').post((req, res) => {
  if (!(req.body.auth)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  jwt.verify(req.body.auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.store.checkout(req.body).then(response => {
      return Respond(res, { ...response });
    });
  });
});
// </editor-fold>
// <editor-fold> GET
router.route('/settings/account').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.load.account(decoded).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/settings/payment').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.load.payment(decoded).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/settings/shipping').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.load.shipping(decoded).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/notifications').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.load.notifications(decoded).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/brand').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.load.brand(decoded).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/sales/zoom').get((req, res) => {
  const auth = req.query.auth;
  const sale = req.query.sale;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error || !sale || sale == '') return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.load.saleZoom(decoded, sale).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/portfolio').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.load.portfolio(decoded).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/portfolio/zoom').get((req, res) => {
  const auth = req.query.auth;
  const design = req.query.design;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error || !design || design == '') return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.load.portfolioZoom(decoded, design).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/store').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.load.store(decoded, req.query).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/store/release').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error || !req.query.identifier) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.load.release(decoded, req.query.identifier).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/store/brand').get((req, res) => {
  const auth = req.query.auth;
  const designer = req.query.designer;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error || !designer) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.load.brandProfile(decoded, designer).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/store/zoom').get((req, res) => {
  const auth = req.query.auth;
  const release = req.query.release;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error || !release) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.load.zoom(decoded, release).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/store/zoom/color').get((req, res) => {
  const auth = req.query.auth;
  const release = req.query.release;
  const color = req.query.color;

  console.log(req.query);

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error || !release || !color) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.load.colorChange(decoded, release, color).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/store/cart').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.load.cart(decoded, req.query).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/history').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.load.history(decoded).then(response => {
      return Respond(res, { ...response });
    });
  });
});

router.route('/track').get((req, res) => {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error || !req.query.orderNumber) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

    User.load.track(decoded, req.query.orderNumber).then(response => {
      return Respond(res, { ...response });
    });
  });
});
// </editor-fold>
// <editor-fold API TOKEN
router.route('/public/store/order').post((req, res) => {
  if (!(req.body.key) || (typeof req.body.modelCode === 'undefined') || !(req.body.paymentToken) || !(req.body.name) || !(req.body.phoneNumber)  || !(req.body.bag) || !(req.body.streetAddress) || (typeof req.body.streetAddressCont === 'undefined') || !(req.body.city) || !(req.body.state) || !(req.body.postal) || !(req.body.country) || !(req.body.total)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  Guest.order(req.body).then(response => {
    return Respond(res, { ...response });
  });
});

router.route('/public/store').get((req, res) => {
  const key = req.query.key;

  if (!key || key === '') return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

  Guest.load.store(key, req.query).then(response => {
    return Respond(res, { ...response });
  });
});

router.route('/public/store/zoom').get((req, res) => {
  const key = req.query.key;
  const release = req.query.release;

  if (!key || key === '' || !release || release === '') return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

  Guest.load.zoom(key, release).then(response => {
    return Respond(res, { ...response });
  });
});

router.route('/public/store/zoom/color').get((req, res) => {
  const key = req.query.key;
  const release = req.query.release;
  const color = req.query.color;

  if (!key || key === '' || !release || release === '' || !color || color === '') return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

  Guest.load.colorChange(key, release, color).then(response => {
    return Respond(res, { ...response });
  });
});

router.route('/public/store/brand').get((req, res) => {
  const key = req.query.key;
  const identifier = req.query.identifier;

  if (!key || key === '' || !identifier || identifier === '') return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

  Guest.load.brand(key, identifier).then(response => {
    return Respond(res, { ...response });
  });
});


router.route('/public/store/bag').get((req, res) => {
  const key = req.query.key;
  const bag = req.query.bag;

  if (!key || key === '' || !bag ) return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

  Guest.load.bag(key, bag).then(response => {
    return Respond(res, { ...response });
  });
});

router.route('/public/store/pay').get((req, res) => {
  const key = req.query.key;
  const bag = req.query.bag;
  const name = req.query.name;
  const streetAddress = req.query.streetAddress;
  const streetAddressCont = req.query.streetAddressCont;
  const city = req.query.city;
  const state = req.query.state;
  const postal = req.query.postal;
  const country = req.query.country;
  const modelCode = req.query.modelCode;

  if (!key || key === '' || !name || !streetAddress || !city || !state || !postal || !country || typeof modelCode == "undefined") return Respond(res, {_iE: true, _c: 401, _e: 'BAD_AUTH'});

  Guest.load.pay(key, req.query).then(response => {
    return Respond(res, { ...response });
  });
});

// </editor-fold>
/*
  Respond
    res -> express provided parameter to send network call
    {
      _hE -> hasError,
      _c ->  response code,
      _e -> error
      _m -> message
      _body -> response
    }
*/
function Respond(res, {_hE = false, _c = 200, _e = null, _m = null, _body = {}}) {
  res.status(_c).json({_hE, _c, _e, _m, _body});
}
