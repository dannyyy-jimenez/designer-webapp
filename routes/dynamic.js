const express = require ('express');
const router = express.Router();
const mongoose = require('mongoose');
const path = require('path');
const jwt = require('jsonwebtoken');
const User = require('../controllers/user');
const Guest = require('../controllers/guest');
const Admin = require('../controllers/admin');
const Liana = require('forest-express-mongoose');
const fs = require('fs');

module.exports = router;

// <editor-fold> METADATA

router.get('/brand/:identifier', (req, res) => {
  const raw = fs.readFileSync(path.join(__dirname, "../client", "build", "index.html"), 'utf8');
  Guest.metadata.brand(req.params.identifier).then((brand) => {
    if (brand === 'Tailori') {
      res.send(raw);
      return;
    }

    let updatedMetadata = `<meta property="og:url" content="https://www.tailorii.app/shop/brand/${brand.identifier}" /><meta property="description" content="${brand.description}"/><meta property="og:description" content="${brand.description}"/><title>${brand.name}</title><meta name="title" content="${brand.name}"/><meta property="og:title" content="${brand.name}"/><meta property="twitter:title" content="${brand.name}"/>`;
    updatedMetadata += `<meta property="og:image" content="${Guest.metadata.formatShot(brand.logo)}"/>`

    const updated = raw.replace('<meta name="__DYNAMIC_META__"/>', updatedMetadata);

    res.send(updated);
  });
});

router.get('/zoom/:identifier', (req, res) => {
  const raw = fs.readFileSync(path.join(__dirname, "../client", "build", "index.html"), 'utf8');
  Guest.metadata.release(req.params.identifier).then((release) => {

    if (release === 'Tailori') {
      res.send(raw);
      return;
    }

    let updatedMetadata = `<meta property="og:url" content="https://www.tailorii.app/shop/zoom/${release.identifier}" /><meta property="og:description" content="Checkout ${release.design.name} on Tailori"/><meta property="product:catalog_id" content="${release.identifier}"/><meta property="product:retailer_item_id" content="${release.identifier}"/><meta property="og:url" content="https://www.tailorii.app/shop/zoom/${release.identifier}"/><meta property="product:brand" content="${release.designer.name}"/><meta property="product:availability" content="in stock"/><meta property="product:condition" content="new"/><meta property="og:price:amount" content="${release.price.toFixed(2)}"/><meta property="product:price:amount" content="${release.price.toFixed(2)}"/><meta property="og:price:currency" content="USD"/><meta property="product:price:currency" content="USD"/><title>${release.design.name} from ${release.designer.name}</title><meta name="title" content="${release.design.name} from ${release.designer.name}"/><meta property="og:title" content="${release.design.name} from ${release.designer.name}"/><meta property="twitter:title" content="${release.design.name} from ${release.designer.name}"/>`;
    for (let shot of release.design.shots) {
      updatedMetadata += `<meta property="og:image" content="${Guest.metadata.formatShot(shot)}"/>`
    }

    let updated = raw.replace('<meta name="__DYNAMIC_META__"/>', updatedMetadata);

    res.send(updated);
  });
});

// </editor-fold>
