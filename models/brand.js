const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');
const moment = require('moment');
const Schema = mongoose.Schema;

const payoutSchema = new Schema({
  _id: {type: Schema.Types.ObjectId, index: true, required: true, auto: true},
  amount: {type: Number, min: 0, required: true},
  transfer: {type: String, required: true} // STRIPE ID
}, {timestamps: true});

const schema = new Schema({
  creator: { type: Schema.Types.ObjectId, ref: 'User' },
  identifier: {type: String, required: true},
  logo: {type: String, required: true},
  name: {type: String, required: true},
  description: {type: String, required: true},
  hearts: { type: Number, min: 0, default: 0},
  releases: [{ type: Schema.Types.ObjectId, ref: 'Release' }],
  payouts: [payoutSchema],
  royalties: {type: Number, min: 0, default: 0},
  distribution: {type: Number, min: 0, default: 0},
  socials: {
    youtube: {type: String},
    instagram: {type: String},
    facebook: {type: String},
    snapchat: {type: String},
  },
  connect: {type: String, required: false}
}, {timestamps: true});

schema.plugin(mongooseUniqueValidator);
schema.methods.prettyReleases = function() {
  return this.releases.map((release) => {
    return {
      id: release.design._id,
      type: release.design.type,
      shots: release.design.shots,
      name: release.design.name,
      description: release.design.description,
      reactive: release.design.isReactive(),
      identifier: release.identifier,
      price: release.price,
      remaining: release.remaining,
      production: release.production,
      royalties: release.production * release.royalty
    }
  });
};
schema.methods.profilize = function(favorites) {
  return {
    _des: this.description,
    _name: this.name,
    _logo: this.logo,
    _f: favorites.includes(this._id),
    _h: this.hearts,
    _i: this.releases.length,
    _d: this.distribution,
    _sm: {
      _yt: this.socials.youtube || false,
      _fb: this.socials.facebook || false,
      _ig: this.socials.instagram || false,
      _sc: this.socials.snapchat || false
    },
    _m: this.releases.map((release) => release.prettyFormat(favorites))
  }
};
schema.methods.lgxyQuickFormat = function() {
  return {
    id: this._id.toString(),
    identifier: this.identifier,
    name: this.name,
    logo: this.logo,
    payouts: this.royalties,
    distribution: this.distribution,
    hasReleases: this.releases.length > 0
  }
};
schema.methods.lgxyFormat = function() {
  return {
    _lu: moment(this.updatedAt).format("MM/DD"),
    _d: this.description,
    _p: this.royalties.toFixed(2),
    _db: this.distribution,
    _r: this.releases.length,
    _o: {
      identifier: this.creator._id,
      name: this.creator.firstName + ' ' + this.creator.lastName
    }
  }
}
module.exports = mongoose.model('Brand', schema);
