const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');
const moment = require('moment');
const Schema = mongoose.Schema;

const schema = new Schema({
  designer: { type: Schema.Types.ObjectId, ref: 'Brand' },
  design: { type: Schema.Types.ObjectId, ref: 'Design' },
  identifier: {type: String, required: true, unique: false},
  price: {type: Number, required: true, default: 0},
  customizable: {
    color: {type: Boolean}
  },
  featured: {type: Boolean, default: false},
  production: {type: Number, required: true, min: 0, default: 0},
  royalty: {type: Number, required: true, min: 0, default: 0},
  actions: [String], // colorable, sizeable
  remaining: {type: Number, default: -1},
  printable: {type: Boolean, default: false},
  approved: {type: Boolean, default: false}
}, {timestamps: true});

schema.plugin(mongooseUniqueValidator);
schema.methods.prettyFormat = function(favorites) {
  if (this.remaining != '-1') {
    this.actions.push('multiples');
  }
  return {
    design: {
      id: this.design.id,
      type: this.design.type,
      shots: this.design.shots,
      name: this.design.name,
      reactive: this.design.isReactive(),
      reactives: this.design.getReactives(),
      description: this.design.description,
      base: this.design.metrics.base
    },
    designer: {
      identifier: this.designer.identifier,
      logo: this.designer.logo,
      name: this.designer.name,
      description: this.designer.description,
      isFavorite: favorites.includes(this.designer._id)
    },
    identifier: this.identifier,
    price: this.price.toFixed(2),
    remaining: this.remaining,
    customizables: this.customizable,
    actions: this.actions,
    approved: this.approved
  }
};
schema.methods.lgxyQuickFormat = function(permissions) {
  return {
    identifier: this.identifier,
    name: this.design.name,
    cover: this.design.shots[0],
    price: this.price.toFixed(2),
    date: moment(this.createdAt).format('MM/DD'),
    approved: permissions.includes('trinum') ? this.printable : this.approved
  }
};
schema.methods.lgxyFormat = function(permissions) {
  return {
    _ds: {
      identifier: this.designer.identifier,
      name: this.designer.name
    },
    _dID: this.design._id,
    _i: this.identifier,
    _n: this.design.name,
    _s: this.design.shots,
    _t: this.design.type,
    _d: this.design.description,
    _m: this.design.mode,
    _mtr: this.design.metrics.sides,
    _p: this.price.toFixed(2),
    _r: this.royalty.toFixed(2),
    _cop: this.production.toFixed(2),
    _a: this.actions.map((action) => action.toUpperCase()).join(','),
    _cA: moment(this.createdAt).format('MM/DD'),
    _v: permissions.includes('trinum') ? this.printable : this.approved
  }
};
module.exports = mongoose.model('Release', schema);
