const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const Schema = mongoose.Schema;
const crypto = require('crypto');

const schema = new Schema({
  crypto: {type: String, required: true, unique: true},
  socket: {type: String, required: true, unique: true},
  sockets: {type: [String], default: []},
  firstName: {type: String, required: true, unique: false},
  lastName: {type: String, required: true, unique: false},
  dob: {type: Date, required: true, unique: false},
  phoneNumber: {type: String, required: true, unique: true},
  brand: { type: Schema.Types.ObjectId, ref: 'Brand', required: false, index: true, unique: true, sparse: true},
  portfolio: [{ type: Schema.Types.ObjectId, ref: 'Design'}],
  favorites: [{ type: Schema.Types.ObjectId, ref: 'Brand' }],
  password: String,
  permissions: {
    sms: {type: Boolean, unique: false, default: true},
  },
  orders: [{ type: Schema.Types.ObjectId, ref: 'Order' }],
  salt: String,
  payment: {
    customer: String,
    source: String,
    updated: {type: Date}
  },
  cart: {
    items: [{
      _id: {type: Schema.Types.ObjectId, index: true, required: true, auto: true},
      release: { type: Schema.Types.ObjectId, ref: 'Release'},
      quantity: {type: Number, min: 1, default: 1},
      size: {type: String, default: 'S'}, // XS, S, M, L, XL, XXL
      color: {type: String, default: 'default'},
      reactives: [{
        artwork: {type: String},
        side: {type: String}
      }]
    }],
    shipping: {
      cost: {type: Number, min: 0, default: 0},
      valid: {type: Boolean, default: false},
      streetAddress: {type: String},
      streetAddressCont: {type: String},
      city: {type: String},
      state: {type: String},
      postal: {type: String},
      country: {type: String}
    },
    subtotal: {type: Number, min: 0, default: 0},
    tax: {type: Number, min: 0, default: 0},
    taxRate: {type: Number, min: 0, default: 0.1025}
  },
  deleted: {type: Boolean, default: false, required: false}
}, {timestamps: true});

schema.methods.setPassword = function(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.password = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};
schema.methods.validPassword = function(password) {
 const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
 return this.password === hash;
};
schema.methods.generateJWT = function() {
  var exp = moment().add(1, 'y');

  return jwt.sign({
    crypto: this.crypto,
    _fn: this.firstName + ' ' + this.lastName,
    _s: this.socket,
    _d: moment().toDate(),
    exp: exp.unix()
  }, process.env.JWT_SECRET);
};
schema.methods.toAuth = function() {
  return {
    fullName: this.firstName + ' ' + this.lastName,
    socket: this.socket,
    crypto: this.generateJWT(),
  };
};
schema.methods.lgxyQuickFormat = function() {
  return {
    id: this._id.toString(),
    name: this.firstName + ' ' + this.lastName,
    brand: this.brand ? this.brand.name : '',
    designs: this.portfolio.length,
    since: moment(this.createdAt).format("MM/YYYY"),
    deleted: this.deleted
  }
};
schema.methods.lgxyFormat = function() {
  return {
    _wd: this.deleted,
    _lu: moment(this.updatedAt).format("MM/DD"),
    _pn: this.phoneNumber,
    _dob: moment(this.dob).format("MM/DD/YYYY"),
    _b: {
      identifier: this.brand ? this.brand.identifier : '',
      name: this.brand ? this.brand.name : ''
    }
  }
};
schema.plugin(mongooseUniqueValidator);
module.exports = mongoose.model('User', schema);
