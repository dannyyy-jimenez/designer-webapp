const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');
const moment = require('moment');
const Schema = mongoose.Schema;
const Colors = require('../controllers/colors');

const itemSchema = new Schema({
  _id: {type: Schema.Types.ObjectId, index: true, required: true, auto: true},
  seller: { type: Schema.Types.ObjectId, ref: 'Brand', required: true },
  design: { type: Schema.Types.ObjectId, ref: 'Design', required: true },
  color: { type: String, required: true},
  size: { type: String, required: true},
  production: {type: Number, min: 0, default: 0},
  payout: {type: Number, min: 0, default: 0},
  quantity: { type: Number, min: 0, unique: false, required: true},
  price: { type: Number, min: 0, unique: false, required: true},
  reactives: [{
    artwork: {type: String},
    side: {type: String}
  }]
});

const updateSchema = new Schema({
  _id: {type: Schema.Types.ObjectId, index: true, required: true, auto: true},
  message: { type: String, required: true},
  provider: { type: String, required: true, default: "Tailori"}, // Tailori, Trinum
}, {timestamps: true});

const GuestSchema = new Schema({
  phoneNumber: {type: String},
  name: {type: String}
});

const schema = new Schema({
  guest: {type: GuestSchema},
  buyer: { type: Schema.Types.ObjectId, ref: 'User' },
  details: [itemSchema],
  updates: [updateSchema],
  serial: { type: String, required: true},
  cost: {
    experimental: {type: Number, min: 0, unique: false, default: 0},
    production: {type: Number, min: 0, unique: false, required: true},
    subtotal: { type: Number, min: 0, unique: false, required: true},
    tax: { type: Number, min: 0, unique: false, required: true},
    taxRate: { type: Number, min: 0, default: 0.1025, unique: false, required: true},
    shipping: { type: Number, min: 0, unique: false, required: true},
    discount: {type: Number, min: 0, unique: false},
    total: { type: Number, min: 0, unique: false, required: true}
  },
  shipping: {
    details: {
      streetAddress: {type: String},
      streetAddressCont: {type: String},
      city: {type: String},
      state: {type: String},
      postal: {type: String},
      country: {type: String}
    },
    label: {
      identifier: {type: String},
      url: {type: String}
    },
    carrier: {type: String, required: true, default: "EasyPost"},
    tracking: {type: String, required: false}
  },
  stripe: {type: String},
  trinumDesign: {type: String},
  supporting: { type: Schema.Types.ObjectId, ref: 'Model' },
  fulfilled: {type: Boolean, default: false}
}, {timestamps: true});

schema.plugin(mongooseUniqueValidator);
schema.methods.quickFormat = function() {
  return {
    identifier: this.serial,
    total: this.cost.total.toFixed(2),
    date: moment(this.createdAt).format("MM/DD"),
    fulfilled: this.fulfilled
  }
};
schema.methods.prettyItems = function() {
  return this.details.map((detail) => {
    return {
      price: detail.price.toFixed(2),
      singlePrice: (detail.price / detail.quantity).toFixed(2),
      quantity: detail.quantity.toString(),
      designer: detail.seller.name,
      name: detail.design.name
    }
  });
};
schema.methods.prettyUpdates = function() {
  return this.updates.map((update) => {
    return {
      content: update.message,
      provider: update.provider,
      date: moment(update.createdAt).format("MM/DD")
    }
  }).reverse();
};
function GetDaysDue(date) {
  let ideal = moment(date).add('5', 'd');

  return ideal.diff(moment(), 'd');
}
schema.methods.lgxyQuickFormat = function() {
  return {
    identifier: this.serial,
    buyer: (this.buyer || this.guest.name).toString(),
    items: this.details.reduce((total, item) => total + item.quantity, 0),
    total: this.cost.total.toFixed(2),
    date: moment(this.createdAt).format("MM/DD"),
    daysLeft: GetDaysDue(this.createdAt),
    fulfilled: this.fulfilled
  }
};
schema.methods.formattedShipping = function() {
  return [this.shipping.details.streetAddress, this.shipping.details.city, this.shipping.details.state, this.shipping.details.postal].join(', ');
};
schema.methods.shippingFriendly = function() {
  return [this.shipping.details.streetAddress + ' ' + this.shipping.details.streetAddressCont, this.shipping.details.city, this.shipping.details.state, this.shipping.details.postal, this.shipping.details.country].join(',');
};
schema.methods.wasDelivered = function() {
  let delivered = false;

  this.updates.forEach((update) => {
    if (update.message.includes("Time to Explore")) delivered = true;
  })

  return delivered;
};
function GetMetrics(sides, detail) {
  return sides.map(side => {
    if (side.artwork.includes('templates:static:reactive')) {
      let reactiveSide = detail.reactives.find(r => r.side === side.side);
      return reactiveSide || side;
    }

    return side;
  });
}
schema.methods.lgxyFormat = function() {
  return {
    _f: this.fulfilled,
    _c: moment(this.createdAt).format("MM/DD"),
    _exp: (this.cost.experimental || 0).toFixed(2),
    _cop: (this.cost.production || 0).toFixed(2),
    _po: this.details.reduce((total, detail) => total + (detail.design && detail.design.trinum.includes("TLRI*EXP*") ? 0 : detail.payout), 0).toFixed(2),
    _t: this.cost.total.toFixed(2),
    _tx: this.cost.tax.toFixed(2),
    _sP: this.cost.shipping.toFixed(2),
    _a: this.formattedShipping(),
    _eg: CalculateEstimatedGains(this),
    _u: this.prettyUpdates(),
    _b: {
      identifier: this.buyer ? this.buyer._id.toString() : "GUEST",
      name: this.buyer ? this.buyer.firstName + ' ' + this.buyer.lastName : this.guest.name + " as GUEST"
    },
    _i: this.details.map((detail) => {
      if (!detail.design) {
        return {
          id: detail._id.toString(),
          token: 'TLRI*TRNM*DLTD',
          identifier: '',
          name: 'Deleted',
          cover: '',
          quantity: 0,
          color: 'BLCK',
          metrics: [],
          size: 'M'
        }
      }

      return {
        id: detail._id.toString(),
        token: detail.design.trinum,
        identifier: (detail.design._id || '').toString(),
        name: detail.design.name,
        cover: detail.design.shots[0],
        quantity: detail.quantity,
        color: Colors(detail.design.type, detail.color, true, false, true),
        metrics: GetMetrics(detail.design.metrics.sides, detail),
        size: detail.size || 'M'
      }
    })
  }
};

function CalculateEstimatedGains(order) {
  const StripeFee = (order.cost.total * 0.029) + 0.30;
  const AfterPayouts = (order.cost.subtotal - (order.cost.production + (order.cost.experimental || 0) + order.details.reduce((total, detail) => total + (detail.design.trinum.includes("TLRI*EXP*") ? 0 : detail.payout), 0)));

  return (AfterPayouts - StripeFee).toFixed(2);
}

module.exports = mongoose.model('Order', schema);
