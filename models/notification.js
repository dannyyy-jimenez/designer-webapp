const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');
const moment = require('moment');
const Schema = mongoose.Schema;

const schema = new Schema({
  recipient: { type: Schema.Types.ObjectId, ref: 'User' },
  header: { type: String, required: true},
  subheader: { type: String, required: false, default: null},
  action: { type: String, required: false, default: 'none'},
  metadata: { type: Object, required: false, default: null},
  cleared: {type: Boolean, required: false, default: false}
}, {timestamps: true});

schema.plugin(mongooseUniqueValidator);
schema.methods.format = function() {
  return {
    header: this.header,
    subheader: this.subheader || '',
    action: this.action || 'none',
    metadata: this.metadata
  };
};
module.exports = mongoose.model('Notification', schema);
