const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

const schema = new Schema({
  firstName: {type: String, required: true, unique: false},
  lastName: {type: String, required: true, unique: false},
  dob: {type: Date, required: true, unique: false},
  phoneNumber: {type: String, required: true, unique: true},
  socials: [{type: String}],
  shots: {type: String}, // Folder for shots (Google Photos)
  scheme: {type: String, required: true}, // minthirty $4 -> $30 more ($3), minfifty big $8 -> $50 more ($8 off), multiplier (10% off), default $2 ($1 shipping)
  identifier: {type: String, unique: false, required: false},
  connect: {type: String, required: true, unique: false}
}, {timestamps: true});

schema.plugin(mongooseUniqueValidator);
module.exports = mongoose.model('Model', schema);
