const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const Schema = mongoose.Schema;
const crypto = require('crypto');

const actionSchema = new Schema({
  _id: {type: Schema.Types.ObjectId, index: true, required: true, auto: true},
  description: {type: String, required: true},
  user: { type: Schema.Types.ObjectId, ref: 'User', required: false },
  brand: { type: Schema.Types.ObjectId, ref: 'Brand', required: false },
  design: { type: Schema.Types.ObjectId, ref: 'Design', required: false },
  order: { type: Schema.Types.ObjectId, ref: 'Order', required: false },
  release: { type: Schema.Types.ObjectId, ref: 'Release', required: false }
}, {timestamps: true});

const schema = new Schema({
  crypto: {type: String, required: true, unique: true},
  socket: {type: String, required: true, unique: true},
  sockets: {type: [String], default: []},
  firstName: {type: String, required: true, unique: false},
  lastName: {type: String, required: true, unique: false},
  phoneNumber: {type: String, required: true, unique: true},
  password: String,
  salt: String,
  permissions: {type: [String], required: true, default: []}, // orders, finances, orders-notify, developer, brands, users, users-delete, releases, releases-approve, designs, payouts, trinum
  stripe: { type: String },
  actions: [actionSchema]
}, {timestamps: true});

schema.methods.setPassword = function(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.password = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};
schema.methods.validPassword = function(password) {
 const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
 return this.password === hash;
};
schema.methods.generateJWT = function() {
  var exp = moment().add(1, 'y');

  return jwt.sign({
    crypto: this.crypto,
    _fn: this.firstName + ' ' + this.lastName,
    _s: this.socket,
    _d: moment().toDate(),
    exp: exp.unix()
  }, process.env.JWT_ADMIN_SECRET);
};
schema.methods.lgxyPayee = function() {
  return {
    name: this.firstName,
    id: this._id.toString()
  }
}
schema.methods.toAuth = function() {
  return {
    fullName: this.firstName + ' ' + this.lastName,
    socket: this.socket,
    crypto: this.generateJWT(),
  };
};

schema.plugin(mongooseUniqueValidator);
module.exports = mongoose.model('Admin', schema);
