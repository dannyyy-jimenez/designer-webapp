const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');
const moment = require('moment');
const Schema = mongoose.Schema;

/*

  TYPES
    shirtT - T Shirt
    shirtT_Sleeve - T Shirt with long sleeve
    hoodie - Sweatshirt with face hoodie

*/

const SideSchema = new Schema({
  artwork: {type: String},
  side: {type: String},
  reactivity: {type: String}
});

const schema = new Schema({
  creator: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  designer: { type: Schema.Types.ObjectId, ref: 'Brand', required: false },
  type: { type: String, required: true},
  shots: { type: [String], required: true},
  mode: {type: String, required: true}, // Template, Upload
  name: { type: String, required: true},
  metrics: {
    base: {type: String, required: true, default: 'BLCK'}, // COLOR VALUE OF ELEMENT
    sides: [{type: SideSchema}]
  },
  trinum: {type: String, default: 'TRNM*TLRI*'},
  description: { type: String, required: true},
  deleted: {type: Boolean, default: false}
}, {timestamps: true});

schema.plugin(mongooseUniqueValidator);
schema.methods.getArtworks = function() {
  let works = this.metrics.sides.map((metric) => metric.artwork);
  return works;
}
schema.methods.getReactives = function() {
  return this.metrics.sides.filter(side => side.reactivity);
}
schema.methods.isReactive = function() {
  return this.metrics.sides.find(side => side.reactivity);
}
schema.methods.format = function(releases) {
  let hasDropped = false;

  for (let release of releases) {
    if (release.design.toString() === this._id.toString()) {
      hasDropped = true;
      break;
    }
  }

  return {
    id: this._id,
    t: this.type,
    s: this.shots,
    n: this.name,
    b: this.metrics.base,
    d: this.description,
    r: this.isReactive(),
    hD: hasDropped
  }
};
schema.methods.lgxyQuickFormat = function() {
  return {
    identifier: this._id.toString(),
    creator: this.creator.toString(),
    name: this.name,
    cover: this.shots[0],
    type: this.type,
    date: moment(this.createdAt).format("MM/DD"),
    deleted: this.deleted,
  }
};
schema.methods.lgxyFormat = function() {
  return {
    _c: {
      identifier: this.creator._id,
      name: this.creator.firstName + ' ' + this.creator.lastName
    },
    _ds: {
      identifier: this.designer ? this.designer.identifier : '',
      name: this.designer ? this.designer.name : ''
    },
    _cA: moment(this.createdAt).format("MM/DD"),
    _s: this.shots,
    _t: this.type,
    _m: this.mode,
    _wd: this.deleted,
    _d: this.description
  }
};
module.exports = mongoose.model('Design', schema);
